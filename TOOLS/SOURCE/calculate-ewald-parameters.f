      PROGRAM Calculate_Ewald_Parameters
      implicit none

      double precision Box,EwaldPrecision,Rcoulcut,tol,Alpha,tol1,OnePi
      integer Kmax
      character*100 arg

      Parameter (OnePi = 4.0d0*DATAN(1.0d0))

      IF(COMMAND_ARGUMENT_COUNT().EQ.1) THEN
         CALL GETARG(1, arg)
         READ(arg,*) Box
      ELSE
         WRITE(6,'(A)',ADVANCE="NO") "Box size = "
         READ(6,*) Box
      END IF

      EwaldPrecision=1.0D-6
      Rcoulcut = 0.4d0*Box
      tol = dsqrt(abs(log(EwaldPrecision*Rcoulcut)))
      Alpha = dsqrt(abs(log(EwaldPrecision*Rcoulcut*tol)))/Rcoulcut
      tol1 = dsqrt(-log(EwaldPrecision*Rcoulcut*4.0d0*tol*tol*Alpha*Alpha))
      Kmax = NINT(0.25d0+Box*Alpha*tol1/OnePi)
      WRITE(6,'(A)') "Suggested Parameters for Ewald Method: (precision = 1E-6)"
      WRITE(6,'(A,f6.2)') "Cutoff = ", Rcoulcut
      WRITE(6,'(A,f9.6)') "Alpha  = ", Alpha
      WRITE(6,'(A,i5)')   "Kmax   = ", Kmax
      WRITE(6,*)


      STOP
      END
