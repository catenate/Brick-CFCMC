      program Smoothen_Weightfunction
      implicit none

      integer Maxbin,Maxcol
      Parameter (Maxbin = 1000)
      Parameter (Maxcol = 5)

      integer Factor,I,J,K,N,Nn,Nnn,io,Ncol,Nweightfunctions,arg_int(30)
      double precision X(Maxbin),Y(Maxbin,Maxcol),Y2(Maxbin,Maxcol),Lambda,Weight(Maxcol),Xnew(Maxbin),Ynew(Maxbin,Maxcol)
      logical arguments_from_command_line
      character*256 line,weightfile,arg(30)
      character*100 dummy,fracname
      character*1 answer

      arguments_from_command_line=.false.
      IF(COMMAND_ARGUMENT_COUNT().GT.1) THEN
         DO I=2,COMMAND_ARGUMENT_COUNT()
            CALL GETARG(I, arg(I))
            READ(arg(I),*) arg_int(I-1)
         END DO
         arguments_from_command_line=.true.
      END IF

      weightfile="OUTPUT/WEIGHTFUNCTIONS/weightfunction.out"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m###############################################################\033[0m"
      WRITE(6,'(A)') "\033[1;33m Smoothen Weightfunctions: Rebin, Fit a Spline and Discretize  \033[0m"
      WRITE(6,'(A)') "\033[1;33m###############################################################\033[0m"
      WRITE(6,*)
      IF(.NOT.arguments_from_command_line) THEN
         WRITE(6,'(A)',ADVANCE="NO") "Use the file 'OUTPUT/WEIGHTFUNCTIONS/weightfunction.out' (y/n): "
         READ(6,*) answer
         IF(answer(1:1).EQ."n".OR.answer(1:1).EQ."N") THEN
            WRITE(6,'(A)',ADVANCE="NO") "Name of the file: "
            READ(6,*) weightfile
         END IF
      END IF

      Nweightfunctions=0

      OPEN(7,file=weightfile)
      OPEN(8,file="weightfunction.rebinned")
      OPEN(9,file="weightfunction.smooth")

  10  CONTINUE
      READ(7,'(A)',END=50) line
      READ(line,*) dummy, dummy, dummy, dummy, dummy, dummy, fracname

      Nweightfunctions = Nweightfunctions + 1

      WRITE(6,*)
      WRITE(6,'(A,i2,A2,A)') "Weightfunction ", Nweightfunctions, ": ", TRIM(fracname)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"

      IF(.NOT.arguments_from_command_line) THEN
  20     WRITE(6,'(A)',ADVANCE="NO") "Number of Boxes or Reaction Steps: "
         READ(6,*,iostat=io,err=20) Ncol
         IF(Ncol.LT.1) THEN
            WRITE(6,'(A)') "Number should be a positive integer"
            GO TO 20
         END IF
      ELSE
         Ncol = arg_int(3*Nweightfunctions-2)
      END IF

      IF(.NOT.arguments_from_command_line) THEN
  30     WRITE(6,'(A)',ADVANCE="NO") "Rebin Factor: "
         READ(6,*,iostat=io,err=30) Factor
         IF(Factor.LT.1) THEN
            WRITE(6,'(A)') "Rebin Factor should be a positive integer"
            GO TO 30
         END IF
      ELSE
         Factor = arg_int(3*Nweightfunctions-1)
      END IF

      N = 0

      DO
         READ(7,*,IOSTAT=io) Lambda, (Weight(K), K=1,Ncol)
         IF(io.EQ.0) THEN
            IF(Lambda.LT.0.0d0.OR.Lambda.GT.1.0d0) THEN
               WRITE(6,*) "Error Input"
               STOP
            END IF
            N = N + 1
            IF(N.GT.Maxbin) THEN
               WRITE(6,*) "Error Maxbin"
               STOP
            END IF
            X(N) = Lambda
            DO K=1,Ncol
               Y(N,K) = Weight(K)
            END DO
            CYCLE
         ELSE
            WRITE(6,'(A,i4)') "Number Of Bins: ", N
C     Rebinning And Copy The First And Last Entries
            J  = 0
            Nn = 1
            Lambda  = 0.0d0

            Xnew(1) = 0.0d0
            DO K=1,Ncol
               Weight(K) = 0.0d0
               Ynew(1,K) = Y(1,K)
            END DO

            DO I=2,N-1

               Lambda = Lambda + X(I)
               DO K=1,Ncol
                  Weight(K) = Weight(K) + Y(I,K)
               END DO
               J = J + 1

               IF(J.EQ.Factor.OR.I.EQ.N-1) THEN

C     Enter New Bin
                  Nn = Nn + 1
                  IF(Nn.GT.Maxbin) THEN
                     WRITE(6,*) "Error Maxbin"
                     STOP
                  END IF

                  Xnew(Nn) = Lambda/Dble(J)
                  DO K=1,Ncol
                     Ynew(Nn,K) = Weight(K)/Dble(J)
                  END DO

                  J = 0
                  Lambda = 0.0d0
                  DO K=1,Ncol
                     Weight(K) = 0.0d0
                  END DO
               END IF
            END DO

            Nn = Nn + 1
            IF(Nn.Gt.Maxbin) Stop "Error Maxbin"

            Xnew(Nn) = 1.0d0
            DO K=1,Ncol
               Ynew(Nn,K) = Y(N,K)
            END DO

            WRITE(6,'(A,i4)') "Number of Bins after Rebinning: ", Nn

C     Write Rebinned Histrogram
            DO I=1,Nn
               WRITE(8,'(f10.7,99(1x,e20.10))') Xnew(I), (Ynew(I,K), K=1,Ncol)
            END DO
            WRITE(8,*)
            WRITE(8,*)

C     Write Spline Fit To The New Histrogram
            DO K=1,Ncol
               CALL Spline(Xnew,Ynew(:,K),Nn,Y2(:,K))
            END DO

            IF(.NOT.arguments_from_command_line) THEN
  40           WRITE(6,'(A)',ADVANCE="NO") "Number of Bins for Discretizing: "
               READ(6,*,iostat=io,err=40) Nnn
               IF(Factor.LT.1) THEN
                  WRITE(6,'(A)') "Rebin Factor should be a positive integer"
                  GO TO 40
               END IF
            ELSE
               Nnn = arg_int(3*Nweightfunctions)
            END IF

            WRITE(9,'(A,i5,A,A)') "# ", Nnn, " bins for weightfunction of ", TRIM(fracname)
            DO I=1,Nnn
               Lambda = Dble(I-1)/dble(Nnn-1)
               DO K=1,Ncol
                  CALL Splint(Xnew,Ynew(:,K),Y2(:,K),Nn,Lambda,Weight(K))
               END DO
               WRITE(9,'(f10.7,99(1x,e20.10))') Lambda, (Weight(K), K=1,Ncol)
            END DO
            WRITE(9,*)
            WRITE(9,*)

            BACKSPACE(7)
            GO TO 10
         END IF
      END DO

  50  CONTINUE
      CLOSE(7)
      CLOSE(8)
      CLOSE(9)

      WRITE(6,*)
      WRITE(6,'(A)') "Finished. Copy weightfunction.smooth to INPUT/weightfunction.in to use in simulations."
      WRITE(6,*)

      STOP

      CONTAINS

      SUBROUTINE Spline(X,Y,N,Y2)
      implicit none

C     Setup Second Derivatives For The Spline Interpolation

      integer Maxbin
      Parameter (Maxbin = 102)

      double precision X(Maxbin),Y(Maxbin),Y2(Maxbin),P,Qn,Sig,Un,U(Maxbin)
      integer I,K,N

      Y2(1)=0.0d0
      U(1)=0.d0

      DO I=2,N-1
         Sig=(X(I)-X(I-1))/(X(I+1)-X(I-1))
         P=Sig*Y2(I-1)+2.0d0
         Y2(I)=(Sig-1.0d0)/P
         U(I)=(6.0d0*((Y(I+1)-Y(I))/(X(I+1)-X(I))-(Y(I)-Y(I-1))/(X(I)-X(I-1)))/(X(I+1)-X(I-1))-Sig*U(I-1))/P
      END DO

      Qn=0.0d0
      Un=0.0d0

      Y2(N)=(Un-Qn*U(N-1))/(Qn*Y2(N-1)+1.0d0)

      DO K=N-1,1,-1
         Y2(K)=Y2(K)*Y2(K+1)+U(K)
      END DO

      RETURN
      END SUBROUTINE

      SUBROUTINE Splint(Xa,Ya,Y2a,N,X,Y)
      implicit none

C     Perform The Spline Interpolation Using The Second Derivatives
C     Obtained From The Previous Subroutine

      integer Maxbin
      Parameter (Maxbin = 102)

      integer N,K,Khi,Klo
      double precision X,Y,Xa(Maxbin),Y2a(Maxbin),Ya(Maxbin),A,B,H

      Klo=1
      Khi=N

  3   CONTINUE
      IF (Khi-Klo.Gt.1) THEN
         K=(Khi+Klo)/2
         IF(Xa(K).Gt.X) THEN
            Khi=K
         ELSE
            Klo=K
         END IF
         GO TO 3
      END IF

      H=Xa(Khi)-Xa(Klo)

      IF(H.Eq.0.0d0) THEN
         WRITE(6,*) "Bad Input For Xa In Subroutine Splint"
         STOP
      END IF

      A=(Xa(Khi)-X)/H
      B=(X-Xa(Klo))/H
      Y=A*Ya(Klo)+B*Ya(Khi)+((A**3-A)*Y2a(Klo)+(B**3-B)*Y2a(Khi))*(H**2)/6.0d0

      RETURN
      END SUBROUTINE

      END
