      PROGRAM create_input
      implicit none

      integer  Nmoltypeinlist,Nmoltype,Natoms,Nbonds,Ntorsions,Nintra,Nfractype,Nmolinfrac,Ncoeff
      integer  I,J,K,Iatom(4),Ibond(4),Itype,Imoltype(9),Nmolinbox(2),Nbox,Natomsinlist,io
      integer  Ntorsioninlist,Nbeads,Ntemplate,Ensemble,Nmoltypinreac,Nstoi,Tm,Nreactions,Nstoiprev
      integer  Nrottemp,Nvibtemp,Npairs,Nbendings,Nbendinginlist,NM,NA,NB,NT,Kmax(2),Istartinbox
      double precision X,Y,Z,Scalelj6,Scalelj12,Scalecoul,dtrans(2),drot(2),dlambda(2),Box(2),Sig,Eps,Q
      double precision Ators(6),dphi,Rljcut(2),Rcoulcut(2),Alpha(2),Qpartition,molmass,theta0,K_bend
      double precision qtrans,qrot,qvib,qelec,onepi,dtheta
      double precision tol,tol1,EwaldPrecision
      logical  Lcheck,Lcheckmol(100),Llj,Lcoul,Lidealgas,Lpartition,Lfugacity,Lewald

      character*24  Catom,atomlist(100)
      character*24  Ctors,torsionlist(100)
      character*24  Cbend,bendinglist(100)
      character*24  Cmoltypeinlist(100),Cmoltype(100),templatelist(100)
      character*24  Cmolname,Calkane,Cfracname,Cfractype
      character*7   Symb
      character*100 molfile,Molarmass,molinbox,volbox,delta,BRICKDIR
      character*1   answer,answer_array(2)

      Parameter (onepi = 4.0d0*DATAN(1.0d0))

      Natomsinlist   = 0
      Nbendinginlist = 0
      Ntorsioninlist = 0
      Ntemplate      = 0

      NM = 11
      NA = 0
      NB = 4
      NT = 2

      CALL GETENV("BRICK_DIR",BRICKDIR)

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m###############################################################\033[0m"
      WRITE(6,'(A)') "\033[1;33m                 Create topology and forcefield                \033[0m"
      WRITE(6,'(A)') "\033[1;33m###############################################################\033[0m"
      WRITE(6,*)

      WRITE(6,'(A)') "List of molecules in database:"
      CALL system('ls ${BRICK_DIR}/MOLECULES')
      OPEN(87,file=TRIM(BRICKDIR)//"/MOLECULES/.listofmolecules")
      READ(87,*) Nmoltypeinlist
      DO I=1,Nmoltypeinlist
         READ(87,*) Cmoltypeinlist(I)
      END DO
      CLOSE(87)

      WRITE(6,*)
      WRITE(6,'(A)') "For creating an alkane of N beads, use 'alkane-N'"
      WRITE(6,'(A)') "If a Molecule is not in this list an empty template is inserted"
      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

 10   WRITE(6,'(A)',ADVANCE="NO") "Number of molecule types: "
      READ(6,*,iostat=io,err=10) Nmoltype
      WRITE(6,*)

      IF(Nmoltype.LT.1) THEN
         WRITE(6,'(A)') "Number of molecule should be 1 or more"
         GO TO 10
      END IF


      DO I=1,Nmoltype
         WRITE(6,'(1x,A14,i3,A2)',ADVANCE="NO") "Molecule type ", I, ": "
         READ(6,*) Cmoltype(I)

         Lcheckmol(I)=.false.

         IF(Cmoltype(I)(1:6).EQ."alkane".OR.Cmoltype(I)(1:6).EQ."Alkane") THEN
            READ(Cmoltype(I)(8:12),'(i)') Nbeads
            CALL CREATE_ALKANE(Nbeads,Calkane)
            Cmoltype(I)=Calkane
            Lcheckmol(I)=.true.
         END IF

         IF(LEN(TRIM(Cmoltype(I))).GT.NM) NM=LEN(TRIM(Cmoltype(I)))

         IF(.NOT.Lcheckmol(I)) THEN
            DO J=1,Nmoltypeinlist
               IF(Cmoltype(I).EQ.Cmoltypeinlist(J)) THEN
                  Lcheckmol(I)=.true.
                  EXIT
               END IF
            END DO
         END IF

         IF(Lcheckmol(I)) THEN
            molfile=TRIM(BRICKDIR) // "/MOLECULES/" // Cmoltype(I)
            OPEN(71,file=molfile)
            READ(71,*,iostat=io,err=20) Molarmass
            READ(71,*,iostat=io,err=20)
            READ(71,*,iostat=io,err=20) Natoms

            DO J=1,Natoms
               READ(71,*,iostat=io,err=20) Catom, X, Y, Z
               CALL CHECK_ATOMLIST(Catom,atomlist,Natomsinlist,Lcheck)
               IF(.NOT.Lcheck) THEN
                  Natomsinlist=Natomsinlist+1
                  atomlist(Natomsinlist) = Catom
                  IF(LEN(TRIM(Catom)).GT.NA) NA=LEN(TRIM(Catom))
               END IF
            END DO

            READ(71,*,iostat=io,err=20)
            READ(71,*,iostat=io,err=20) Nbonds

            DO J=1,Nbonds
               READ(71,*,iostat=io,err=20) (Ibond(K), K=1,2)
            END DO

            READ(71,*,iostat=io,err=20)
            READ(71,*,iostat=io,err=20) Nbendings

            DO J=1,Nbendings
               READ(71,*,iostat=io,err=20) Cbend, (Iatom(K), K=1,3)
               CALL CHECK_BENDINGLIST(Cbend,bendinglist,Nbendinginlist,Lcheck,K)
               IF(.NOT.Lcheck) THEN
                  Nbendinginlist=Nbendinginlist+1
                  bendinglist(Nbendinginlist) = Cbend
                  IF(LEN(TRIM(Cbend)).GT.NB) NB=LEN(TRIM(Cbend))
               END IF
            END DO

            READ(71,*,iostat=io,err=20)
            READ(71,*,iostat=io,err=20) Ntorsions

            DO J=1,Ntorsions
               READ(71,*,iostat=io,err=20) Ctors, (Iatom(K), K=1,4)
               CALL CHECK_TORSIONLIST(Ctors,torsionlist,Ntorsioninlist,Lcheck,K)
               IF(.NOT.Lcheck) THEN
                  Ntorsioninlist=Ntorsioninlist+1
                  torsionlist(Ntorsioninlist) = Ctors
                  IF(LEN(TRIM(Ctors)).GT.NT) NT=LEN(TRIM(Ctors))
               END IF
            END DO

            READ(71,*,iostat=io,err=20)
            READ(71,*,iostat=io,err=20) Nintra

            DO J=1,Nintra
               READ(71,*,iostat=io,err=20) (Iatom(K), K=1,2), Scalelj6, Scalelj12, Scalecoul
            END DO

            CLOSE(71)

         ELSE

            Ntemplate = Ntemplate + 1
            templatelist(Ntemplate) = Cmoltype(I)

         END IF

      END DO

 20   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') "Error: Wrong format of Molecule Type", Cmoltype(I)
         STOP
      END IF

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                              CCC
CCC                   TOPOLOGY                   CCC
CCC                                              CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      OPEN(88,file="INPUT/topology.in")

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C        Number of Molecules in Box(es)            C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

 30   WRITE(6,'(A)',ADVANCE="NO") "Number of Simulation Boxes (1 or 2): "
      READ(6,*,iostat=io,err=30) Nbox
      IF((Nbox.NE.1).AND.(Nbox.NE.2)) THEN
         WRITE(6,'(A)') "Invalid number"
         GO TO 30
      END IF

      WRITE(6,*)

      IF(Nbox.EQ.1) THEN
         WRITE(88,'(A8,<NM-5>x,A7)') TRIM("Molecule"), "# Box 1"
         molinbox="Number of molecules in box, "
      ELSE
         WRITE(88,'(A8,<NM-5>x,A17)') TRIM("Molecule"), "# Box 1   # Box 2"
         molinbox="Number of molecules in box 1 and box 2, "
      END IF

      DO I=1,Nmoltype
  40     WRITE(6,'(A,1x,A<NM>,A3)',ADVANCE="NO") TRIM(molinbox), ADJUSTL(Cmoltype(I)), " : "
         READ(6,*,iostat=io,err=40) (Nmolinbox(J), J=1,Nbox)
         WRITE(88,'(A<NM>,4x,i4,6x,i4)') Cmoltype(I),(Nmolinbox(J), J=1,Nbox)
      END DO

      WRITE(88,'(A)') "################################################################"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C               Partition Functions                C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      WRITE(6,'(A)',ADVANCE="NO") "Use Partition Functions? (y/n): "
      READ(6,*) answer
      IF(answer(1:1).EQ."y") THEN
         WRITE(88,'(A)') "Partition Functions"
         DO I=1,Nmoltype
            WRITE(88,'(A<NM>,4x,A7)') Cmoltype(I), "#-#-#-#"
         END DO
         Lpartition=.true.
         WRITE(88,'(A)') "################################################################"
      ELSE
         Lpartition=.false.
      END IF

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C                   Fugacities                     C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      WRITE(6,'(A)',ADVANCE="NO") "Use Fugacity Coefficients? (y/n): "
      READ(6,*) answer
      IF(answer(1:1).EQ."y") THEN
         WRITE(88,'(A)') "Fugacity Coefficients"
         DO I=1,Nmoltype
            WRITE(88,'(A<NM>,4x,A7)') Cmoltype(I), "#-#-#-#"
         END DO
         Lfugacity=.true.
         WRITE(88,'(A)') "################################################################"
      ELSE
         Lfugacity=.false.
      END IF

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C                  Fractionals                     C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

  50  WRITE(6,'(A)',ADVANCE="NO") "Number of Fractional Groups (excluding reactions): "
      READ(6,*,iostat=io,err=50) Nfractype
      IF(Nfractype.LT.0) THEN
         WRITE(6,'(A)') "Number of Fractionals should be 0 or more"
         GO TO 50
      END IF
      WRITE(88,'(i1,A)') Nfractype, " Fractional Group(s)"
      WRITE(88,'(A)') "Type, Box, NinFrac, MoleculeTypes and Name"

      DO I=1,Nfractype
         WRITE(6,*)
         WRITE(6,'(A,i3)') "Fractional Group ", I
  51     WRITE(6,'(A)',ADVANCE="NO") " Fractional Type (NVT/NPT, GE or GC): "
         READ(6,*,iostat=io,err=51) Cfractype

         IF(Nbox.EQ.2) THEN
  52        WRITE(6,'(A)',ADVANCE="NO") " Fractional (starts) in Box:          "
            READ(6,*,iostat=io,err=52) Istartinbox
            IF((Istartinbox.LE.0).OR.(Istartinbox.GE.3)) THEN
               WRITE(6,'(A)') " Box number should be 1 or 2"
               GO TO 52
            END IF
         ELSE
            Istartinbox = 1
         END IF

  53     WRITE(6,'(A)',ADVANCE="NO") " Number of Molecules in Fractional:   "
         READ(6,*,iostat=io,err=53) Nmolinfrac
         IF(Nmolinfrac.LE.0) THEN
            WRITE(6,'(A)') " Number of Molecules in Fractional should be 1 or more"
            GO TO 53
         END IF

  54     WRITE(6,'(A)',ADVANCE="NO") " Enter Molecule Type number(s):       "
         READ(6,*,iostat=io,err=54) (Imoltype(J), J=1,Nmolinfrac)
  55     WRITE(6,'(A)',ADVANCE="NO") " Name of fractional group             "
         READ(6,*,iostat=io,err=55) Cfracname

         WRITE(88,'(A3,4x,i1,6x,i1,5x,9(A,1x))') Cfractype, Istartinbox, Nmolinfrac,
     &     (TRIM(Cmoltype(Imoltype(J))),  J=1,Nmolinfrac), TRIM(Cfracname)
      END DO

      WRITE(88,'(A)') "################################################################"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C             Reactions + Fractionals              C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

  60  WRITE(6,'(A)',ADVANCE="NO") "Number of Reactions: "
      READ(6,*,iostat=io,err=60) Nreactions
      IF(Nreactions.LT.0) THEN
         WRITE(6,'(A)') "Number of Reactions should be 0 or positive"
         GO TO 60
      END IF

      WRITE(88,'(i1,A)') Nreactions, " Reaction(s)"

      DO I=1,Nreactions

         WRITE(88,'(A)') "-------------------------------------------------------"

         WRITE(6,*)
  61     WRITE(6,'(A,i1,A)',ADVANCE="NO") "Number of Molecule Types in Reaction ", I, ": "
         READ(6,*,iostat=io,err=61) Nmoltypinreac
         IF(Nmoltypinreac.LE.0) THEN
            WRITE(6,'(A)') "Number should be 1 or larger"
            GO TO 61
         END IF

         Nstoiprev = 0

         IF(Nbox.EQ.2) THEN
  62        WRITE(6,'(A)',ADVANCE="NO") "Reaction in Box: "
            READ(6,*,iostat=io,err=62) Istartinbox
            IF((Istartinbox.LE.0).OR.(Istartinbox.GE.3)) THEN
               WRITE(6,'(A)') " Box number should be 1 or 2"
               GO TO 62
            END IF
         ELSE
            Istartinbox = 1
         END IF

         WRITE(88,'(i1,A,i1)') Nmoltypinreac, " MolTypes in Reaction in box ", Istartinbox
         WRITE(88,'(A8,<NM+4>x,A5)') "MolType", "Nstoi"

         DO J=1,Nmoltypinreac
  63        WRITE(6,'(i1,A)',ADVANCE="NO") J,") Molecule Type Number and Stoichiometric Coefficient: "
            READ(6,*,iostat=io,err=63) Tm, Nstoi
            IF(Tm.LE.0) THEN
               WRITE(6,'(A)') "Molecule Type should be a positive integer"
               GO TO 63
            END IF
            WRITE(88,'(A<NM>,7x,i2)') Cmoltype(Tm), Nstoi
            Nstoiprev=Nstoi
         END DO

      END DO

      WRITE(88,'(A)') "################################################################"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C      BoxSize + dVolume for MC Volume Change      C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      WRITE(88,'(A)') "Box   Length   dVolume  Gamma_Translation  Gamma_Rotation"
      IF(Nbox.EQ.1) THEN
  70     WRITE(6,'(A)',ADVANCE="NO") "Box length: "
         READ(6,*,iostat=io,err=70) Box(1)
         IF(Box(1).LT.0.0d0) THEN
            WRITE(6,'(A)') "Box length should be positive"
            GO TO 70
         END IF
      ELSEIF(Nbox.EQ.2) THEN
  71     WRITE(6,'(A)',ADVANCE="NO") "Box lengths of Box 1 and Box 2: "
         READ(6,*,iostat=io,err=71) Box(1), Box(2)
         IF((Box(1).LT.0.0d0).OR.(Box(2).LT.0.0d0)) THEN
            WRITE(6,'(A)') "Box lengths should be positive"
            GO TO 71
         END IF
      END IF

      DO I=1,Nbox
         WRITE(88,'(1x,i1,3x,f7.3,1x,f8.2,9x,A,14x,A)') I, Box(I), 0.01d0*Box(I)**3, "0.15", "0.15"
      END DO

      WRITE(88,'(A)') "----------------------------------------------------------------"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C  Maximum translation, rotation and lambda change C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      IF(Nbox.EQ.1) THEN
         WRITE(88,'(<NM+6>x,A)') "Box 1"
         WRITE(88,'(A7,<NM-6>x,A)') "MolType", "dtrans  drotation"
      ELSE
         WRITE(88,'(<NM+6>x,A)') "Box 1               Box 2"
         WRITE(88,'(A7,<NM-6>x,A)') "MolType", "dtrans  drotation     dtrans  drotation"
      END IF

  80  WRITE(6,'(A)',ADVANCE="NO") "Manually input the maximum displacement parameters? (y/n): "
      READ(6,*,iostat=io,err=80) answer
      IF((answer(1:1).EQ.'n').OR.(answer(1:1).EQ.'N')) THEN
         DO I=1,Nmoltype
            IF(Nbox.EQ.1) THEN
               WRITE(88,'(A<NM>,1x,f5.2,5x,A)')  Cmoltype(I), 0.02*Box(1), "90.0"
            ELSE
               WRITE(88,'(A<NM>,1x,f5.2,5x,A,7x,f5.2,5x,A)')  Cmoltype(I), 0.02*Box(1), "90.0", 0.3*Box(2), "180.0"
            END IF
         END DO
         WRITE(88,'(A)') "----------------------------------------------------------------"
         IF(Nbox.EQ.1) THEN
            WRITE(88,'(A)') "             Box 1"
            WRITE(88,'(A)') "Fractional  dlambda  N_LambdaBins   LambdaSwitch"
         ELSE
            WRITE(88,'(A)') "             Box 1     Box 2 "
            WRITE(88,'(A)') "Fractional  dlambda   dlambda  N_LambdaBins   LambdaSwitch"
         END IF
         DO I=1,(Nfractype+Nreactions)
            WRITE(88,'(4x,i1,9x,3(A,7x))') I, ("0.2", J=1,Nbox), "100           1.0"
         END DO
      ELSE
         IF(Nbox.EQ.2) THEN
            WRITE(6,'(A)',ADVANCE="NO") "Use same maximum displacements in both Boxes? (y/n): "
            READ(6,*) answer
         ELSE
            answer="No"
         END IF
         IF(answer(1:1).EQ.'n'.OR.answer(1:1).EQ.'N') THEN
            IF(Nbox.EQ.1) THEN
               delta="Deltas for Translation and Rotation for Molecule Type"
            ELSE
               delta="Deltas for Translation and Rotation in Box 1 and Box 2 for Molecule Type: "
            END IF
            DO I=1,Nmoltype
  81           WRITE(6,'(A,1x,A24,1x,A2)',ADVANCE="NO") TRIM(delta), Cmoltype(I), ": "
               READ(6,*,iostat=io,err=81) (dtrans(J), drot(J), J=1,Nbox)
               WRITE(88,'(A<NM>,1x,f5.2,5x,A,7x,f5.2,5x,A)') Cmoltype(I), (dtrans(J), drot(J), J=1,Nbox)
            END DO
            WRITE(88,'(A)') "----------------------------------------------------------------"
            IF(Nbox.EQ.1) THEN
               WRITE(88,'(A)') "             Box 1"
               WRITE(88,'(A)') "Fractional  dlambda"
               delta="Delta for Lambda Move for Fractional Group"
            ELSE
               WRITE(88,'(A)') "             Box 1     Box 2 "
               WRITE(88,'(A)') "Fractional  dlambda   dlambda"
               delta="Delta for Lambda Move in Box 1 and Box 2 for Fractional Group"
            END IF
            WRITE(6,*)
            DO I=1,(Nfractype+Nreactions)
  82           WRITE(6,'(A,1x,i1,1x,A2)',ADVANCE="NO") TRIM(delta), I, ": "
               READ(6,*,iostat=io,err=82) (dlambda(J), J=1,Nbox)
               IF(Nbox.EQ.1) THEN
                  WRITE(88,'(4x,i1,9x,f3.1,8x,A)') I, (dlambda(J), J=1,Nbox), "100            1.0"
               ELSE
                  WRITE(88,'(4x,i1,9x,f3.1,7x,f3.1,8x,A)') I, (dlambda(J), J=1,Nbox), "100            1.0"
               END IF
            END DO
         ELSE
            delta="Delta for Translation and Rotation for Molecule Type"
            DO I=1,Nmoltype
  83           WRITE(6,'(A,1x,i1,1x,A2)', ADVANCE="NO") TRIM(delta), I, ": "
               READ(6,*,iostat=io,err=83) dtrans(1), drot(1)
               WRITE(88,'(A24,6x,f5.2,3x,f5.1,7x,f4.2,4x,f5.1)') Cmoltype(I), (dtrans(1), drot(1), J=1,Nbox)
            END DO
            WRITE(88,'(A)') "----------------------------------------------------------------"
            WRITE(6,*)
            delta="Delta for Lambda Move for Fractional Group"
            IF(Nbox.EQ.1) THEN
               WRITE(88,'(A)') "           Box 1"
               WRITE(88,'(A)') "FracType  dlambda"
            ELSE
               WRITE(88,'(A)') "           Box 1     Box 2 "
               WRITE(88,'(A)') "FracType  dlambda   dlambda"
            END IF
            DO I=1,(Nfractype+Nreactions)
  84           WRITE(6,'(A,1x,i1,1x,A2)',ADVANCE="NO") TRIM(delta), I, ": "
               READ(6,*,iostat=io,err=84) dlambda(1)
               WRITE(88,'(3x,i1,8x,f3.1,7x,f3.1)') I, (dlambda(1), J=1,Nbox)
            END DO
         END IF
      END IF

      WRITE(88,'(A)') "################################################################"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C             Pair and Cluster Moves               C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      IF(Nmoltype.GT.1) THEN
  90     WRITE(6,'(A)',ADVANCE="NO") "Number of Molecule Type Pairs: "
         READ(6,*,iostat=io,err=90) Npairs
         WRITE(6,*)
         IF(Npairs.LT.0) THEN
            WRITE(6,'(A)') "Number of Molecule Type Pairs should be 0 or more"
            GO TO 90
         END IF
      ELSE
         Npairs = 0
      END IF

      WRITE(88,'(i1,A)') Npairs, " MolType Pair(s)"

      DO I=1,Npairs
  91     WRITE(6,'(A,i1,A)',ADVANCE="NO") " Molecule Type numbers in Pair ", I, ": "
         READ(6,*,iostat=io,err=91) J, K
         IF((J.LE.0).OR.(K.LE.0)) THEN
            WRITE(6,'(A)') "Molecule Types should be positive"
            GO TO 91
         ELSEIF((J.GT.Nmoltype).OR.(K.GT.Nmoltype)) THEN
            WRITE(6,'(A)') "Molecule Types should be smaller than the number of Molecule Types"
            GO TO 91
         END IF
         WRITE(88,'(i3,1x,i3)') Cmoltype(J), Cmoltype(K)
      END DO

      WRITE(88,'(A)') "----------------------------------------------------------------"
      WRITE(88,'(A)') "Cluster Radius = 0.0"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;92mTopology created.\033[0m"
      WRITE(6,*)



CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                              CCC
CCC                  FORCEFIELD                  CCC
CCC                                              CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      OPEN(89,file="INPUT/forcefield.in")

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C               Atom interactions                  C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      WRITE(89,'(i3,A)') Natomsinlist, " atomtype(s)"
      WRITE(89,'(A4,<NA+4>x,A)') "Atom", "Sigma     Epsilon    Charge  LJ? EL? Print"

      OPEN(72,file=TRIM(BRICKDIR)//"/FORCEFIELD/atoms")
      DO
         READ(72,*,IOSTAT=io) Catom,Sig,Eps,Q,Llj,Lcoul,Symb
         IF(io.EQ.0) THEN
            CALL CHECK_ATOMLIST(Catom,atomlist,Natomsinlist,Lcheck)
            IF(Lcheck) THEN
               WRITE(89,'(A<NA>,4x,f10.5,1x,f10.5,1x,f10.6,2x,L1,3x,L1,4x,A7)')
     &                    Catom,Sig,Eps,Q,Llj,Lcoul,ADJUSTL(Symb)
            END IF
         ELSEIF(io.GT.0) THEN
            CYCLE
         ELSE
            EXIT
         END IF
      END DO

      CLOSE(72)

      WRITE(89,'(A)') "---------------------------------------------------------------------------------------"

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C                     Bendings                     C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      WRITE(89,'(i3,A)') Nbendinginlist, " bendingtype(s)"
      WRITE(89,'(A8,<NB-4>x,A)') "Bending ", "theta0     K    dtheta"

      OPEN(72,file=TRIM(BRICKDIR)//"/FORCEFIELD/bendings")
      DO
         READ(72,*,IOSTAT=io) Cbend, theta0, K_bend, dtheta
         IF(io.EQ.0) THEN
            CALL CHECK_BENDINGLIST(Cbend,bendinglist,Nbendinginlist,Lcheck,K)
            IF(Lcheck) THEN
               WRITE(89,'(A<NB>,4x,f6.2,2x,f8.2,1x,f6.2)') ADJUSTL(Cbend), theta0, K_bend, dtheta
            END IF
         ELSEIF(io.GT.0) THEN
            CYCLE
         ELSE
            EXIT
         END IF
      END DO

      CLOSE(72)

      WRITE(89,'(A)') "---------------------------------------------------------------------------------------"

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C                    Torsions                      C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      WRITE(89,'(i3,A)') Ntorsioninlist, " torsiontype(s)"
      WRITE(89,'(A8,<NT-2>x,A)') "Torsion ", "A0        A1        A2        A3        A4        A5    dphi"

      OPEN(72,file=TRIM(BRICKDIR)//"/FORCEFIELD/torsions")
      DO
         READ(72,*,IOSTAT=io) Ctors

         IF(io.EQ.0) THEN

            IF(Ctors(1:1).EQ.'T'.OR.Ctors(1:1).EQ.'t') THEN
               Ncoeff = 4
            ELSEIF(Ctors(1:1).EQ.'O'.OR.Ctors(1:1).EQ.'o') THEN
               Ncoeff = 4
            ELSEIF(Ctors(1:1).EQ.'R'.OR.Ctors(1:1).EQ.'r') THEN
               Ncoeff = 6
            ELSE
               Ncoeff = 6
            ENDIF

            BACKSPACE(72)
            READ(72,*,IOSTAT=io) Ctors, (Ators(I), I=1,Ncoeff), dphi
            IF(io.NE.0) CYCLE

            CALL CHECK_TORSIONLIST(Ctors,torsionlist,Ntorsioninlist,Lcheck,K)
            IF(Lcheck) THEN
               IF(Ncoeff.EQ.4) THEN
                  WRITE(89,'(A<NT>,1x,4(f9.3,1x),20x,f5.1)') ADJUSTL(Ctors), (Ators(I), I=1,Ncoeff), dphi
               ELSEIF(Ncoeff.EQ.5) THEN
                  WRITE(89,'(A<NT>,1x,5(f9.3,1x),10x,f5.1)') ADJUSTL(Ctors), (Ators(I), I=1,Ncoeff), dphi
               ELSEIF(Ncoeff.EQ.6) THEN
                  WRITE(89,'(A<NT>,1x,6(f9.3,1x),f5.1)')  ADJUSTL(Ctors), (Ators(I), I=1,Ncoeff), dphi
               END IF
            END IF

         ELSEIF(io.GT.0) THEN

            CYCLE

         ELSE

            EXIT

         END IF

      END DO

      CLOSE(72)

      WRITE(89,'(A)') "#######################################################################################"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
      WRITE(6,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                  C
C               Forcefield details                 C
C                                                  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      IF(Nbox.EQ.2) THEN
         WRITE(6,'(A)') "Answer the following questions for both Simulation Boxes"
      END IF


      Lidealgas = .false.
      WRITE(6,'(A)',ADVANCE="NO") "Ideal Gas? (y/n):                      "
      READ(6,*) (answer_array(I), I=1,Nbox)

      IF(Nbox.EQ.1) THEN
         WRITE(89,'(A)') "                       Box 1"
         IF(answer_array(1)(1:1).EQ.'y') THEN
            WRITE(89,'(A)') "Ideal_Gas              .true."
            Lidealgas = .true.
         ELSE
            WRITE(89,'(A)') "Ideal_Gas             .false."
         END IF
      ELSE
         WRITE(89,'(A)') "                       Box 1     Box 2"
         IF((answer_array(1)(1:1).EQ.'y').AND.(answer_array(2)(1:1).EQ.'y')) THEN
            WRITE(89,'(A)') "Ideal_Gas              .true.    .true."
            Lidealgas = .true.
         ELSEIF((answer_array(1)(1:1).EQ.'y').AND.(answer_array(2)(1:1).EQ.'n')) THEN
            WRITE(89,'(A)') "Ideal_Gas              .true.    .false."
         ELSEIF((answer_array(1)(1:1).EQ.'n').AND.(answer_array(2)(1:1).EQ.'y')) THEN
            WRITE(89,'(A)') "Ideal_Gas              .false.   .true."
         ELSE
            WRITE(89,'(A)') "Ideal_Gas              .false.   .false."
         END IF
      END IF

      IF(.NOT.Lidealgas) THEN
100      WRITE(6,'(A)',ADVANCE="NO") "Cutoff Radius for LJ:                  "
         READ(6,*,iostat=io,err=100) (Rljcut(I), I=1,Nbox)
         WRITE(89,'(A,f5.2,5x,f5.2)') "Cutoff_LJ_Energy       ",  (Rljcut(I), I=1,Nbox)

         LEwald=.false.
101      WRITE(6,'(A)',ADVANCE="NO") "Electrostatic Method? (Wolf/FG/Ewald/None): "
         READ(6,*,iostat=io,err=101) (answer_array(I), I=1,Nbox)

         IF((answer_array(1)(1:1).EQ.'W').OR.(answer_array(1)(1:1).EQ.'w')) THEN
            WRITE(89,'(A,$)') "Method_EL_Energy       Wolf "
         ELSEIF((answer_array(1)(1:1).EQ.'F').OR.(answer_array(1)(1:1).EQ.'f')) THEN
            WRITE(89,'(A,$)') "Method_EL_Energy        FG  "
         ELSEIF((answer_array(1)(1:1).EQ.'E').OR.(answer_array(1)(1:1).EQ.'e')) THEN
            WRITE(89,'(A,$)') "Method_EL_Energy       Ewald"
            LEwald=.true.
         ELSE
            WRITE(89,'(A,$)') "Method_EL_Energy       none "
         END IF

         IF(Nbox.EQ.2) THEN
            IF((answer_array(2)(1:1).EQ.'W').OR.(answer_array(2)(1:1).EQ.'w')) THEN
               WRITE(89,'(A)') "     Wolf"
            ELSEIF((answer_array(2)(1:1).EQ.'F').OR.(answer_array(2)(1:1).EQ.'f')) THEN
               WRITE(89,'(A)') "      FG"
            ELSEIF((answer_array(2)(1:1).EQ.'E').OR.(answer_array(2)(1:1).EQ.'e')) THEN
               WRITE(89,'(A)') "     Ewald"
               LEwald=.true.
            ELSE
               WRITE(89,'(A)') "     none"
            END IF
         ELSE
            WRITE(89,*)
         END IF

         IF(LEwald) THEN
            WRITE(6,*)
            EwaldPrecision=1.0D-6
            DO I=1,Nbox
               Rcoulcut(I) = 0.4d0*Box(I)
               tol = dsqrt(abs(log(EwaldPrecision*Rcoulcut(I))))
               Alpha(I) = dsqrt(abs(log(EwaldPrecision*Rcoulcut(I)*tol)))/Rcoulcut(I)
               tol1 = dsqrt(-log(EwaldPrecision*Rcoulcut(I)*4.0d0*tol*tol*Alpha(I)*Alpha(I)))
               Kmax(I) = NINT(0.25d0+Box(I)*Alpha(I)*tol1/OnePi)
            END DO
            WRITE(6,'(A)') "Suggested Parameters for Ewald Method: (precision = 1E-6)"
            IF(Nbox.EQ.1) THEN
               WRITE(6,'(A)') "---------------------------------------------------------"
            ELSE
               WRITE(6,'(A)') "--------- Box 1  ---  Box 2 ------------------------------"
            END IF
            WRITE(6,'(A,f6.2,6x,f6.2)') "Cutoff = ", (Rcoulcut(I), I=1,Nbox)
            WRITE(6,'(A,f9.6,3x,f9.6)') "Alpha  = ", (Alpha(I), I=1,Nbox)
            WRITE(6,'(A,i5,3x,i5)')     "Kmax   = ", (Kmax(I), I=1,Nbox)
            WRITE(6,*)
         END IF

102      WRITE(6,'(A)',ADVANCE="NO") "Cutoff Radius for Electrostatics:      "
         READ(6,*,iostat=io,err=102) (Rcoulcut(I), I=1,Nbox)
         WRITE(89,'(A,f5.2,5x,f5.2)') "Cutoff_EL_Energy       ", (Rcoulcut(I), I=1,Nbox)

         WRITE(6,'(A)',ADVANCE="NO") "Alpha for Electrostatics:              "
103      READ(6,*,iostat=io,err=103) (Alpha(I), I=1,Nbox)
         WRITE(89,'(A,f9.6,1x,f9.6)') "Alpha_EL_Energy        ", (Alpha(I), I=1,Nbox)

         IF(LEwald) THEN
104            WRITE(6,'(A)',ADVANCE="NO") "Kmax for Ewald:                        "
            READ(6,*,iostat=io,err=104) (Kmax(I), I=1,Nbox)
            WRITE(89,'(A,i5,5x,i5)')  "Kmax_Ewald             ", (Kmax(I), I=1,Nbox)
         END IF
      END IF

      WRITE(89,'(A)') "---------------------------------------------------------------------------------------"

      IF(.NOT.LidealGas) THEN
         WRITE(6,*)
         WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"
         WRITE(6,*)

         WRITE(6,'(A)',ADVANCE="NO") "LJ Tailcorrections? (y/n):             "
         READ(6,*) answer
         IF(answer(1:1).EQ.'y') THEN
            WRITE(89,'(A)') "LJ_Truncation           Tailcorrections"
         ELSE
            WRITE(6,'(A)',ADVANCE="NO") "LJ Shifted Potential? (y/n):           "
            READ(6,*) answer
            IF(answer(1:1).EQ.'y') THEN
               WRITE(89,'(A)') "LJ_Truncation           Shifted"
            ELSE
               WRITE(89,'(A)') "LJ_Truncation           none"
            END IF
         END IF

         WRITE(6,'(A)',ADVANCE="NO") "Lorentz Berthelot Mixing Rules? (y/n): "
         READ(6,*) answer
         IF(answer(1:1).EQ.'y') THEN
            WRITE(89,'(A)') "Mixing_Rules            Lorentz-Berthelot"
         ELSE
            WRITE(6,'(A)') "Jorgensen Mixing Rules used"
            WRITE(89,'(A)') "Mixing_Rules            Jorgensen"
         END IF
      END IF

      WRITE(89,'(A)') "---------------------------------------------------------------------------------------"
      WRITE(89,'(A)') "0 overrides"
      WRITE(89,'(A)') "atom 1  atom 2  Sigma   Epsilon   Rmin"
      WRITE(89,'(A)') "---------------------------------------------------------------------------------------"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;92mForcefield created.\033[0m"
      WRITE(6,*)



CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                              CCC
CCC                 FINALIZING                   CCC
CCC                                              CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      OPEN(4,file=".molecules_to_copy")
      OPEN(5,file=".templates_to_copy")
      DO I=1,Nmoltype
         IF(Lcheckmol(I)) THEN
            WRITE(4,'(A)') Cmoltype(I)
         ELSE
            WRITE(5,'(A)') Cmoltype(I)
         END IF
      END DO
      CLOSE(4)
      CLOSE(5)

      IF(Ntemplate.NE.0) THEN
         WRITE(6,'(A)') "\033[1;91mWarning\033[0m: One or more empty molecule templates are added to the INPUT folder because they"
         WRITE(6,'(A)') "are not found in the MOLECULES folder. The following molecules have an empty template that"
         WRITE(6,'(A)') "should be filled in before starting simulations:"
         DO I=1,Ntemplate
            WRITE(6,'(1x,A24)') ADJUSTL(templatelist(I))
         END DO
      END IF

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33mModify the settings.in file manually to set simulation details.\033[0m"
      IF(Lpartition) WRITE(6,'(A)') "\033[1;33mModify the topology file manually to set Partition Functions.\033[0m"
      IF(Lfugacity)  WRITE(6,'(A)') "\033[1;33mModify the topology file manually to set Fugacities.\033[0m"
      WRITE(6,*)

      STOP


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                              CCC
CCC                SUBROUTINES                   CCC
CCC                                              CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      CONTAINS


      SUBROUTINE CHECK_ATOMLIST(Catom,atomlist,Natomsinlist,Lcheck)
      implicit none

      integer        Natomsinlist,I
      character*24   Catom,atomlist(100)
      logical        Lcheck

      Lcheck=.false.

      DO I=1,Natomsinlist
         IF(Catom.EQ.atomlist(I)) THEN
            Lcheck=.true.
            EXIT
         END IF
      END DO

      END SUBROUTINE


      SUBROUTINE CHECK_BENDINGLIST(Cbend,bendinglist,Nbendinginlist,Lcheck,J)
      implicit none

      integer        Nbendinginlist,I,J
      character*24   Cbend,bendinglist(100)
      logical        Lcheck

      Lcheck=.false.

      DO I=1,Nbendinginlist
         IF(Cbend.EQ.bendinglist(I)) THEN
            Lcheck=.true.
            J=I
            EXIT
         END IF
      END DO

      END SUBROUTINE


      SUBROUTINE CHECK_TORSIONLIST(Ctors,torsionlist,Ntorsioninlist,Lcheck,J)
      implicit none

      integer        Ntorsioninlist,I,J
      character*24   Ctors,torsionlist(100)
      logical        Lcheck

      Lcheck=.false.

      DO I=1,Ntorsioninlist
         IF(Ctors.EQ.torsionlist(I)) THEN
            Lcheck=.true.
            J=I
            EXIT
         END IF
      END DO

      END SUBROUTINE


C     Create an alkane molecule-structure (maximum 78 beads)
      SUBROUTINE CREATE_ALKANE(Nbeads,alkane)
      implicit none

      integer           Nbeads,Nbonds,Nbendings,Ntors,I,NC,NH
      double precision  dx,dy,mass
      character*24      alkane
      character*100     moleculefile

      Parameter (dx = 1.291552674635953d0)
      Parameter (dy = 0.838744113923142d0)

      IF(Nbeads.LE.0.OR.Nbeads.GE.79) STOP "ERROR, number of beads"

      NC=Nbeads
      NH=2*NC+2

      mass=12.0107d0*dble(NC)+1.00794d0*dble(NH)

      IF(NC.EQ.1) THEN
         alkane="CH4"
      ELSEIF(NC.LE.9) THEN
         IF(NH.LE.9) THEN
            WRITE(alkane,'(A1,i1,A1,i1)') "C",NC,"H",NH
         ELSE
            WRITE(alkane,'(A1,i1,A1,i2)') "C",NC,"H",NH
         END IF
      ELSE
         IF(NH.LE.99) THEN
            WRITE(alkane,'(A1,i2,A1,i2)') "C",NC,"H",NH
         ELSE
            WRITE(alkane,'(A1,i2,A1,i3)') "C",NC,"H",NH
         END IF
      END IF

      moleculefile=TRIM(BRICKDIR) // "/MOLECULES/" // TRIM(alkane)
      moleculefile=TRIM(moleculefile)

      OPEN(91,file=moleculefile)
      WRITE(91,'(f7.3,A)') mass, " g/mol"

      WRITE(91,'(A)') "------------------------------------------------------"

      IF(Nbeads.LE.9) THEN
         WRITE(91,'(i1,1x,a7)') Nbeads, "atoms"
      ELSE
         WRITE(91,'(i2,1x,a7)') Nbeads, "atoms"
      END IF

      IF(Nbeads.EQ.1) THEN
         WRITE(91,'(A)') "CH4   0.000000000   0.000000000   0.000000000"
      ELSE
         DO I=1,Nbeads
            IF(I.EQ.1) THEN
               WRITE(91,'(A)') "CH3   0.000000000   0.000000000   0.000000000"
            ELSEIF(I.EQ.Nbeads) THEN
               WRITE(91,'(A3,3(2x,f12.9))') "CH3", dble(I-1)*dx, dble(MOD(I-1,2))*dy, 0.0d0
            ELSE
               WRITE(91,'(A3,3(2x,f12.9))') "CH2", dble(I-1)*dx, dble(MOD(I-1,2))*dy, 0.0d0
            END IF
         END DO
      END IF

      WRITE(91,'(A)') "------------------------------------------------------"

      Nbonds=Nbeads-1

      IF(Nbonds.LE.9) THEN
         WRITE(91,'(i1,A)') Nbonds, " bond(s)"
      ELSE
         WRITE(91,'(i2,A)') Nbonds, " bond(s)"
      END IF

      DO I=1,Nbonds
         WRITE(91,'(i2,1x,i2)') I, I+1
      END DO

      WRITE(91,'(A)') "------------------------------------------------------"

      Nbendings=max(0,Nbeads-2)

      IF(Nbendings.LE.9) THEN
         WRITE(91,'(i1,A)') Nbendings, " bending(s)"
      ELSE
         WRITE(91,'(i2,A)') Nbendings, " bending(s)"
      END IF

      DO I=1,Nbendings
         WRITE(91,'(A,3(i2,1x),A)') "TraPPE_C-C-C ", (I+J-1, J=1,3), "T"
      END DO

      WRITE(91,'(A)') "------------------------------------------------------"

      Ntors=max(0,Nbeads-3)

      IF(Ntors.LE.9) THEN
         WRITE(91,'(i1,A)') Ntors, " torsion(s)"
      ELSE
         WRITE(91,'(i2,A)') Ntors, " torsion(s)"
      END IF

      DO I=1,Ntors
         WRITE(91,'(A,4(i2,1x),A)') "TraPPE_X-CH2-CH2-X ", (I+J-1, J=1,4), "T"
      END DO

      WRITE(91,'(A)') "------------------------------------------------------"
      WRITE(91,'(A)') "0 intramolecular interaction(s)"

      END SUBROUTINE

      END
