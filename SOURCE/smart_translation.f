      SUBROUTINE Smart_Translation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "output.inc"

C     Displace molecules based on the force acting on them

      integer Imol,Ib,Tm,I,Select_Random_Integer,Iatom
      double precision dE,XCMold(MaxMol),YCMold(MaxMol),ZCMold(MaxMol),
     &      Xold(MaxMol,MaxAtom),Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),F_x,F_y,F_z,
     &      E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,E_BendingNew,
     &      E_TorsionNew,E_EL_FourNew,Eold,Enew,dX(MaxMol),dY(MaxMol),dZ(MaxMol),Avd,
     &      s_old,s_new,ds,Ran_Gauss,E_LJ_TailOld,E_LJ_TailNew,E_EL_SelfOld,E_EL_SelfNew
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept


      Ib = Select_Random_Integer(N_Box)

      TrialSmartTranslation(Ib) = TrialSmartTranslation(Ib) + 1.0d0

C     Save old configuration
      E_LJ_TailOld = U_LJ_Tail(Ib)
      E_EL_SelfOld = U_EL_Self(Ib)

      Eold = U_Total(Ib)

      IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)

C     Calculate 's' of old configuration for acceptance rule
      s_old = 0.0d0

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Imol,Iatom) = X(Imol,Iatom)
            Yold(Imol,Iatom) = Y(Imol,Iatom)
            Zold(Imol,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(Imol) = XCM(Imol)
         YCMold(Imol) = YCM(Imol)
         ZCMold(Imol) = ZCM(Imol)

         CALL Force_Molecule(Imol,F_x,F_y,F_z,L_Overlap_Inter,L_Overlap_Intra)

         dX(I) = beta*A_Translation(Ib)*F_x + Ran_Gauss()*Gamma_Translation(Ib)
         dY(I) = beta*A_Translation(Ib)*F_y + Ran_Gauss()*Gamma_Translation(Ib)
         dZ(I) = beta*A_Translation(Ib)*F_z + Ran_Gauss()*Gamma_Translation(Ib)

         s_old = s_old - (beta*A_Translation(Ib)*F_x - dX(I))*(beta*A_Translation(Ib)*F_x - dX(I))
     &                 - (beta*A_Translation(Ib)*F_y - dY(I))*(beta*A_Translation(Ib)*F_y - dY(I))
     &                 - (beta*A_Translation(Ib)*F_z - dZ(I))*(beta*A_Translation(Ib)*F_z - dZ(I))

      END DO

C     Generate new configuration
      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            X(Imol,Iatom) = X(Imol,Iatom) + dX(I)
            Y(Imol,Iatom) = Y(Imol,Iatom) + dY(I)
            Z(Imol,Iatom) = Z(Imol,Iatom) + dZ(I)
         END DO

         XCM(Imol) = XCM(Imol) + dX(I)
         YCM(Imol) = YCM(Imol) + dY(I)
         ZCM(Imol) = ZCM(Imol) + dZ(I)

         CALL Place_molecule_back_in_box(Imol)

      END DO

C     Calculate 's' of new configuration for acceptance rule
      s_new = 0.0d0

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)

         CALL Force_Molecule(Imol,F_x,F_y,F_z,L_Overlap_Inter,L_Overlap_Intra)

         s_new = s_new - (beta*A_Translation(Ib)*F_x + dX(I))*(beta*A_Translation(Ib)*F_x + dX(I))
     &                 - (beta*A_Translation(Ib)*F_y + dY(I))*(beta*A_Translation(Ib)*F_y + dY(I))
     &                 - (beta*A_Translation(Ib)*F_z + dZ(I))*(beta*A_Translation(Ib)*F_z + dZ(I))
      END DO

C     Calculate energy of new configuration
      CALL Energy_Total(Ib,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,
     &                     E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Smart Translation)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF

      E_EL_FourNew = 0.0d0
      IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew)

      CALL Energy_Correction(Ib)

      E_LJ_TailNew = U_LJ_Tail(Ib)
      E_EL_SelfNew = U_EL_Self(Ib)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
     &     + E_TorsionNew  + E_LJ_TailNew  + E_EL_SelfNew + E_EL_ExclNew  + E_EL_FourNew

      dE = Enew - Eold
      ds = (s_new - s_old)/(4.0d0*A_Translation(Ib))

      CALL Accept_or_Reject(dexp(-beta*dE + ds),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         AcceptSmartTranslation(Ib) = AcceptSmartTranslation(Ib) + 1.0d0

         U_LJ_Inter(Ib) = E_LJ_InterNew
         U_LJ_Intra(Ib) = E_LJ_IntraNew

         U_EL_Real(Ib)  = E_EL_RealNew
         U_EL_Intra(Ib) = E_EL_IntraNew
         U_EL_Excl(Ib)  = E_EL_ExclNew
         U_EL_Four(Ib)  = E_EL_FourNew
         U_EL_Self(Ib)  = E_EL_SelfNew

         U_Bending_Total(Ib) = E_BendingNew
         U_Torsion_Total(Ib) = E_TorsionNew

         U_Total(Ib) = U_Total(Ib) + dE

         Avd = 0.0d0
         DO I=1,N_MolInBox(Ib)
            Avd = Avd + dsqrt(dX(I)*dX(I)+dY(I)*dY(I)+dZ(I)*dZ(I))
         END DO
         AvDelta_AcceptSmartTranslation(Ib) = AvDelta_AcceptSmartTranslation(Ib) + Avd/dble(N_MolInBox(Ib))

      ELSE

         U_LJ_Tail(Ib)  = E_LJ_TailOld
         U_EL_Self(Ib)  = E_EL_SelfOld

         DO I=1,N_MolInBox(Ib)
            Imol=I_MolInBox(Ib,I)

            DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
               X(Imol,Iatom) = Xold(Imol,Iatom)
               Y(Imol,Iatom) = Yold(Imol,Iatom)
               Z(Imol,Iatom) = Zold(Imol,Iatom)
            END DO

            XCM(Imol) = XCMold(Imol)
            YCM(Imol) = YCMold(Imol)
            ZCM(Imol) = ZCMold(Imol)
         END DO

         IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,2)

      END IF

      RETURN
      END
