      SUBROUTINE Bending
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "bending.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"

      integer Imol,Ib,Tm,Ibend,Tb,Il,I,Iatom1,Iatom2,Iatom3,Iatom,Select_Random_Integer,It,J,Ifrac
      double precision Xold(MaxAtom),Yold(MaxAtom),Zold(MaxAtom),XCMold,YCMold,ZCMold,factor,
     &   phi,s,c,ux,uy,uz,vx,vy,vz,nx,ny,nz,Ox,Oy,Oz,Xo,Yo,Zo,unorm,vnorm,nnorm,Ran_Uniform,dE,
     &   E_LJ_InterOld,E_LJ_IntraOld,E_EL_RealOld,E_EL_IntraOld,E_EL_ExclOld,E_BendingOld,
     &   E_TorsionOld,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,
     &   E_BendingNew,E_TorsionNew,costheta_old,costheta_new,Eold,Enew,dE_EL_Four,Myl,Myc,Myi,Rm
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                            CCC
CCC    Select a random Molecule, select a random Bending       CCC
CCC     in this Molecule and give it a random Bending          CCC
CCC                                                            CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      Rm = Ran_Uniform()
      IF(Rm.LT.0.95d0) THEN
         Imol = Select_Random_Integer(N_MolTotal)
      ELSE
         IF(N_Frac.EQ.0) RETURN
         Ifrac = Select_Random_Integer(N_Frac)
         I     = Select_Random_Integer(N_MolInFrac(Ifrac))
         Imol  = I_MolInFrac(Ifrac,I)
      END IF

      Ib = Ibox(Imol)
      Tm = TypeMol(Imol)

      IF(N_BendingInMolType(Tm).EQ.0) RETURN

      Ibend = Select_Random_Integer(N_BendingInMolType(Tm))
      Tb    = TypeBending(Tm,Ibend)
      Il    = Select_Random_Integer(2) !List or complementary list (which side of the molecule will bend)

      LEwald = .false.
      IF(L_ChargeInMolType(Tm).AND.L_Ewald(Ib)) LEwald = .true.

      IF(LEwald) CALL Ewald_Init

      TrialBending(Ib,Tm,Ibend) = TrialBending(Ib,Tm,Ibend) + 1.0d0

CCC   Store old configuration and calculate energy
      DO I=1,N_AtomInMolType(Tm)
         Xold(I) = X(Imol,I)
         Yold(I) = Y(Imol,I)
         Zold(I) = Z(Imol,I)
      END DO

      XCMold = XCM(Imol)
      YCMold = YCM(Imol)
      ZCMold = ZCM(Imol)

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN
         IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi)
         DO I=1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,I)
            IF(L_Charge(It)) THEN
               NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
               J = NKSPACE(Ib,1)
               XKSPACE(J,Ib,1) = X(Imol,I)
               YKSPACE(J,Ib,1) = Y(Imol,I)
               ZKSPACE(J,Ib,1) = Z(Imol,I)
               IF(L_frac(Imol)) THEN
                  QKSPACE(J,Ib,1) = Myc*Q(It)
               ELSE
                  QKSPACE(J,Ib,1) = Q(It)
               END IF
            END IF
         END DO
      END IF

C     Calculate the energy of the molecule
      CALL Energy_Molecule(Imol,E_LJ_InterOld,E_LJ_IntraOld,E_EL_RealOld,E_EL_IntraOld,
     &                          E_EL_ExclOld,E_BendingOld,E_TorsionOld,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Energy Overlap (Bending)"
         STOP
      END IF

      Eold = E_LJ_InterOld + E_LJ_IntraOld + E_EL_RealOld + E_EL_IntraOld
     &     + E_EL_ExclOld + E_BendingOld + E_TorsionOld


CCC   Generate new configuration and calculate energy
      phi = 2.0d0*(Ran_Uniform()-0.5d0)*dtheta(Tb)

      s=dsin(0.5d0*phi)
      c=dcos(0.5d0*phi)

      Iatom1 = BendingList(Tm,Ibend,1)
      Iatom2 = BendingList(Tm,Ibend,2)
      Iatom3 = BendingList(Tm,Ibend,3)

C     Calculate the old bond angle
      ux = X(Imol,Iatom1) - X(Imol,Iatom2)
      uy = Y(Imol,Iatom1) - Y(Imol,Iatom2)
      uz = Z(Imol,Iatom1) - Z(Imol,Iatom2)

      vx = X(Imol,Iatom3) - X(Imol,Iatom2)
      vy = Y(Imol,Iatom3) - Y(Imol,Iatom2)
      vz = Z(Imol,Iatom3) - Z(Imol,Iatom2)

      unorm = ux*ux + uy*uy + uz*uz
      vnorm = vx*vx + vy*vy + vz*vz

      IF(unorm.LT.1.0d-12) THEN
         WRITE(6,'(A,A)') ERROR, "Norm too small (Bending 1)"
         STOP
      ELSE
         unorm = 1.0d0/dsqrt(unorm)
      END IF

      IF(vnorm.LT.1.0d-12) THEN
         WRITE(6,'(A,A)') ERROR, "Norm too small (Bending 2)"
         STOP
      ELSE
         vnorm = 1.0d0/dsqrt(vnorm)
      END IF
      costheta_old = (ux*vx + uy*vy + uz*vz)*unorm*vnorm

      nx = uy*vz - uz*vy
      ny = uz*vx - ux*vz
      nz = ux*vy - uy*vx

      nnorm = nx*nx + ny*ny + nz*nz
      IF(nnorm.LT.1.0d-12) THEN
         WRITE(6,'(A,A)') ERROR, "Norm too small (Bending 3)"
         STOP
      ELSE
      nnorm = 1.0d0/dsqrt(nnorm)
      END IF

      nx = nx*nnorm
      ny = ny*nnorm
      nz = nz*nnorm

      Ox = X(Imol,Iatom2)
      Oy = Y(Imol,Iatom2)
      Oz = Z(Imol,Iatom2)

      DO I=1,Natombendlist(Tm,Ibend,Il)
         Iatom=Iatombendlist(Tm,Ibend,Il,I)

         Xo=X(Imol,Iatom)-Ox
         Yo=Y(Imol,Iatom)-Oy
         Zo=Z(Imol,Iatom)-Oz

         X(Imol,Iatom) = s*s*(Xo*(nx*nx-ny*ny-nz*nz) + 2.0d0*nx*(Yo*ny+Zo*nz))
     &        + 2.0d0*s*c*(Zo*ny-Yo*nz) + c*c*Xo + Ox
         Y(Imol,Iatom) = s*s*(Yo*(ny*ny-nz*nz-nx*nx) + 2.0d0*ny*(Xo*nx+Zo*nz))
     &        + 2.0d0*s*c*(Xo*nz-Zo*nx) + c*c*Yo + Oy
         Z(Imol,Iatom) = s*s*(Zo*(nz*nz-nx*nx-ny*ny) + 2.0d0*nz*(Xo*nx+Yo*ny))
     &        + 2.0d0*s*c*(Yo*nx-Xo*ny) + c*c*Zo + Oz

      END DO

      XCM(Imol) = 0.0d0
      YCM(Imol) = 0.0d0
      ZCM(Imol) = 0.0d0

      DO Iatom=1,N_AtomInMolType(Tm)
         XCM(Imol) = XCM(Imol) + X(Imol,Iatom)
         YCM(Imol) = YCM(Imol) + Y(Imol,Iatom)
         ZCM(Imol) = ZCM(Imol) + Z(Imol,Iatom)
      END DO

      XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
      YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
      ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

      CALL Place_molecule_back_in_box(Imol)

C     Calculate the new bond angle
      ux = X(Imol,Iatom1) - X(Imol,Iatom2)
      uy = Y(Imol,Iatom1) - Y(Imol,Iatom2)
      uz = Z(Imol,Iatom1) - Z(Imol,Iatom2)

      vx = X(Imol,Iatom3) - X(Imol,Iatom2)
      vy = Y(Imol,Iatom3) - Y(Imol,Iatom2)
      vz = Z(Imol,Iatom3) - Z(Imol,Iatom2)

      unorm = ux*ux + uy*uy + uz*uz
      vnorm = vx*vx + vy*vy + vz*vz

      IF(unorm.LT.1.0d-6) THEN
         WRITE(6,'(A,A)') ERROR, "Norm too small (Bending 4)"
         STOP
      ELSE
         unorm = 1.0d0/dsqrt(unorm)
      END IF

      IF(vnorm.LT.1.0d-6) THEN
         WRITE(6,'(A,A)') ERROR, "Norm too small (Bending 5)"
         STOP
      ELSE
         vnorm = 1.0d0/dsqrt(vnorm)
      END IF
      costheta_new = (ux*vx + uy*vy + uz*vz)*unorm*vnorm

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN
         IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi)
         DO I=1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,I)
            IF(L_Charge(It)) THEN
               NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
               J = NKSPACE(Ib,2)
               XKSPACE(J,Ib,2) = X(Imol,I)
               YKSPACE(J,Ib,2) = Y(Imol,I)
               ZKSPACE(J,Ib,2) = Z(Imol,I)
               IF(L_frac(Imol)) THEN
                  QKSPACE(J,Ib,2) = Myc*Q(It)
               ELSE
                  QKSPACE(J,Ib,2) = Q(It)
               END IF
            END IF
         END DO
      END IF

C     Calculate the energy of the molecule
      CALL Energy_Molecule(Imol,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,
     &                          E_EL_ExclNew,E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
         Laccept=.false.
         GO TO 1
      END IF

      dE_EL_Four = 0.0d0
      IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew
     &     + E_EL_ExclNew + E_BendingNew + E_TorsionNew


CCC   Calculate energy difference and Accept or Reject this MC move
      dE = Enew - Eold + dE_EL_Four

      IF(abs(1.0d0-costheta_old).GT.1.0D-6) THEN
         factor = dsqrt((1.0d0 - costheta_new*costheta_new)/(1.0d0 - costheta_old*costheta_old))
      ELSE
         factor = 1.0D15
      END IF

      CALL Accept_or_Reject(factor*dexp(-beta*dE),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         
         AcceptBending(Ib,Tm,Ibend) = AcceptBending(Ib,Tm,Ibend) + 1.0d0

         U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
         U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew - E_LJ_IntraOld

         U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
         U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew - E_EL_IntraOld
         U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew  - E_EL_ExclOld
         U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

         U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew - E_BendingOld
         U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew - E_TorsionOld

         U_Total(Ib) = U_Total(Ib) + dE

         IF(LEwald) CALL Ewald_Accept(Ib)

      ELSE

         DO I=1,N_AtomInMolType(Tm)
            X(Imol,I) = Xold(I)
            Y(Imol,I) = Yold(I)
            Z(Imol,I) = Zold(I)
         END DO

         XCM(Imol) = XCMold
         YCM(Imol) = YCMold
         ZCM(Imol) = ZCMold

      END IF

      RETURN
      END
