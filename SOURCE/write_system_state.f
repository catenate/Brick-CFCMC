      SUBROUTINE Write_System_State(Ilabel)
      implicit none

      include "global_variables.inc"
      include "output.inc"

      integer        Ilabel,Ib,Tm,Imol,I,Iatom,Ifrac
      character*64   fname
      character*7    Clabel
      character*10   N1

CCC   Write system state to a file (used for debugging)
      WRITE(Clabel,'(i7)') Ilabel
      Clabel = ADJUSTL(TRIM(Clabel))
      fname = "./STATES/state-" // Clabel

      OPEN(91,file=fname)

      DO Ib=1,N_Box
      WRITE(91,'(A,i1)') "Molecules in box ", Ib
      WRITE(91,'(A)') hashes
         DO Tm=1,N_MolType
            WRITE(91,'(A,i2)') "Moleculetype ", Tm
            WRITE(N1,'(i6)') Nmptpb(Ib,Tm)
            WRITE(91,'(A,A)')  "Number-of-molecules: ", adjustl(TRIM(N1))
            WRITE(91,'(A)') "Atomtype         X                  Y                   Z"
            DO I=1,Nmptpb(Ib,Tm)
               Imol = Imptpb(Ib,Tm,I)
               DO Iatom=1,N_AtomInMolType(Tm)
                  WRITE(91,'(i3,3f20.10)')
     &                     TypeAtom(Tm,Iatom), X(Imol,Iatom), Y(Imol,Iatom), Z(Imol,Iatom)
               END DO
            END DO
            WRITE(91,*)
         END DO
         WRITE(91,*)
      END DO

      WRITE(N1,'(i2)') N_Frac
      WRITE(91,'(A,A)') "Number-of-fractionals: ", N1
      WRITE(91,'(A)') hashes

      DO Ifrac=1,N_Frac
         WRITE(91,'(A,i2)')   "Fractional-type: ", Type_Frac(Ifrac)
         WRITE(91,'(A,i2)')   "Box:             ", Box_Frac(Ifrac)
         WRITE(91,'(A,f9.6)') "Lambda:          ", Lambda_Frac(Ifrac)
         WRITE(91,*)

         DO I=1,N_MolInFrac(Ifrac)
            Imol = I_MolInFrac(Ifrac,I)
            Tm   = TypeMol(Imol)
            WRITE(91,'(A,i2)') "Moleculetype ", Tm
            WRITE(91,'(A)') "Atomtype         X                  Y                   Z"
            DO Iatom=1,N_AtomInMolType(Tm)
               WRITE(91,'(i3,3f20.10)')
     &            TypeAtom(Tm,Iatom), X(Imol,Iatom), Y(Imol,Iatom), Z(Imol,Iatom)
            END DO
            WRITE(91,*)
         END DO
         WRITE(91,*)
      END DO

      CLOSE(91)

      RETURN
      END
