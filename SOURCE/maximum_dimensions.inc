C     All Maximum Dimensions For Arrays
C
C     MaxMol               = Max number of molecules
C     MaxMolType           = Max number of components
C     MaxAtom              = Max number of atoms
C     MaxAtomType          = Max number of atom types
C     MaxBond              = Max number of bonds in a molecule
C     MaxBendingType       = Max number of bending types
C     MaxBendingInMolType  = Max number of bond bendings in a molecule
C     MaxTorsionType       = Max number of torsion types
C     MaxTorsionInMolType  = Max number of torsions per molecule
C     MaxFrac              = Max number of fractionals
C     MaxMolInFrac         = Max number of molecules in a fractional
C     MaxReaction          = Max number of reactions
C     MaxReactionStep      = Max number of reaction steps in a reaction
C     MaxMolTypePair       = Max number of molecule type pairs
C     MaxCluster           = Max number of clusters
C     MaxMolInCluster      = Max number of molecules in a cluster
C     MaxLambdaBin         = Max number of bins for lambda histogram
C     MaxKvec              = Max number of k-vectors in one direction for Ewald summation
C     MaxRDFbins           = Max number of bins for calculating RDFs
C     MaxUmbrellaBin       = Max number of bins used for Umbrella Sampling (Pressure and Beta)

      integer  MaxMol,MaxMolType,MaxAtom,MaxAtomType,
     &         MaxBond,MaxBendingType,MaxBendingInMolType,
     &         MaxTorsionType,MaxTorsionInMolType,
     &         MaxFrac,MaxMolInFrac,
     &         MaxReaction,MaxReactionStep,
     &         MaxMolTypePair,MaxCluster,MaxMolInCluster,
     &         MaxLambdaBin,MaxKvec,MaxRDFbins,MaxUmbrellaBin

      Parameter (MaxMol               = 2000)
      Parameter (MaxMolType           = 10)

      Parameter (MaxAtom              = 32)
      Parameter (MaxAtomType          = 32)

      Parameter (MaxBond              = 32)
      Parameter (MaxBendingType       = 32)
      Parameter (MaxBendingInMolType  = 32)

      Parameter (MaxTorsionType       = 32)
      Parameter (MaxTorsionInMolType  = 32)

      Parameter (MaxFrac              = 10)
      Parameter (MaxMolInFrac         = 5)

      Parameter (MaxReaction          = 5)
      Parameter (MaxReactionStep      = 3)

      Parameter (MaxMolTypePair       = 5)
      Parameter (MaxCluster           = 50)
      Parameter (MaxMolInCluster      = 50)

      Parameter (MaxLambdaBin         = 500)
      Parameter (MaxKvec              = 25)
      Parameter (MaxRDFbins           = 10000)
      Parameter (MaxUmbrellaBin       = 20)
