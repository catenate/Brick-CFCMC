      SUBROUTINE Update_Deltas_And_Gammas
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "output.inc"
      include "settings.inc"
      include "trial_moves.inc"

      integer Ib,Tm
      double precision rm

      IF(L_Translation) THEN
         DO Ib=1,N_Box
            DO Tm=1,N_MolType
               IF(TrialTranslation(Ib,Tm).GT.5000.0d0) THEN
                  rm = 2.0d0*AcceptTranslation(Ib,Tm)/TrialTranslation(Ib,Tm)

                  IF(rm.GT.2.0d0) THEN
                     rm = 2.0d0
       		      ELSEIF(rm.LT.0.5d0) THEN
                     rm = 0.5d0
                  END IF

                  Delta_Translation(Ib,Tm) = rm*Delta_Translation(Ib,Tm)

                  IF(Delta_Translation(Ib,Tm).GT.0.5d0*BoxSize(Ib)) Delta_Translation(Ib,Tm) = 0.5d0*BoxSize(Ib)

                  AcceptTranslation(Ib,Tm) = 0.0d0
                  TrialTranslation(Ib,Tm)  = 0.0d0
               END IF
            END DO
         END DO
      END IF

      IF(L_Rotation) THEN
         DO Ib=1,N_Box
            DO Tm=1,N_MolType
               IF(TrialRotation(Ib,Tm).GT.5000.0d0) THEN
                  rm = 2.0d0*AcceptRotation(Ib,Tm)/TrialRotation(Ib,Tm)

                  IF(rm.GT.2.0d0) THEN
                     rm = 2.0d0
                  ELSEIF(rm.LT.0.5d0) THEN
                     rm = 0.5d0
                  END IF

                  Delta_Rotation(Ib,Tm) = rm*Delta_Rotation(Ib,Tm)
                  IF(Delta_Rotation(Ib,Tm).GT.OnePi) Delta_Rotation(Ib,Tm) = OnePi

                  AcceptRotation(Ib,Tm) = 0.0d0
                  TrialRotation(Ib,Tm)  = 0.0d0
               END IF
            END DO
         END DO
      END IF

      IF(L_Volume) THEN
         DO Ib=1,N_Box
            IF(TrialVolume(Ib).GT.5000.0d0) THEN
               rm = 2.0d0*AcceptVolume(Ib)/TrialVolume(Ib)

               IF(rm.GT.2.0d0) THEN
                  rm = 2.0d0
               ELSEIF(rm.LT.0.5d0) THEN
                  rm = 0.5d0
               END IF

               Delta_Volume(Ib) = rm*Delta_Volume(Ib)
               IF(Delta_Volume(Ib).GT.0.4d0*Volume(Ib)) Delta_Volume(Ib)= 0.4d0*Volume(Ib)

               AcceptVolume(Ib) = 0.0d0
               TrialVolume(Ib)  = 0.0d0
            END IF
         END DO
      END IF

      IF(L_ClusterVolume) THEN
         DO Ib=1,N_Box
            IF(TrialClusterVolume(Ib).GT.5000.0d0) THEN
               rm = 2.0d0*AcceptClusterVolume(Ib)/TrialClusterVolume(Ib)

               IF(rm.GT.2.0d0) THEN
                  rm = 2.0d0
               ELSEIF(rm.LT.0.5d0) THEN
                  rm = 0.5d0
               END IF

               Delta_ClusterVolume(Ib) = rm*Delta_ClusterVolume(Ib)
               IF(Delta_ClusterVolume(Ib).GT.0.4d0*Volume(Ib)) Delta_ClusterVolume(Ib)=0.4d0*Volume(Ib)

               AcceptClusterVolume(Ib) = 0.0d0
               TrialClusterVolume(Ib)  = 0.0d0

            END IF
         END DO
      END IF

      IF(L_SmartTranslation) THEN
         DO Ib=1,N_Box
            IF(TrialSmartTranslation(Ib).GT.5000.0d0) THEN
               rm = 2.0d0*AcceptSmartTranslation(Ib)/TrialSmartTranslation(Ib)

               IF(rm.GT.2.0d0) THEN
                  rm = 2.0d0
               ELSEIF(rm.LT.0.5d0) THEN
                  rm = 0.5d0
               END IF

               Gamma_Translation(Ib) = rm*Gamma_Translation(Ib)
               A_Translation(Ib) = 0.5d0*Gamma_Translation(Ib)*Gamma_Translation(Ib)

               AcceptSmartTranslation(Ib) = 0.0d0
               TrialSmartTranslation(Ib)  = 0.0d0
            END IF
         END DO
      END IF

      IF(L_SmartRotation) THEN
         DO Ib=1,N_Box
            IF(TrialSmartRotation(Ib).GT.5000.0d0) THEN
               rm = 2.0d0*AcceptSmartRotation(Ib)/TrialSmartRotation(Ib)

               IF(rm.GT.2.0d0) THEN
                  rm = 2.0d0
               ELSEIF(rm.LT.0.5d0) THEN
                  rm = 0.5d0
               END IF

               Gamma_Rotation(Ib) = rm*Gamma_Rotation(Ib)
               B_Rotation(Ib)     = 0.5d0*Gamma_Rotation(Ib)*Gamma_Rotation(Ib)

               AcceptSmartRotation(Ib) = 0.0d0
               TrialSmartRotation(Ib)  = 0.0d0
            END IF
         END DO
      END IF




      RETURN
      END
