      SUBROUTINE Accept_or_Reject(Factor,Laccept)
      implicit none

C     Test Acceptance; Draw Random Number For This
C     Always Reject Very Low Acceptance Probabilities In Advance

      double precision Ran_Uniform,Factor
      logical Laccept

      IF(Factor.GT.1.0d0) THEN
         Laccept = .true.
      ELSEIF(Factor.LT.1.0d-20) THEN
         Laccept = .false.
      ELSE
         IF(Ran_Uniform().LT.Factor) THEN
            Laccept = .true.
         ELSE
            Laccept = .false.
         END IF
      END IF

      RETURN
      END
