      SUBROUTINE Convert_Units(Lreducedunits,CpressureUnit)
      implicit none

      include "global_variables.inc"
      include "output.inc"    

      logical           Lreducedunits
      character*3       CpressureUnit

      IF(Lreducedunits) THEN
         Pconv = 1.0d0
         Mconv = 1.0d0
         RETURN
      ELSE
         Mconv = 1660.53904d0
         IF(LEN(TRIM(CpressureUnit)).EQ.2) THEN
            IF(TRIM(CpressureUnit).EQ."Pa") THEN
               Pconv = 13806485.2d0
            ELSE
               WRITE(6,'(A,A)') ERROR, "Invalid pressure unit (settings.in)"
               STOP
            END IF
         ELSEIF(LEN(TRIM(CpressureUnit)).EQ.3) THEN
            IF((CpressureUnit(1:1).EQ."r").OR.(CpressureUnit(1:1).EQ."R")) THEN
               IF(.NOT.Lreducedunits) THEN
                  WRITE(6,'(A,A)') ERROR, "Set Lreducedunits to .true. (settings.in)"
               END IF
            ELSEIF((CpressureUnit(2:3).EQ.'Pa').OR.(CpressureUnit(2:3).EQ.'PA').OR.(CpressureUnit(2:3).EQ.'pa')) THEN
               IF((CpressureUnit(1:1).EQ.'k').OR.(CpressureUnit(1:1).EQ.'K')) THEN
                  Pconv = 13806.4852d0
               ELSEIF((CpressureUnit(1:1).EQ.'M')) THEN
                  Pconv = 13.8064852d0
               ELSEIF((CpressureUnit(1:1).EQ.'h').OR.(CpressureUnit(1:1).EQ.'H')) THEN
                  Pconv = 138064.852d0
               ELSE
                  WRITE(6,*) ERROR, "Invalid SI-prefix for pressure in Pascal (settings.in)"
                  STOP
               END IF
            ELSEIF((CpressureUnit(1:3).EQ.'Bar').OR.(CpressureUnit(1:3).EQ.'bar').OR.(CpressureUnit(1:3).EQ.'BAR')) THEN
               Pconv = 138.064852d0
            ELSE
               WRITE(6,'(A,A)') ERROR, "Invalid pressure unit (settings.in)"
               STOP
            END IF
         ELSE
            WRITE(6,'(A,A)') ERROR, "Invalid pressure unit (settings.in)"
            STOP
         END IF
      END IF

      RETURN
      END
