CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                                             CCC
CCC                                Initialize Umbrella Sampling                                 CCC
CCC                                                                                             CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE Umbrella_Sampling_Initialize
      implicit none

      include "global_variables.inc"
      include "output.inc"

      integer I,Ib,Iff
      double precision dP(2),dBeta(2),dP_max(2),dBeta_max(2)
      character dummy
      logical Lfile_exist

      Save Iff
      Data Iff /0/

      INQUIRE(file="./INPUT/umbrella.in",EXIST=Lfile_exist)

      IF(Lfile_exist.AND.(Iff.EQ.0)) THEN
         WRITE(6,'(A)') "Umbrella Sampling"
         OPEN(9,file="./INPUT/umbrella.in")
         READ(9,*)
         READ(9,*) dummy, (dP_max(Ib),N_UmbrellaBinPressure(Ib), Ib=1,N_Box)
         READ(9,*) dummy, (dBeta_max(Ib),N_UmbrellaBinBeta(Ib), Ib=1,N_Box)
         WRITE(6,'(A,A)') OK, "Read from ./INPUT/umbrella.in"
         WRITE(6,*)

         DO Ib=1,N_Box
            dP_max(Ib) = dP_max(Ib)/Pconv

            IF(N_UmbrellaBinPressure(Ib).LT.0) THEN
               WRITE(6,*) ERROR, "Number of Bins for Umbrella Sampling Pressure is negative."
               STOP
            END IF
            IF(N_UmbrellaBinBeta(Ib).LT.0) THEN
               WRITE(6,*) ERROR, "Number of Bins for Umbrella Sampling Beta is negative."
               STOP
            END IF
            IF(N_UmbrellaBinPressure(Ib).GT.MaxUmbrellaBin) THEN
               WRITE(6,*) ERROR, "Number of Bins for Umbrella Sampling Pressure is larger than MaxUmbrellaBin."
               STOP
            END IF
            IF(N_UmbrellaBinBeta(Ib).GT.MaxUmbrellaBin) THEN
               WRITE(6,*) ERROR, "Number of Bins for Umbrella Sampling Beta is larger than MaxUmbrellaBin."
               STOP
            END IF

         END DO

         DO Ib=1,N_Box

            IF((N_UmbrellaBinPressure(Ib).NE.0).AND.(N_UmbrellaBinPressure(Ib).NE.1)) THEN
               IF(MOD(N_UmbrellaBinPressure(Ib),2).EQ.1) THEN
                  N_UmbrellaBinPressure(Ib) = N_UmbrellaBinPressure(Ib) - 1
               END IF
               dP(Ib) = dP_max(Ib)/(dble(N_UmbrellaBinPressure(Ib)/2))
            END IF

            IF((N_UmbrellaBinBeta(Ib).NE.0).AND.(N_UmbrellaBinBeta(Ib).NE.1)) THEN
               IF(MOD(N_UmbrellaBinBeta(Ib),2).EQ.1) THEN
                  N_UmbrellaBinBeta(Ib) = N_UmbrellaBinBeta(Ib) - 1
               END IF
               dBeta(Ib) = dBeta_max(Ib)/(dble(N_UmbrellaBinBeta(Ib)/2))
            END IF

         END DO

         DO Ib=1,N_Box
            DO I=1,N_UmbrellaBinPressure(Ib)/2
                P_Umbrella(I,Ib) = Pressure - dble(N_UmbrellaBinPressure(Ib)/2+1-I)*dP(Ib)
                P_Umbrella(N_UmbrellaBinPressure(Ib)/2+I+1,Ib) = Pressure + dble(I)*dP(Ib)
            END DO
            DO I=1,N_UmbrellaBinBeta(Ib)/2
                Beta_Umbrella(I,Ib) = Beta - dble(N_UmbrellaBinBeta(Ib)/2+1-I)*dBeta(Ib)
                Beta_Umbrella(N_UmbrellaBinBeta(Ib)/2+I+1,Ib) = Beta + dble(I)*dBeta(Ib)
            END DO

            P_Umbrella(N_UmbrellaBinPressure(Ib)/2+1,Ib) = Pressure
            Beta_Umbrella(N_UmbrellaBinBeta(Ib)/2+1,Ib) = Beta

            N_UmbrellaBinPressure(Ib) = N_UmbrellaBinPressure(Ib) + 1
            N_UmbrellaBinBeta(Ib) = N_UmbrellaBinBeta(Ib) + 1
         END DO

      ELSEIF((.NOT.Lfile_exist).AND.(Iff.EQ.1)) THEN

         DO Ib=1,N_Box
            N_UmbrellaBinPressure(Ib) = 10
            N_UmbrellaBinBeta(Ib) = 10

            dP(Ib) = (5.0d0/(beta*Volume(Ib)))/dble(N_UmbrellaBinPressure(Ib)/2)
            dBeta(Ib) = dabs(5.0d0/(U_Total(Ib)+pressure*Volume(Ib)))/dble(N_UmbrellaBinBeta(Ib)/2)
         END DO

         DO Ib=1,N_Box
            DO I=1,N_UmbrellaBinPressure(Ib)/2
                P_Umbrella(I,Ib) = Pressure - dble(N_UmbrellaBinPressure(Ib)/2+1-I)*dP(Ib)
                P_Umbrella(N_UmbrellaBinPressure(Ib)/2+I+1,Ib) = Pressure + dble(I)*dP(Ib)
            END DO
            DO I=1,N_UmbrellaBinBeta(Ib)/2
                Beta_Umbrella(I,Ib) = Beta - dble(N_UmbrellaBinBeta(Ib)/2+1-I)*dBeta(Ib)
                Beta_Umbrella(N_UmbrellaBinBeta(Ib)/2+I+1,Ib) = Beta + dble(I)*dBeta(Ib)
            END DO

            P_Umbrella(N_UmbrellaBinPressure(Ib)/2+1,Ib) = Pressure
            Beta_Umbrella(N_UmbrellaBinBeta(Ib)/2+1,Ib) = Beta

            N_UmbrellaBinPressure(Ib) = N_UmbrellaBinPressure(Ib) + 1
            N_UmbrellaBinBeta(Ib) = N_UmbrellaBinBeta(Ib) + 1
         END DO

      END IF

      Iff = 1

      RETURN
      END





      SUBROUTINE Umbrella_Sampling_Finalize
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"

      integer J,Ifrac,Ib,Rs,Tm
      double precision Density
      character*100 fname

      CALL system('mkdir OUTPUT/UMBRELLA_SAMPLING')

      OPEN(80,file="./OUTPUT/UMBRELLA_SAMPLING/Density_vs_Pressure.dat")
      DO Tm=1,N_MolType
         WRITE(80,'(A,A)') "# Molecule: ", C_MolType(Tm)
         DO Ib=1,N_Box
            WRITE(80,'(A,i1,19x,A)') "# Box ", Ib, "Boltzmann             Biased"
            DO J=1,N_UmbrellaBinPressure(Ib)
               WRITE(80,'(4e20.10e3)') P_Umbrella(J,Ib)*Pconv,
     &    MolarMass(Tm)*Mconv*AvR_Densityptpb_UmbrellaPressure(J,Ib,Tm)/AvR_Norm_UmbrellaPressure(J,Ib,Tm),
     &    MolarMass(Tm)*Mconv*AvB_Densityptpb_UmbrellaPressure(J,Ib,Tm)/AvB_Norm_UmbrellaPressure(J,Ib,Tm)
            END DO
            WRITE(80,*)
            WRITE(80,*)
         END DO
      END DO
      CLOSE(80)

      OPEN(80,file="./OUTPUT/UMBRELLA_SAMPLING/Density_vs_Beta.dat")
      DO Tm=1,N_MolType
         WRITE(80,'(A,A)') "# Molecule: ", C_MolType(Tm)
         DO Ib=1,N_Box
            WRITE(80,'(A,i1,19x,A)') "# Box ", Ib, "Boltzmann             Biased"
            DO J=1,N_UmbrellaBinBeta(Ib)
               WRITE(80,'(24e20.10e3)') Beta_Umbrella(J,Ib),
     &    MolarMass(Tm)*Mconv*AvR_Densityptpb_UmbrellaBeta(J,Ib,Tm)/AvR_Norm_UmbrellaBeta(J,Ib,Tm),
     &    MolarMass(Tm)*Mconv*AvB_Densityptpb_UmbrellaBeta(J,Ib,Tm)/AvB_Norm_UmbrellaBeta(J,Ib,Tm)
            END DO
            WRITE(80,*)
            WRITE(80,*)
         END DO
      END DO
      CLOSE(80)

      DO Ifrac=1,N_Frac
         IF(Type_Frac(Ifrac).EQ.1) THEN
            WRITE(fname,'(A,i1,A)') "./OUTPUT/UMBRELLA_SAMPLING/ChemicalPotential_vs_Pressure-", Ifrac, ".dat"
            OPEN(80,file=fname)
            Ib = Box_Frac(Ifrac)
            Rs = 1

            WRITE(80,'(A)') "#     Pressure              mu_IG              mu_excess           mu_total"
            DO J=1,N_UmbrellaBinPressure(Ib)
               IF(N_MolInFrac(Ifrac).EQ.1) THEN
                  Tm = TypeMol(I_MolInFrac(Ifrac,1))
                  Density = AvR_Densityptpb_UmbrellaPressure(J,Ib,Tm)/AvR_Norm_UmbrellaPressure(J,Ib,Tm)
               ELSE
                  Density = 1.0d0
               END IF
               WRITE(80,'(4e20.10e3)') P_Umbrella(J,Ib)*Pconv, (1.0d0/beta)*dlog(Density),
     &           -dlog(Lambda1Counter_UmbrellaPressure(J,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))/
     &                (Lambda0Counter_UmbrellaPressure(J,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac))))/beta,
     &           (1.0d0/beta)*dlog(Density)
     &           -dlog(Lambda1Counter_UmbrellaPressure(J,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))/
     &                (Lambda0Counter_UmbrellaPressure(J,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac))))/beta
            END DO
         END IF
      END DO

      DO Ifrac=1,N_Frac
         IF(Type_Frac(Ifrac).EQ.1) THEN
            WRITE(fname,'(A,i1,A)') "./OUTPUT/UMBRELLA_SAMPLING/ChemicalPotential_vs_Beta-", Ifrac, ".dat"
            OPEN(80,file=fname)
            Ib = Box_Frac(Ifrac)
            Rs = 1

            WRITE(80,'(A)') "#       Beta                mu_IG              mu_excess           mu_total"
            DO J=1,N_UmbrellaBinBeta(Ib)
               IF(N_MolInFrac(Ifrac).EQ.1) THEN
                  Tm = TypeMol(I_MolInFrac(Ifrac,1))
                  Density = AvR_Densityptpb_UmbrellaBeta(J,Ib,Tm)/AvR_Norm_UmbrellaBeta(J,Ib,Tm)
               ELSE
                  Density = 1.0d0
               END IF
               WRITE(80,'(4e20.10e3)') Beta_Umbrella(J,Ib), (1.0d0/Beta_Umbrella(J,Ib))*dlog(Density),
     &           -dlog(Lambda1Counter_UmbrellaBeta(J,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))/
     &                (Lambda0Counter_UmbrellaBeta(J,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac))))/Beta_Umbrella(J,Ib),
     &           (1.0d0/beta_Umbrella(J,Ib))*dlog(Density)
     &           -dlog(Lambda1Counter_UmbrellaBeta(J,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))/
     &                (Lambda0Counter_UmbrellaBeta(J,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac))))/Beta_Umbrella(J,Ib)
            END DO
         END IF
      END DO

      RETURN
      END





      SUBROUTINE Umbrella_Sampling_Get_Molar_Volume(Ifrac,PartialMolarVolume)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"

      integer Ifrac,Ib,Rs,Jmid,J,K,Tm
      double precision PartialMolarVolume,Density,YY(4),Central_Finite_Difference

      PartialMolarVolume=0.0d0

      IF(Type_Frac(Ifrac).EQ.1) THEN
         Ib = Box_Frac(Ifrac)
         Rs = 1

         Jmid = (N_UmbrellaBinPressure(Ib)+1)/2
         IF(Jmid.LE.2) RETURN

         K = 0
         DO J=Jmid-2,Jmid+2
            IF(J.EQ.Jmid) CYCLE
            K = K + 1
            IF(N_MolInFrac(Ifrac).EQ.1) THEN
               Tm = TypeMol(I_MolInFrac(Ifrac,1))
               Density = AvR_Densityptpb_UmbrellaPressure(J,Ib,Tm)/AvR_Norm_UmbrellaPressure(J,Ib,Tm)
            ELSE
               Density = 1.0d0
            END IF

            YY(K) = (1.0d0/beta)*dlog(Density)
     &         -dlog(Lambda1Counter_UmbrellaPressure(J,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))/
     &              (Lambda0Counter_UmbrellaPressure(J,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac))))/beta
         END DO

      PartialMolarVolume = Central_Finite_Difference((P_Umbrella(2,Ib)-P_Umbrella(1,Ib))*Pconv,YY)*Pconv

      END IF

      RETURN
      END




      SUBROUTINE Umbrella_Sampling_Get_Molar_Enthalpy(Ifrac,PartialMolarEnthalpy)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"

      integer Ifrac,Ib,Rs,Jmid,J,K,Tm
      double precision PartialMolarEnthalpy,Density,YY(4),Central_Finite_Difference

      PartialMolarEnthalpy = 0.0d0

      IF(Type_Frac(Ifrac).EQ.1) THEN
         Ib = Box_Frac(Ifrac)
         Rs = 1

         Jmid = (N_UmbrellaBinBeta(Ib)+1)/2
         IF(Jmid.LE.2) RETURN

         K = 0
         DO J=Jmid-2,Jmid+2
            IF(J.EQ.Jmid) CYCLE
            K = K + 1
            IF(N_MolInFrac(Ifrac).EQ.1) THEN
               Tm = TypeMol(I_MolInFrac(Ifrac,1))
               Density = AvR_Densityptpb_UmbrellaBeta(J,Ib,Tm)/AvR_Norm_UmbrellaBeta(J,Ib,Tm)
            ELSE
               Density = 1.0d0
            END IF

            YY(K) = dlog(Density)
     &             -dlog(Lambda1Counter_UmbrellaBeta(J,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))/
     &                  (Lambda0Counter_UmbrellaBeta(J,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac))))
         END DO

         PartialMolarEnthalpy = Central_Finite_Difference(Beta_Umbrella(2,Ib)-Beta_Umbrella(1,Ib),YY) - 1.0d0/Beta

      END IF


      RETURN
      END



      FUNCTION Central_Finite_Difference(h,yy)
      implicit none

      Double Precision yy(4),h,Central_Finite_Difference

      Central_Finite_Difference = (yy(1)-yy(4) + 8.0d0*(yy(3)-yy(2)))/(12.0d0*h)

      RETURN
      END
