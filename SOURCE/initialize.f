      SUBROUTINE Initialize(Ichoice)
      implicit None

      include "global_variables.inc"
      include "energy.inc"
      include "output.inc"

      integer Tm,Tmi,Tmj,Tf,I,ii,jj,Ib,Imol,Jmol,It,Jt,Ifrac,Rs,Ireac,
     &     Ichoice,Tmtemp,Iatom,Nwhole(2),Select_Random_Integer
      double precision Ran_Uniform,dX,dY,dZ,R2,Numdens(2)
      logical Lresult
      character*1 dummy

      WRITE(6,'(A)') "Configuration"

      IF(Ichoice.EQ.1) THEN
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C              DISTRIBUTE MOLECULES RANDOMLY               C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         DO Ib=1,2
            N_MolInBox(Ib) = 0
            DO I=1,MaxMol
               I_MolInBox(Ib,I) = 0
               DO Tm=1,MaxMolType
                  Imptpb(Ib,Tm,I) = 0
               END DO
            END DO
         END DO

         DO Imol=1,MaxMol
            Ibox(Imol)       = 0
            TypeMol(Imol)    = 0
            L_Frac(Imol)     = .false.
         END DO

         DO Ifrac=1,MaxFrac
            Lambda_Frac(Ifrac)       = -1.0d0
            DO I=1,MaxMolInFrac
               I_MolInFrac(Ifrac,I) = 0
            END DO
         END DO


         Imol = 0                 !Unique label

         DO Ib=1,N_Box

            ii = 0              !Counter in box

            DO Tm=1,N_MolType

               jj = 0

               DO I=1,Nmptpb(Ib,Tm)
                  Imol = Imol + 1

                  IF(Imol.GT.MaxMol) THEN
                     WRITE(6,'(A,A)') ERROR, "Total Number of Molecules + Fractionals > MaxMol"
                     STOP
                  END IF

                  ii = ii + 1
                  jj = jj + 1

                  TypeMol(Imol) = Tm
                  Ibox(Imol)    = Ib

                  I_MolInBox(Ib,ii) = Imol
                  Imptpb(Ib,Tm,jj)  = Imol
               END DO

            END DO

            N_MolInBox(Ib) = ii

         END DO


         DO Ifrac=1,N_Frac

            Lambda_Frac(Ifrac) = 0.5d0

            Tf = Type_Frac(Ifrac)
            Ib = Box_Frac(Ifrac)

            IF(Tf.NE.3) THEN

               DO I=1,N_MolInFrac(Ifrac)
                  Imol = Imol + 1

                  IF(Imol.GT.MaxMol) THEN
                     WRITE(6,'(A,A)') ERROR, "Total Number of Molecules + Fractionals > MaxMol"
                     STOP
                  END IF
                  Tm = I_MolTypeInFrac(Ifrac,I)

                  TypeMol(Imol) = Tm
                  Ibox(Imol)    = Ib

                  N_MolInBox(Ib) = N_MolInBox(Ib) + 1
                  I_MolInBox(Ib,N_MolInBox(Ib)) = Imol

                  I_MolInFrac(Ifrac,I) = Imol

                  L_Frac(Imol) = .true.
               END DO

            ELSEIF(Tf.EQ.3) THEN

               Ireac = Reaction_Frac(Ifrac)

               Rs = Select_Random_Integer(N_ReactionStep(Ireac))
               ReactionStep_Frac(Ifrac) = Rs
               N_MolInFrac(Ifrac) = N_MolInReactionStep(Ireac,Rs)

               DO I=1,N_MolInReactionStep(Ireac,Rs)
                  Imol = Imol + 1

                  IF(Imol.GT.MaxMol) THEN
                     WRITE(6,'(A,A)') ERROR, "Total Number of Molecules + Fractionals > MaxMol"
                     STOP
                  END IF

                  Tm = I_MolTypeInReactionStep(Ireac,Rs,I)

                  TypeMol(Imol) = Tm
                  Ibox(Imol)    = Ib

                  N_MolInBox(Ib) = N_MolInBox(Ib) + 1
                  I_MolInBox(Ib,N_MolInBox(Ib)) = Imol

                  I_MolInFrac(Ifrac,I) = Imol

                  L_Frac(Imol) = .true.
               END DO

            ELSE
               WRITE(6,'(A,A,i2)') ERROR, "Fractional type undefined for fractional number: ", Ifrac
               STOP
            END IF
         END DO

         N_MolTotal = N_MolInBox(1) + N_MolInBox(2)

C     Generate A Random Initial Configuration And Place Molecules In The Box

         DO Imol=1,N_MolTotal

 1          CONTINUE

            Ib  = Ibox(Imol)
            Tmi = TypeMol(Imol)

            XCM(Imol) = 0.0d0
            YCM(Imol) = 0.0d0
            ZCM(Imol) = 0.0d0

            dX = Ran_Uniform()*BoxSize(Ib)
            dY = Ran_Uniform()*BoxSize(Ib)
            dZ = Ran_Uniform()*BoxSize(Ib)

            Tm = TypeMol(Imol)

            DO I=1,N_AtomInMolType(Tm)
               X(Imol,I) = Xin(Tm,I) + dX
               Y(Imol,I) = Yin(Tm,I) + dY
               Z(Imol,I) = Zin(Tm,I) + dZ

               XCM(Imol) = XCM(Imol) + X(Imol,I)
               YCM(Imol) = YCM(Imol) + Y(Imol,I)
               ZCM(Imol) = ZCM(Imol) + Z(Imol,I)
            END DO

            XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
            YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
            ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

C     Random Orientation And Random Position

            CALL Random_Orientation(Imol)

            CALL Place_molecule_back_in_box(Imol)

C     Check If There Is No Hard-Core Overlap With Molecules Already Present

            DO Jmol=1,(Imol-1)
               IF(Ib.EQ.Ibox(Jmol)) THEN

                  Tmj = TypeMol(Jmol)

                  DO ii = 1,N_AtomInMolType(Tmi)
                     DO jj = 1,N_AtomInMolType(Tmj)

                        It = TypeAtom(Tmi,ii)
                        Jt = TypeAtom(Tmj,jj)

                        dX = X(Imol,ii) - X(Jmol,jj)
                        dY = Y(Imol,ii) - Y(Jmol,jj)
                        dZ = Z(Imol,ii) - Z(Jmol,jj)

                        dX = dX - BoxSize(Ib)*dnint(dX*InvBoxSize(Ib))
                        dY = dY - BoxSize(Ib)*dnint(dY*InvBoxSize(Ib))
                        dZ = dZ - BoxSize(Ib)*dnint(dZ*InvBoxSize(Ib))

                        R2 = dX*dX + dY*dY + dZ*dZ

                        IF(R2.LT.1.01d0*R_Min_2(It,Jt)) GO TO 1

                     END DO
                  END DO
               END IF
            END DO
         END DO

C     Determine the vapor phase box
         DO Ib=1,N_Box
            Nwhole(Ib) = 0
            DO Tm=1,N_MolType
               Nwhole(Ib) = Nwhole(Ib) + Nmptpb(Ib,Tm)
            END DO
            Numdens(Ib) = dble(Nwhole(Ib))/Volume(Ib)
         END DO

         IF(N_Box.EQ.1) THEN
            Ibvapor = 1
         ELSEIF(Numdens(1).LT.Numdens(2)) THEN
            Ibvapor = 1
         ELSE
            Ibvapor = 2
         END IF

         WRITE(6,'(A,A)') OK, "Starting from random configuration."
         WRITE(6,*)

      ELSE
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C                READ MOLECULES FROM FILE                  C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

         OPEN(11,file="./INPUT/restart.in")

         READ(11,*)
         READ(11,*)
         READ(11,*)

         READ(11,*) dummy, N_MolTotal
         READ(11,*)
         READ(11,*) dummy, (BoxSize(Ib), Ib=1,N_Box)

         DO Ib=1,N_Box
            Volume(Ib) = BoxSize(Ib)*BoxSize(Ib)*BoxSize(Ib)
            InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)
         END DO

         READ(11,*)
         READ(11,*)
         READ(11,*)

         READ(11,*)

         READ(11,*)
         DO Ib=1,N_Box
            READ(11,*) dummy, dummy, (Delta_Translation(Ib,Tm), Tm=1,N_MolType)
         END DO

         READ(11,*)

         READ(11,*)
         DO Ib=1,N_Box
            READ(11,*) dummy, dummy, (Delta_Rotation(Ib,Tm), Tm=1,N_MolType)
            DO Tm=1,N_MolType
               Delta_Rotation(Ib,Tm) = Delta_Rotation(Ib,Tm)*OnePi/180.0d0
            END DO
         END DO

         READ(11,*)

         READ(11,*)
         DO Ib=1,N_Box
            READ(11,*) dummy, dummy, Delta_Volume(Ib), Gamma_Translation(Ib), Gamma_Rotation(Ib)
         END DO

         READ(11,*)
         READ(11,*)
         READ(11,*)

         Imol = 0

         DO Ib=1,N_Box
            READ(11,*)
            READ(11,*)
            N_MolInBox(Ib) = 0
            InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)
            DO Tm=1,N_MolType
               READ(11,*) dummy, Tmtemp
               IF((Tmtemp.NE.Tm)) THEN
                  WRITE(6,'(A,A)') ERROR, "Something went wrong reading the input configuration"
                  STOP
               END IF
               READ(11,*) dummy, Nmptpb(Ib,Tm)
               READ(11,*)
               DO I=1,Nmptpb(Ib,Tm)
                  Imol = Imol + 1
                  IF(Imol.GT.MaxMol) THEN
                     WRITE(6,*) ERROR, "Number of molecules > MaxMol"
                     STOP
                  END IF
                  N_MolInBox(Ib) = N_MolInBox(Ib) + 1

                  Imptpb(Ib,Tm,I)               = Imol
                  I_MolInBox(Ib,N_MolInBox(Ib)) = Imol

                  Ibox(Imol)    = Ib
                  TypeMol(Imol) = Tm
                  L_Frac(Imol)  = .false.

                  XCM(Imol) = 0.0d0
                  YCM(Imol) = 0.0d0
                  ZCM(Imol) = 0.0d0

                  DO Iatom=1,N_AtomInMolType(Tm)
                     READ(11,*) TypeAtom(Tm,Iatom),  X(Imol,Iatom),  Y(Imol,Iatom),  Z(Imol,Iatom)

                     XCM(Imol) = XCM(Imol) + X(Imol,Iatom)
                     YCM(Imol) = YCM(Imol) + Y(Imol,Iatom)
                     ZCM(Imol) = ZCM(Imol) + Z(Imol,Iatom)
                  END DO

                  XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
                  YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
                  ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

                  CALL Place_molecule_back_in_box(Imol)
               END DO
               READ(11,*)
            END DO
            READ(11,*)
         END DO

         READ(11,*) dummy, N_Frac
         READ(11,*)

         Ireac=0

         DO Ifrac=1,N_Frac
            READ(11,*) dummy, Type_Frac(Ifrac)
            READ(11,*) dummy, Box_Frac(Ifrac)
            READ(11,*) dummy, Lambda_Frac(Ifrac)
            READ(11,*) dummy, ReactionStep_Frac(Ifrac)
            READ(11,*)

            IF(Type_Frac(Ifrac).EQ.3) THEN
               Ireac = Ireac + 1
               Reaction_Frac(Ifrac) = Ireac
               N_MolInFrac(Ifrac) = N_MolInReactionStep(Ireac,ReactionStep_Frac(Ifrac))
            END IF

            DO I=1,N_MolInFrac(Ifrac)
               Ib = Box_Frac(Ifrac)
               Imol = Imol + 1
               IF(Imol.GT.MaxMol) THEN
                  WRITE(6,*) ERROR, "Number of molecules > MaxMol"
                  STOP
               END IF
               N_MolInBox(Ib) = N_MolInBox(Ib) + 1

               I_MolInBox(Ib,N_MolInBox(Ib)) = Imol
               I_MolInFrac(Ifrac,I) = Imol

               Ibox(Imol)   = Box_Frac(Ifrac)
               L_Frac(Imol) = .true.

               READ(11,*) dummy, Tm
               READ(11,*)

               TypeMol(Imol) = Tm

               XCM(Imol) = 0.0d0
               YCM(Imol) = 0.0d0
               ZCM(Imol) = 0.0d0

               DO Iatom=1,N_AtomInMolType(Tm)
                  READ(11,*) TypeAtom(Tm,Iatom), X(Imol,Iatom), Y(Imol,Iatom), Z(Imol,Iatom)

                  XCM(Imol) = XCM(Imol) + X(Imol,Iatom)
                  YCM(Imol) = YCM(Imol) + Y(Imol,Iatom)
                  ZCM(Imol) = ZCM(Imol) + Z(Imol,Iatom)
               END DO

               XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
               YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
               ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

               CALL Place_molecule_back_in_box(Imol)

               READ(11,*)
            END DO
            READ(11,*)
            READ(11,*)
         END DO

         N_MolTotal = N_MolInBox(1) + N_MolInBox(2)

         CLOSE(11)

C     Determine the vapor phase box
         DO Ib=1,N_Box
            Nwhole(Ib) = 0
            DO Tm=1,N_MolType
               Nwhole(Ib) = Nwhole(Ib) + Nmptpb(Ib,Tm)
            END DO
            Numdens(Ib) = dble(Nwhole(Ib))/Volume(Ib)
         END DO

         IF(N_Box.EQ.1) THEN
            Ibvapor = 1
         ELSEIF(Numdens(1).LT.Numdens(2)) THEN
            Ibvapor = 1
         ELSE
            Ibvapor = 2
         END IF

C      Check system
         CALL Check_System(Lresult)


         IF(Lresult) THEN
            WRITE(6,'(A,A)') OK, "Read from ./INPUT/restart.in"
         ELSE
            WRITE(6,'(A,A,A)') WARNING, "Read from ./INPUT/restart.in but something might be wrong with the format. ",
     &            "Check the Warning/Error messages."
         END IF
         WRITE(6,*)

      END IF

      Return
      End
