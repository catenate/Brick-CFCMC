      SUBROUTINE LambdaMove(Ifrac)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"
      include "settings.inc"

C     Change the value of the fractional parameter lambda

      integer Ib,Ifrac,Tf,Rs,I,J,Select_Random_Integer,Imol(MaxMolInFrac),IbinNew,IbinOld,Tm,It,Iatom,Kmol
      double precision Ran_Uniform,LambdaOld,LambdaNew,Enew,Eold,dE,dW,dE_EL_Four,
     &   E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_Bending,
     &   E_EL_Intra,E_EL_Excl,E_Torsion,E_LJ_InterOld,E_LJ_IntraOld,E_EL_RealOld,E_BendingOld,
     &   E_EL_IntraOld,E_EL_ExclOld,E_TorsionOld,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_BendingNew,
     &   E_EL_IntraNew,E_EL_ExclNew,E_TorsionNew,E_LJ_TailOld,E_EL_SelfOld,E_LJ_TailNew,E_EL_SelfNew,Myl,Myc,Myi
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

      IF(N_Frac.EQ.0) RETURN

 100  CONTINUE
      Ifrac     = Select_Random_Integer(N_Frac)
      Tf        = Type_Frac(Ifrac)

      IF(Tf.EQ.4) GO TO 100

      Ib        = Box_Frac(Ifrac)
      LambdaOld = Lambda_Frac(Ifrac)

      IF(Tf.EQ.1) THEN
         Rs = 1
      ELSEIF(Tf.EQ.2) THEN
         Rs = 1
      ELSEIF(Tf.EQ.3) THEN
         Rs = ReactionStep_Frac(Ifrac)
      END IF

      LEwald = .false.
C     Get molecule labels of fractional
      DO I=1,N_MolInFrac(Ifrac)
         Imol(I) = I_MolInFrac(Ifrac,I)
         IF(L_ChargeInMolType(TypeMol(Imol(I))).AND.L_Ewald(Ib)) LEwald = .true.
      END DO

C     Check if molecules forming the fractional are in the same box and have the same lambda
      DO I=1,N_MolInFrac(Ifrac)
         IF(Ibox(Imol(I)).NE.Ib) THEN
            WRITE(6,'(A,A)') ERROR, "Fractional parts are in different boxes"
            WRITE(6,'(A,i3,1x,i2,1x,i5,1x,i1,1x,i1)')
     &         "Ifrac, I, Imol(I), Box_Frac(Ifrac), Ibox(Imol(I))",
     &          Ifrac, I, Imol(I), Box_Frac(Ifrac), Ibox(Imol(I))
            STOP
         END IF
      END DO

      LambdaNew = LambdaOld + (2.0d0*Ran_Uniform()-1.0d0)*Delta_Lambda(Ib,Ifrac)

      IF(LambdaNew.GE.1.0d0.OR.LambdaNew.LE.0.0d0) RETURN

      IF(LEwald) CALL Ewald_Init

      IbinOld = 1 + int(dble(N_LambdaBin(Ifrac))*LambdaOld)
      IbinNew = 1 + int(dble(N_LambdaBin(Ifrac))*LambdaNew)

      TrialLambdaMove(Ib,Rs,Ifrac) = TrialLambdaMove(Ib,Rs,Ifrac) + 1.0d0

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN
         DO I=1,N_MolInFrac(Ifrac)
            Kmol = Imol(I)
            Tm = TypeMol(Kmol)
            IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
            CALL interactionlambda(Kmol,Myl,Myc,Myi)
            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                  J = NKSPACE(Ib,1)
                  XKSPACE(J,Ib,1) = X(Kmol,Iatom)
                  YKSPACE(J,Ib,1) = Y(Kmol,Iatom)
                  ZKSPACE(J,Ib,1) = Z(Kmol,Iatom)
                  QKSPACE(J,Ib,1) = Myc*Q(It)
               END IF
            END DO
         END DO
      END IF

CCC   ENERGY OF OLD CONFIGURATION
      E_LJ_TailOld = U_LJ_Tail(Ib)
      E_EL_SelfOld = U_EL_Self(Ib)

      E_LJ_InterOld = 0.0d0
      E_LJ_IntraOld = 0.0d0
      E_EL_RealOld  = 0.0d0
      E_EL_IntraOld = 0.0d0
      E_EL_ExclOld  = 0.0d0
      E_BendingOld  = 0.0d0
      E_TorsionOld  = 0.0d0

      DO I=1,N_MolInFrac(Ifrac)
         CALL Energy_Molecule(Imol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (Lambdamove)"
            STOP
         END IF
         E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
         E_LJ_IntraOld = E_LJ_IntraOld + E_LJ_Intra
         E_EL_RealOld  = E_EL_RealOld  + E_EL_Real
         E_EL_IntraOld = E_EL_IntraOld + E_EL_Intra
         E_EL_ExclOld  = E_EL_ExclOld  + E_EL_Excl
         E_BendingOld  = E_BendingOld  + E_Bending
         E_TorsionOld  = E_TorsionOld  + E_Torsion
      END DO

C     Correct for counting LJ and Electrostatic energy double
      DO I=1,N_MolInFrac(Ifrac)-1
         DO J=I+1,N_MolInFrac(Ifrac)
            CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
            IF(L_Overlap_Inter) THEN
               WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (Lambdamove)"
               STOP
            END IF
            E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
            E_EL_RealOld  = E_EL_RealOld  - E_EL_Real
         END DO
      END DO

      Eold = E_LJ_InterOld + E_LJ_IntraOld + E_EL_RealOld + E_EL_IntraOld + E_BendingOld
     &     + E_EL_ExclOld + E_TorsionOld + E_LJ_TailOld + E_EL_SelfOld

      Lambda_Frac(Ifrac) = LambdaNew

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN
         DO I=1,N_MolInFrac(Ifrac)
            Kmol = Imol(I)
            Tm = TypeMol(Kmol)
            IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
            CALL interactionlambda(Kmol,Myl,Myc,Myi)
            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                  J = NKSPACE(Ib,2)
                  XKSPACE(J,Ib,2) = X(Kmol,Iatom)
                  YKSPACE(J,Ib,2) = Y(Kmol,Iatom)
                  ZKSPACE(J,Ib,2) = Z(Kmol,Iatom)
                  QKSPACE(J,Ib,2) = Myc*Q(It)
               END IF
            END DO
         END DO
      END IF

CCC   ENERGY OF NEW CONFIGURATION
      E_LJ_InterNew = 0.0d0
      E_LJ_IntraNew = 0.0d0
      E_EL_RealNew  = 0.0d0
      E_EL_IntraNew = 0.0d0
      E_EL_ExclNew  = 0.0d0
      E_BendingNew  = 0.0d0
      E_TorsionNew  = 0.0d0

      DO I=1,N_MolInFrac(Ifrac)
         CALL Energy_Molecule(Imol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') "Energy Overlap (Lambdamove)"
            STOP
         END IF
         E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
         E_LJ_IntraNew = E_LJ_IntraNew + E_LJ_Intra
         E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
         E_EL_IntraNew = E_EL_IntraNew + E_EL_Intra
         E_EL_ExclNew  = E_EL_ExclNew  + E_EL_Excl
         E_BendingNew  = E_BendingNew  + E_Bending
         E_TorsionNew  = E_TorsionNew  + E_Torsion
      END DO

C     Correct for counting LJ and Electrostatic energy double
      DO I=1,N_MolInFrac(Ifrac)-1
         DO J=I+1,N_MolInFrac(Ifrac)

            CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
            IF(L_Overlap_Inter) THEN
               WRITE(6,'(A,A)') "Intermolecular Energy Overlap (Lambdamove)"
               STOP
            END IF

            E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
            E_EL_RealNew  = E_EL_RealNew  - E_EL_Real

         END DO
      END DO

      dE_EL_Four = 0.0d0
      IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

C     If lambda changes then also tailcorrections change
      CALL Energy_Correction(Ib)

      E_LJ_TailNew = U_LJ_Tail(Ib)
      E_EL_SelfNew = U_EL_Self(Ib)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
     &     + E_EL_ExclNew + E_TorsionNew + E_LJ_TailNew + E_EL_SelfNew

CCC   ACCEPT OR REJECT
      dE = Enew - Eold + dE_EL_Four
      dW = Weight(IbinNew,Ib,Rs,Ifrac) - Weight(IbinOld,Ib,Rs,Ifrac)

      CALL Accept_or_Reject(dexp(-beta*dE+dW),Laccept)

      IF(Laccept) THEN
         AcceptLambdaMove(Ib,Rs,Ifrac) = AcceptLambdaMove(Ib,Rs,Ifrac) + 1.0d0

         U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
         U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew - E_LJ_IntraOld

         U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
         U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew - E_EL_IntraOld
         U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew  - E_EL_ExclOld
         U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

         U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew - E_BendingOld
         U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew - E_TorsionOld

         U_Total(Ib) = U_Total(Ib) + dE

         IF(LEwald) CALL Ewald_Accept(Ib)

      ELSE

         U_LJ_Tail(Ib) = E_LJ_TailOld
         U_EL_Self(Ib) = E_EL_SelfOld

         Lambda_Frac(Ifrac) = LambdaOld

      END IF

      RETURN
      END
