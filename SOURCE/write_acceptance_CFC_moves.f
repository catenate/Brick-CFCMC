      SUBROUTINE Write_Acceptance_CFC_moves
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "settings.inc"
      include "trial_moves.inc"

      integer Ib,Ibin,Tm,Ifrac,Ireac,Rs

CCC   Write acceptance ratios for CFC moves as a function of lambda

      IF(L_NVPTHybrid) THEN
         OPEN(50,file="./OUTPUT/NVPT_swap_move.dat")
         WRITE(50,'(A)')
     &   "# Swap Acceptance ratios and number of Accepted and Trial Moves (per box) as a function of Lambda"

         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.1) CYCLE
            DO Tm=1,N_MolType
               IF(N_MolOfMolTypeInFrac(Tm,Ifrac).EQ.0) CYCLE
               DO Ibin=1,N_LambdaBin(Ifrac)
                  WRITE(50,'(f8.5,2(f8.5,2(i10)))') (dble(Ibin)-0.5d0)/dble(N_LambdaBin(Ifrac)),
     &              (AcceptNVPTswapvslambda(Ibin,Ib,Tm,Ifrac)/max(1.0d0,TrialNVPTswapvslambda(Ibin,Ib,Tm,Ifrac)),
     &               int(AcceptNVPTswapvslambda(Ibin,Ib,Tm,Ifrac)), int(TrialNVPTswapvslambda(Ibin,Ib,Tm,Ifrac)), Ib=1,N_Box)
               END DO
               WRITE(50,*)
               WRITE(50,*)
            END DO
         END DO

         CLOSE (50)

         OPEN(50,file="./OUTPUT/NVPT_change_move.dat")
         WRITE(50,'(A)')
     &   "# Identity Change Acceptance ratios and number of Accepted and Trial Moves (per box) as a function of Lambda"

         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.1) CYCLE
            DO Tm=1,N_MolType
               IF(N_MolOfMolTypeInFrac(Tm,Ifrac).EQ.0) CYCLE
               DO Ibin=1,N_LambdaBin(Ifrac)
                  WRITE(50,'(f8.5,2(f8.5,2(i10)))') (dble(Ibin)-0.5d0)/dble(N_LambdaBin(Ifrac)),
     &              (AcceptNVPTchangevslambda(Ibin,Ib,Tm,Ifrac)/max(1.0d0,TrialNVPTchangevslambda(Ibin,Ib,Tm,Ifrac)),
     &               int(AcceptNVPTchangevslambda(Ibin,Ib,Tm,Ifrac)), int(TrialNVPTchangevslambda(Ibin,Ib,Tm,Ifrac)), Ib=1,N_Box)
               END DO
               WRITE(50,*)
               WRITE(50,*)
            END DO
         END DO

         CLOSE (50)
      END IF

      IF(L_GEHybrid) THEN
         OPEN(50,file="./OUTPUT/GE_swap_move.dat")
         WRITE(50,'(A)')
     &   "# Swap Acceptance ratios and number of Accepted and Trial Moves (per box) as a function of Lambda"

         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.2) CYCLE

            DO Ibin=1,N_LambdaBin(Ifrac)
               WRITE(50,'(f8.5,2(f8.5,2(i10)))') (dble(Ibin)-0.5d0)/dble(N_LambdaBin(Ifrac)),
     &           (AcceptGEswapvslambda(Ibin,Ib,Ifrac)/max(1.0d0,TrialGEswapvslambda(Ibin,Ib,Ifrac)),
     &            int(AcceptGEswapvslambda(Ibin,Ib,Ifrac)), int(TrialGEswapvslambda(Ibin,Ib,Ifrac)), Ib=1,N_Box)
            END DO
            WRITE(50,*)
            WRITE(50,*)
         END DO

         CLOSE (50)

         OPEN(50,file="./OUTPUT/GE_change_move.dat")
         WRITE(50,'(A)')
     &   "# Identity Change Acceptance ratios and number of Accepted and Trial Moves (per box) as a function of Lambda"

         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.2) CYCLE

            DO Ibin=1,N_LambdaBin(Ifrac)
               WRITE(50,'(f8.5,2(f8.5,2(i10)))') (dble(Ibin)-0.5d0)/dble(N_LambdaBin(Ifrac)),
     &           (AcceptGEchangevslambda(Ibin,Ib,Ifrac)/max(1.0d0,TrialGEchangevslambda(Ibin,Ib,Ifrac)),
     &            int(AcceptGEchangevslambda(Ibin,Ib,Ifrac)), int(TrialGEchangevslambda(Ibin,Ib,Ifrac)), Ib=1,N_Box)
            END DO
            WRITE(50,*)
            WRITE(50,*)
         END DO

         CLOSE (50)
      END IF

      IF(L_RXMCHybrid) THEN
         OPEN(50,file="./OUTPUT/RXMC_swap_move.dat")
         WRITE(50,'(A)')
     &   "# Swap Acceptance ratios and number of Accepted and Trial Moves (per box) as a function of Lambda"

         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) CYCLE
            Ireac = Reaction_Frac(Ifrac)

            DO Rs=1,N_ReactionStep(Ireac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  WRITE(50,'(f8.5,2(f8.5,2(i10)))') (dble(Ibin)-0.5d0)/dble(N_LambdaBin(Ifrac)),
     &           (AcceptRXMCswapvslambda(Ibin,Ib,Rs,Ifrac)/max(1.0d0,TrialRXMCswapvslambda(Ibin,Ib,Rs,Ifrac)),
     &            int(AcceptRXMCswapvslambda(Ibin,Ib,Rs,Ifrac)), int(TrialRXMCswapvslambda(Ibin,Ib,Rs,Ifrac)), Ib=1,N_Box)
               END DO
               WRITE(50,*)
               WRITE(50,*)
            END DO
         END DO

         CLOSE (50)

         OPEN(50,file="./OUTPUT/RXMC_change_move.dat")
         WRITE(50,'(A)')
     &   "# Identity Change Acceptance ratios and number of Accepted and Trial Moves (per box) as a function of Lambda"

         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) CYCLE
            Ireac = Reaction_Frac(Ifrac)

            DO Rs=1,N_ReactionStep(Ireac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  WRITE(50,'(f8.5,2(f8.5,2(i10)))') (dble(Ibin)-0.5d0)/dble(N_LambdaBin(Ifrac)),
     &           (AcceptRXMCchangevslambda(Ibin,Ib,Rs,Ifrac)/max(1.0d0,TrialRXMCchangevslambda(Ibin,Ib,Rs,Ifrac)),
     &            int(AcceptRXMCchangevslambda(Ibin,Ib,Rs,Ifrac)), int(TrialRXMCchangevslambda(Ibin,Ib,Rs,Ifrac)), Ib=1,N_Box)
               END DO
               WRITE(50,*)
               WRITE(50,*)
            END DO
         END DO
         CLOSE(50)
      END IF

      RETURN
      END
