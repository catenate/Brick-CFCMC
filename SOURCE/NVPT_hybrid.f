      SUBROUTINE NVPT_Hybrid(Ifrac)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"
      include "settings.inc"
      include "trial_moves.inc"

      integer Imol,Jmol,Ib,Tm,Ifrac,I,Select_Random_Integer,Ibin,Iatom,It,J
      double precision  Xold(MaxAtom),Yold(MaxAtom),Zold(MaxAtom),dX,dY,dZ,
     &     E_LJ_InterOld1,E_LJ_IntraOld1,E_EL_RealOld1,E_EL_IntraOld1,E_EL_ExclOld1,
     &     E_TorsionOld1,E_LJ_InterOld2,E_LJ_IntraOld2,E_EL_RealOld2,E_EL_ExclOld2,
     &     E_EL_IntraOld2,E_TorsionOld2,E_LJ_InterNew1,E_LJ_IntraNew1,E_EL_RealNew1,
     &     E_EL_IntraNew1,E_EL_ExclNew1,E_TorsionNew1,E_LJ_InterNew2,E_LJ_IntraNew2,
     &     E_EL_RealNew2,E_EL_IntraNew2,E_EL_ExclNew2,E_TorsionNew2,E_LJ_Interdoubleold,
     &     E_EL_Realdoubleold,E_LJ_Interdoublenew,E_EL_Realdoublenew,Eold,Enew,dE,Lf,
     &     XCMold,YCMold,ZCMold,E_LJ_InterOld,E_EL_RealOld,E_LJ_InterNew,E_EL_RealNew,
     &     E_BendingNew1,E_BendingOld1,E_BendingNew2,E_BendingOld2,dE_EL_Four,Myl,Myc,Myi,
     &     dummy1,dummy2,dummy3,dummy4,dummy5,Ran_Uniform
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

C     Select a Random Fractional
      IF(N_Frac.EQ.0) RETURN

 100  CONTINUE
      Ifrac = Select_Random_Integer(N_Frac)
      IF(Type_Frac(Ifrac).NE.1) GO TO 100

      Ib   = Box_Frac(Ifrac)
      Lf   = Lambda_Frac(Ifrac)
      I    = Select_Random_Integer(N_MolInFrac(Ifrac))
      Imol = I_MolInFrac(Ifrac,I)
      Tm   = TypeMol(Imol)
      Ibin = 1 + int(dble(N_LambdaBin(Ifrac))*Lf)

      LEwald = .false.
      IF(L_ChargeInMolType(Tm).AND.L_Ewald(Ib)) LEwald = .true.

      IF(LEwald) CALL Ewald_Init

      IF(Lf.LT.NVPTHybridSwapSwitch) THEN
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC                   RANDOM REINSERTION (SWAP MOVE)                        CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

         TrialNVPTswap(Ib,Tm,Ifrac) = TrialNVPTswap(Ib,Tm,Ifrac) + 1.0d0
         TrialNVPTswapvslambda(Ibin,Ib,Tm,Ifrac) = TrialNVPTswapvslambda(Ibin,Ib,Tm,Ifrac) + 1.0d0

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            CALL interactionlambda(Imol,Myl,Myc,Myi)
            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                  J = NKSPACE(Ib,1)
                  XKSPACE(J,Ib,1) = X(Imol,Iatom)
                  YKSPACE(J,Ib,1) = Y(Imol,Iatom)
                  ZKSPACE(J,Ib,1) = Z(Imol,Iatom)
                  QKSPACE(J,Ib,1) = Myc*Q(It)
               END IF
            END DO
         END IF

C     Energy of old configuration
         CALL Energy_Molecule(Imol,E_LJ_InterOld,dummy1,E_EL_RealOld,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (NVPT Hybrid)"
            STOP
         END IF

         Eold = E_LJ_InterOld + E_EL_RealOld

C     Store old configuration
         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Iatom) = X(Imol,Iatom)
            Yold(Iatom) = Y(Imol,Iatom)
            Zold(Iatom) = Z(Imol,Iatom)
         END DO

         XCMold = XCM(Imol)
         YCMold = YCM(Imol)
         ZCMold = ZCM(Imol)

CCC   New configuration
C     Random Orientation
         CALL Random_Orientation(Imol)

         XCM(Imol) = 0.0d0
         YCM(Imol) = 0.0d0
         ZCM(Imol) = 0.0d0

C     Random Position
         dX = Ran_Uniform()*BoxSize(Ib)
         dY = Ran_Uniform()*BoxSize(Ib)
         dZ = Ran_Uniform()*BoxSize(Ib)

         DO Iatom=1,N_AtomInMolType(Tm)
            X(Imol,Iatom) = Xold(Iatom) + dX
            Y(Imol,Iatom) = Yold(Iatom) + dY
            Z(Imol,Iatom) = Zold(Iatom) + dZ

            XCM(Imol) = XCM(Imol) + X(Imol,Iatom)
            YCM(Imol) = YCM(Imol) + Y(Imol,Iatom)
            ZCM(Imol) = ZCM(Imol) + Z(Imol,Iatom)
         END DO

         XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
         YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
         ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

C     Place molecule back in box
         CALL Place_molecule_back_in_box(Imol)

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            CALL interactionlambda(Imol,Myl,Myc,Myi)
            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                  J = NKSPACE(Ib,2)
                  XKSPACE(J,Ib,2) = X(Imol,Iatom)
                  YKSPACE(J,Ib,2) = Y(Imol,Iatom)
                  ZKSPACE(J,Ib,2) = Z(Imol,Iatom)
                  QKSPACE(J,Ib,2) = Myc*Q(It)
               END IF
            END DO
         END IF

C     Energy of new configuration
         CALL Energy_Molecule(Imol,E_LJ_InterNew,dummy1,E_EL_RealNew,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (NVPT Hybrid)"
            STOP
         END IF

         dE_EL_Four = 0.0d0
         IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

         Enew = E_LJ_InterNew + E_EL_RealNew

CCC   Accept or reject
         dE  = Enew - Eold + dE_EL_Four

         CALL Accept_or_Reject(dexp(-beta*dE),Laccept)

         IF(Laccept) THEN
            AcceptNVPTswap(Ib,Tm,Ifrac) = AcceptNVPTswap(Ib,Tm,Ifrac) + 1.0d0
            AcceptNVPTswapvslambda(Ibin,Ib,Tm,Ifrac) = AcceptNVPTswapvslambda(Ibin,Ib,Tm,Ifrac) + 1.0d0

            U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld

            U_EL_Real(Ib)  = U_EL_Real(Ib) + E_EL_RealNew - E_EL_RealOld
            U_EL_Four(Ib)  = U_EL_Four(Ib) + dE_EL_Four

            U_Total(Ib) = U_Total(Ib) + dE

            IF(LEwald) CALL Ewald_Accept(Ib)

         ELSE

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Imol,Iatom) = Xold(Iatom)
               Y(Imol,Iatom) = Yold(Iatom)
               Z(Imol,Iatom) = Zold(Iatom)
            END DO

            XCM(Imol) = XCMold
            YCM(Imol) = YCMold
            ZCM(Imol) = ZCMold

         END IF

      ELSEIF(Lf.GT.NVPTHybridChangeSwitch) THEN
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC                    IDENTITY CHANGE (CHANGE MOVE)                        CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

         IF(Nmptpb(Ib,Tm).EQ.0) RETURN

         TrialNVPTchange(Ib,Tm,Ifrac) = TrialNVPTchange(Ib,Tm,Ifrac) + 1.0d0
         TrialNVPTchangevslambda(Ibin,Ib,Tm,Ifrac) = TrialNVPTchangevslambda(Ibin,Ib,Tm,Ifrac) + 1.0d0

         Jmol = Imptpb(Ib,Tm,Select_Random_Integer(Nmptpb(Ib,Tm)))

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            CALL interactionlambda(Imol,Myl,Myc,Myi)
            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                  J = NKSPACE(Ib,1)
                  XKSPACE(J,Ib,1) = X(Imol,Iatom)
                  YKSPACE(J,Ib,1) = Y(Imol,Iatom)
                  ZKSPACE(J,Ib,1) = Z(Imol,Iatom)
                  QKSPACE(J,Ib,1) = Myc*Q(It)

                  NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                  J = NKSPACE(Ib,2)
                  XKSPACE(J,Ib,2) = X(Imol,Iatom)
                  YKSPACE(J,Ib,2) = Y(Imol,Iatom)
                  ZKSPACE(J,Ib,2) = Z(Imol,Iatom)
                  QKSPACE(J,Ib,2) = Q(It)

                  NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                  J = NKSPACE(Ib,1)
                  XKSPACE(J,Ib,1) = X(Jmol,Iatom)
                  YKSPACE(J,Ib,1) = Y(Jmol,Iatom)
                  ZKSPACE(J,Ib,1) = Z(Jmol,Iatom)
                  QKSPACE(J,Ib,1) = Q(It)

                  NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                  J = NKSPACE(Ib,2)
                  XKSPACE(J,Ib,2) = X(Jmol,Iatom)
                  YKSPACE(J,Ib,2) = Y(Jmol,Iatom)
                  ZKSPACE(J,Ib,2) = Z(Jmol,Iatom)
                  QKSPACE(J,Ib,2) = Myc*Q(It)
               END IF
            END DO
         END IF

CCC   ENERGY OF OLD CONFIGURATION
         CALL Energy_Molecule(Imol,E_LJ_InterOld1,E_LJ_IntraOld1,E_EL_RealOld1,E_EL_IntraOld1,
     &                             E_EL_ExclOld1,E_BendingOld1,E_TorsionOld1,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (NVPT Hybrid)"
            STOP
         END IF
         CALL Energy_Molecule(Jmol,E_LJ_InterOld2,E_LJ_IntraOld2,E_EL_RealOld2,E_EL_IntraOld2,
     &                             E_EL_ExclOld2,E_BendingOld2,E_TorsionOld2,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (NVPT Hybrid)"
            STOP
         END IF

C     Correct for counting LJ and Electrostatic energy double
         CALL Energy_Intermolecular(Imol,Jmol,E_LJ_Interdoubleold,E_EL_Realdoubleold,L_Overlap_Inter)
         IF(L_Overlap_Inter) THEN
            WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (NVPT Hybrid)"
            STOP
         END IF

         Eold = E_LJ_InterOld1 + E_LJ_IntraOld1 + E_EL_RealOld1 + E_EL_IntraOld1 + E_EL_ExclOld1
     &     + E_TorsionOld1 + E_LJ_InterOld2 + E_LJ_IntraOld2 + E_EL_RealOld2 + E_EL_IntraOld2
     &     + E_EL_ExclOld2 + E_TorsionOld2 - E_LJ_Interdoubleold - E_EL_Realdoubleold
     &     + E_BendingOld1 + E_BendingOld2

C     Change fractional into whole
         L_Frac(Imol)  = .false.
         L_Frac(Jmol)  = .true.

         I_MolInFrac(Ifrac,I) = Jmol

         CALL Find_molecule_in_Imptpb(Jmol,Ib,Tm,J)
         Imptpb(Ib,Tm,J) = Imol

CCC   ENERGY OF NEW CONFIGURATION
         CALL Energy_Molecule(Imol,E_LJ_InterNew1,E_LJ_IntraNew1,E_EL_RealNew1,E_EL_IntraNew1,
     &                             E_EL_ExclNew1,E_BendingNew1,E_TorsionNew1,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            Laccept=.false.
            GO TO 2
         END IF
         CALL Energy_Molecule(Jmol,E_LJ_InterNew2,E_LJ_IntraNew2,E_EL_RealNew2,E_EL_IntraNew2,
     &                             E_EL_ExclNew2,E_BendingNew2,E_TorsionNew2,L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            Laccept=.false.
            GO TO 2
         END IF

C     Correct for counting LJ and Electrostatic energy double
         CALL Energy_Intermolecular(Imol,Jmol,E_LJ_Interdoublenew,E_EL_Realdoublenew,L_Overlap_Inter)
         IF(L_Overlap_Inter) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (NVPT Hybrid)"
            STOP
         END IF

         Enew = E_LJ_InterNew1 + E_LJ_IntraNew1 + E_EL_RealNew1 + E_EL_IntraNew1 + E_EL_ExclNew1
     &      + E_TorsionNew1 + E_LJ_InterNew2 + E_LJ_IntraNew2 + E_EL_RealNew2 + E_EL_IntraNew2
     &      + E_EL_ExclNew2 + E_TorsionNew2 - E_LJ_Interdoublenew - E_EL_Realdoublenew
     &      + E_BendingNew1 + E_BendingNew2

         dE_EL_Four = 0.0d0
         IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

         dE = Enew - Eold + dE_EL_Four

         CALL Accept_or_Reject(dexp(-beta*dE),Laccept)

   2     CONTINUE

         IF(Laccept) THEN
            AcceptNVPTchange(Ib,Tm,Ifrac) = AcceptNVPTchange(Ib,Tm,Ifrac) + 1.0d0
            AcceptNVPTchangevslambda(Ibin,Ib,Tm,Ifrac) = AcceptNVPTchangevslambda(Ibin,Ib,Tm,Ifrac) + 1.0d0

            U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew1 + E_LJ_InterNew2 - E_LJ_Interdoublenew
     &                                      - E_LJ_InterOld1 - E_LJ_InterOld2 + E_LJ_Interdoubleold

            U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew1 + E_LJ_IntraNew2
     &                                      - E_LJ_IntraOld1 - E_LJ_IntraOld2


            U_EL_Real(Ib) = U_EL_Real(Ib) + E_EL_RealNew1 + E_EL_RealNew2 - E_EL_Realdoublenew
     &                                      - E_EL_RealOld1 - E_EL_RealOld2 + E_EL_Realdoubleold

            U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew1 + E_EL_IntraNew2
     &                                      - E_EL_IntraOld1 - E_EL_IntraOld2

            U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew1  + E_EL_ExclNew2
     &                                      - E_EL_ExclOld1  - E_EL_ExclOld2

            U_EL_Four(Ib) = U_EL_Four(Ib) + dE_EL_Four

            U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew1 + E_BendingNew2
     &                                                - E_BendingOld1 - E_BendingOld2

            U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew1 + E_TorsionNew2
     &                                                - E_TorsionOld1 - E_TorsionOld2

            U_Total(Ib) = U_Total(Ib) + dE

            IF(LEwald) CALL Ewald_Accept(Ib)

         ELSE

            L_Frac(Imol)  = .true.
            L_Frac(Jmol)  = .false.

            I_MolInFrac(Ifrac,I) = Imol

            CALL Find_molecule_in_Imptpb(Imol,Ib,Tm,J)
            Imptpb(Ib,Tm,J) = Jmol

         END IF

      END IF

      RETURN
      END
