      SUBROUTINE Find_molecule_in_I_MolInBox(Imol,Ib,J)
      implicit none

      include "global_variables.inc"
      include "output.inc"
      
      integer Imol,Ib,I,J

      J=-10

C     Find the position of the label Imol in the list I_MolInBox
      DO I=1,N_MolInBox(Ib)

         IF(I_MolInBox(Ib,I).EQ.Imol) THEN
            J=I
            RETURN
         END IF

      END DO

      IF(J.EQ.-10) THEN
         WRITE(6,'(A,A)') ERROR, "Molecule not found in list I_MolInBox"
         WRITE(6,'(i5,A,i1)') Imol, " in box ", Ib
         STOP
      END IF

      RETURN
      END
