      SUBROUTINE Finish_Simulation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "main.inc"
      include "output.inc"
      include "settings.inc"
      include "trial_moves.inc"

      integer I,J,Ib,Ifrac,Tm,Ireac,Ipair,RsReactant,RsProduct,Rs
      double precision ProductOfDensityR(2),ProductOfDensityB(2),
     &                 PartialMolarEnthalpy,PartialMolarVolume,
     &                 EqconstB,EqconstR
      logical Ldrift,Lresult
      character*38  Creac_temp
      character*100 FMT(3)

      WRITE(6,*)
      WRITE(6,*)

      IF(Simulation_Phase.EQ.0) THEN
         WRITE(6,'(A,A,A,A)') char(13),
     &  "Program was interrupted before finishing reading the input and setting up the simulation."
         STOP
      ELSEIF(Simulation_Phase.EQ.1) THEN
         WRITE(6,'(A,A,A,A)') char(13),
     &  "Simulation was interrupted before reaching the equilibration and production phase."
         STOP
      ELSEIF(Simulation_Phase.EQ.2) THEN
         WRITE(6,'(A,A,A,A)') char(13),
     &  "Simulation was interrupted before completing equilibration and starting the production phase."
         WRITE(6,'(A,i10)') "Killed at equilibrating cycle:", Icycle
         CALL Write_System_Properties(2,Icycle)
         CALL Wang_Landau_Finalize_Weightfunction
         WRITE(6,*)
         STOP
      ELSEIF(Simulation_Phase.EQ.3) THEN
         WRITE(6,'(A,A,A,A)') char(13),
     &  "Simulation was interrupted before completing the production phase."
         WRITE(6,'(A,i10)') "Killed at production cycle:", Nmctot-Icycle
         CALL Write_System_Properties(2,Icycle)
         CALL Write_Averages(2,Icycle)
         CALL Write_Lambda_Properties(2)
         CALL Write_Acceptance_Trial_Moves
         CALL Write_Acceptance_CFC_moves
         CALL Write_Acceptance_cluster_moves
         WRITE(6,*)
         STOP
      END IF

      WRITE(6,*)

CCC   Close files
      CALL Write_System_Properties(2,NMCtot)

CCC   Write RDFs to files
      IF(L_RDFMolecule) CALL Calculate_RDF(3)

CCC   Create restart file from final configuration
      CALL Write_Configuration(4)
      CALL Write_Configuration(2)
      CALL Write_Restart(NMCtot)

CCC   Write final averages to files and close files
      CALL Write_Averages(2,NMCtot)

CCC   Write final lambda properties to file and close files
      CALL Write_Lambda_Properties(2)

CCC   Write statistics on trial moves to file
      CALL Write_Acceptance_Trial_Moves

CCC   Write extra statistics on CFC trial moves
      CALL Write_Acceptance_CFC_moves

CCC   Write extra statistics on cluster trial moves
      CALL Write_Acceptance_cluster_moves

      CALL Umbrella_Sampling_Finalize

CCC   Calculate energy-drifts and write to screen
      CALL Check_for_Energydrift(Ldrift)

CCC   Calculate acceptance rates and write to screen
      CALL WRITE_HEADER("TRIAL MOVE ACCEPTANCE RATES")
      IF(N_Box.EQ.2) THEN
         WRITE(6,'(A)') TRIM(Cbox(1))
         WRITE(6,'(A)') TRIM(bars(N_Box))
      ELSE
         WRITE(6,'(A)')
      END IF

      IF(L_Translation) THEN
         WRITE(6,'(A)') "Translation"
         DO Tm=1,N_MolType
            WRITE(6,'(1x,A24,19x,2(f9.5,11x))') C_MolType(Tm),
     &        (AcceptTranslation(Ib,Tm)/max(TrialTranslation(Ib,Tm),1.0d0), Ib=1,N_Box)
         END DO
      END IF

      IF(L_Rotation) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Rotation"
         DO Tm=1,N_MolType
            WRITE(6,'(1x,A24,19x,2(f9.5,11x))') C_MolType(Tm),
     &        (AcceptRotation(Ib,Tm)/max(TrialRotation(Ib,Tm),1.0d0), Ib=1,N_Box)
         END DO
      END IF

      IF(L_Bending) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Bond Bending"
         DO Tm=1,N_MolType
            IF(N_BendingInMolType(Tm).NE.0) THEN
               WRITE(6,'(1x,A24)') C_MolType(Tm)
               DO I=1,N_BendingInMolType(Tm)
                  IF(I.LE.9) THEN
                     WRITE(6,'(A,1x,i1,33x,2(f9.5,11x))') "  Bending", I,
     &                (AcceptBending(Ib,Tm,I)/max(TrialBending(Ib,Tm,I),1.0d0), Ib=1,N_Box)
                  ELSE
                     WRITE(6,'(A,1x,i2,32x,2(f9.5,11x))') "  Bending", I,
     &                (AcceptBending(Ib,Tm,I)/max(TrialBending(Ib,Tm,I),1.0d0), Ib=1,N_Box)
                  END IF
               END DO
            END IF
         END DO
      END IF

      IF(L_Torsion) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Torsion"
         DO Tm=1,N_MolType
            IF(N_TorsionInMolType(Tm).NE.0) THEN
               WRITE(6,'(1x,A24)') C_MolType(Tm)
               DO I=1,N_TorsionInMolType(Tm)
                  IF(I.LE.9) THEN
                     WRITE(6,'(A,1x,i1,33x,2(f9.5,11x))') "  Torsion", I,
     &                (AcceptTorsion(Ib,Tm,I)/max(TrialTorsion(Ib,Tm,I),1.0d0), Ib=1,N_Box)
                  ELSE
                     WRITE(6,'(A,1x,i2,32x,2(f9.5,11x))') "  Torsion", I,
     &                (AcceptTorsion(Ib,Tm,I)/max(TrialTorsion(Ib,Tm,I),1.0d0), Ib=1,N_Box)
                  END IF
               END DO
            END IF
         END DO
      END IF

      IF(L_PairTranslation) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Pair Translation"
         DO Ipair=1,N_MolTypePair
            WRITE(6,'(1x,A40,3x,2(f9.5,11x))') C_MolTypePair(Ipair),
     &      (AcceptPairTranslation(Ib,Ipair)/max(TrialPairTranslation(Ib,Ipair),1.0d0), Ib=1,N_Box)
         END DO
      END IF

      IF(L_PairRotation) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Pair Rotation"
         DO Ipair=1,N_MolTypePair
            WRITE(6,'(1x,A40,3x,2(f9.5,11x))') C_MolTypePair(Ipair),
     &      (AcceptPairRotation(Ib,Ipair)/max(TrialPairRotation(Ib,Ipair),1.0d0), Ib=1,N_Box)
         END DO
      END IF

      IF(L_ClusterTranslation) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Cluster Translation"
         WRITE(6,'(1x,A5,38x,2(f9.5,11x))') "Total",
     &      (AcceptClusterTranslation(Ib)/max(TrialClusterTranslation(Ib),1.0d0), Ib=1,N_Box)
      END IF


      IF(L_ClusterRotation) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Cluster Rotation"
         WRITE(6,'(1x,A5,38x,2(f9.5,11x))') "Total",
     &      (AcceptClusterRotation(Ib)/max(TrialClusterRotation(Ib),1.0d0), Ib=1,N_Box)
      END IF

      IF(L_SmartTranslation) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A,27x,4(f9.5,11x))') "Smart Translation",
     &        (AcceptSmartTranslation(Ib)/max(TrialSmartTranslation(Ib),1.0d0), Ib=1,N_Box)
      END IF

      IF(L_SmartRotation) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A,30x,2(f9.5,11x))') "Smart Rotation",
     &        (AcceptSmartRotation(Ib)/max(TrialSmartRotation(Ib),1.0d0), Ib=1,N_Box)
      END IF

      IF(L_LambdaMove) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Lambda Move"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) THEN
               Rs = 1
               WRITE(6,'(1x,A24,19x,2(f9.5,11x))') Name_Frac(Ifrac),
     &           (AcceptLambdaMove(Ib,Rs,Ifrac)/max(TrialLambdaMove(Ib,Rs,Ifrac),1.0d0), Ib=1,N_Box)
            ELSE
               Ireac = Reaction_Frac(Ifrac)
               DO Rs=1,N_ReactionStep(Ireac)
                  WRITE(6,'(1x,A1,i1,A1,1x,A24,15x,2(f9.5,11x))') "[", Ireac, "]", Name_ReactionStep(Ireac,Rs),
     &              (AcceptLambdaMove(Ib,Rs,Ifrac)/max(TrialLambdaMove(Ib,Rs,Ifrac),1.0d0), Ib=1,N_Box)
               END DO
            END IF
         END DO
      END IF

      IF(L_GCMCLambdaMove) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "GCMC Lambda Move"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.4) CYCLE
            WRITE(6,'(1x,A24)') Name_Frac(Ifrac)
            WRITE(6,'(2x,A12,30x,2(f9.5,11x))') "Lambda Move ",
     & (AcceptGCMCLambdaMoveLambdaMove(Ib,Ifrac)/max(TrialGCMCLambdaMoveLambdaMove(Ib,Ifrac),1.0d0), Ib=1,N_Box)
            WRITE(6,'(2x,A12,30x,2(f9.5,11x))') "Deletion    ",
     & (AcceptGCMCLambdaMoveDeletion(Ib,Ifrac)/max(TrialGCMCLambdaMoveDeletion(Ib,Ifrac),1.0d0), Ib=1,N_Box)
            WRITE(6,'(2x,A12,30x,2(f9.5,11x))') "Insertion   ",
     & (AcceptGCMCLambdaMoveInsertion(Ib,Ifrac)/max(TrialGCMCLambdaMoveInsertion(Ib,Ifrac),1.0d0), Ib=1,N_Box)
         END DO
      END IF

      IF(L_NVPTHybrid) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Fractional Reinsertion"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.1) CYCLE
            WRITE(6,'(1x,A24,$)') Name_Frac(Ifrac)
            IF(N_MolInFrac(Ifrac).EQ.1) THEN
               Tm=TypeMol(I_MolInFrac(Ifrac,1))
               WRITE(6,'(19x,2(f9.5,11x))') (AcceptNVPTswap(Ib,Tm,Ifrac)/max(TrialNVPTswap(Ib,Tm,Ifrac),1.0d0), Ib=1,N_Box)
            ELSE
               WRITE(6,*)
               DO Tm=1,N_MolType
                  IF(N_MolofMolTypeInFrac(Tm,Ifrac).NE.0) THEN
                     WRITE(6,'(2x,A24,18x,2(f9.5,11x))') C_MolType(Tm),
     &                     (AcceptNVPTswap(Ib,Tm,Ifrac)/max(TrialNVPTswap(Ib,Tm,Ifrac),1.0d0), Ib=1,N_Box)
                  END IF
               END DO
            END IF
         END DO

         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "Fractional Identity Change"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.1) CYCLE
            WRITE(6,'(1x,A24,$)') Name_Frac(Ifrac)
            IF(N_MolInFrac(Ifrac).EQ.1) THEN
               Tm=TypeMol(I_MolInFrac(Ifrac,1))
               WRITE(6,'(19x,2(f9.5,11x))') (AcceptNVPTchange(Ib,Tm,Ifrac)/max(TrialNVPTchange(Ib,Tm,Ifrac),1.0d0), Ib=1,N_Box)
            ELSE
               WRITE(6,*)
               DO Tm=1,N_MolType
                  IF(N_MolofMolTypeInFrac(Tm,Ifrac).NE.0) THEN
                     WRITE(6,'(2x,A24,18x,2(f9.5,11x))') C_MolType(Tm),
     &               (AcceptNVPTchange(Ib,Tm,Ifrac)/max(TrialNVPTchange(Ib,Tm,Ifrac),1.0d0), Ib=1,N_Box)
                  END IF
               END DO
            END IF
         END DO
      END IF

      IF(L_GEhybrid) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "GE Swap Fractional"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.2) CYCLE
            WRITE(6,'(1x,A24,19x,2(f9.5,11x))') Name_Frac(Ifrac),
     &        (AcceptGEswap(Ib,Ifrac)/max(TrialGEswap(Ib,Ifrac),1.0d0), Ib=1,N_Box)
         END DO

         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "GE Identity Change"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.2) CYCLE
            WRITE(6,'(1x,A24,19x,2(f9.5,11x))') Name_Frac(Ifrac),
     &        (AcceptGEchange(Ib,Ifrac)/max(TrialGEchange(Ib,Ifrac),1.0d0), Ib=1,N_Box)
         END DO
      END IF

      IF(L_RXMCHybrid) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "RxMC Fractional Reaction"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) CYCLE
            Ireac = Reaction_Frac(Ifrac)
            !Ib = Box_Frac(Ifrac)
            DO Rs=1,N_ReactionStep(Ireac)
               WRITE(6,'(1x,A1,i1,A1,1x,A24,15x,2(f9.5,11x))') "[", Ireac, "]", Name_ReactionStep(Ireac,Rs),
     &           (AcceptRXMCswap(Ib,Rs,Ifrac)/max(TrialRXMCswap(Ib,Rs,Ifrac),1.0d0), Ib=1,N_Box)
            END DO
         END DO

         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A)') "RxMC Identity Change"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) CYCLE
            Ireac = Reaction_Frac(Ifrac)
            !Ib = Box_Frac(Ifrac)
            DO Rs=1,N_ReactionStep(Ireac)
               WRITE(6,'(1x,A1,i1,A1,1x,A24,15x,2(f9.5,11x))') "[", Ireac, "]", Name_ReactionStep(Ireac,Rs),
     &           (AcceptRXMCchange(Ib,Rs,Ifrac)/max(TrialRXMCchange(Ib,Rs,Ifrac),1.0d0), Ib=1,N_Box)
            END DO
         END DO
      END IF

      IF(L_Volume) THEN
         WRITE(6,'(A)') TRIM(bars(N_Box))
         WRITE(6,'(A,38x,2(f9.5,11x))') "Volume", (AcceptVolume(Ib)/max(TrialVolume(Ib),1.0d0), Ib=1,N_Box)
      END IF

      IF(L_ClusterVolume) THEN
         WRITE(6,'(A,30x,2(f9.5,11x))') "Volume Cluster",
     &      (AcceptClusterVolume(Ib)/max(TrialClusterVolume(Ib),1.0d0), Ib=1,N_Box)
      END IF


CCC   Write averages to screen
      WRITE(6,*)
      CALL WRITE_HEADER("AVERAGES")
      WRITE(6,'(A)') TRIM(Caverages(N_Box))
      IF(N_Box.EQ.2) THEN
         WRITE(6,'(A)') Cbox(2)
         WRITE(6,'(A)') TRIM(bars(N_Box))
      END IF

      WRITE(6,'(A)') "Density"
      DO Tm=1,N_MolType
         WRITE(6,'(1x,A18,19x,2e20.10e3,3x,2e20.10e3)') C_MolType(Tm),
     &     (AvR_Densityptpb(Ib,Tm)*MolarMass(Tm)*Mconv/AvR, Ib=1,N_Box),
     &     (AvB_Densityptpb(Ib,Tm)*MolarMass(Tm)*Mconv/AvB, Ib=1,N_Box)
      END DO

      DO Ib=1,N_Box
         AvR_TotalDensity(Ib) = 0.0d0
         AvB_TotalDensity(Ib) = 0.0d0
         DO Tm=1,N_MolType
            AvR_TotalDensity(Ib) = AvR_TotalDensity(Ib) + AvR_Densityptpb(Ib,Tm)*MolarMass(Tm)*Mconv
            AvB_TotalDensity(Ib) = AvB_TotalDensity(Ib) + AvB_Densityptpb(Ib,Tm)*MolarMass(Tm)*Mconv
         END DO
      END DO
      WRITE(6,'(1x,A18,19x,2e20.10e3,3x,2e20.10e3)') "Total             ",
     &  (AvR_TotalDensity(Ib)/AvR, Ib=1,N_Box),
     &  (AvB_TotalDensity(Ib)/AvB, Ib=1,N_Box)

      WRITE(6,*)

      WRITE(6,'(A)') "Number of molecules"
      DO Tm=1,N_MolType
         WRITE(6,'(1x,A24,13x,2e20.10e3,3x,2e20.10e3)') C_MolType(Tm),
     &     (AvR_Nmptpb(Ib,Tm)/AvR, Ib=1,N_Box),
     &     (AvB_Nmptpb(Ib,Tm)/AvB, Ib=1,N_Box)
      END DO

      DO Ib=1,N_Box
         AvR_N_MolInBox(Ib) = 0.0d0
         AvB_N_MolInBox(Ib) = 0.0d0
         DO Tm=1,N_MolType
            AvR_N_MolInBox(Ib) = AvR_N_MolInBox(Ib) + AvR_Nmptpb(Ib,Tm)
            AvB_N_MolInBox(Ib) = AvB_N_MolInBox(Ib) + AvB_Nmptpb(Ib,Tm)
         END DO
      END DO
      WRITE(6,'(1x,A24,13x,2e20.10e3,3x,2e20.10e3)') "Total                   ",
     &  (AvR_N_MolInBox(Ib)/AvR, Ib=1,N_Box),
     &  (AvB_N_MolInBox(Ib)/AvB, Ib=1,N_Box)

      WRITE(6,*)

      WRITE(6,'(A)') "Mole fractions"
      DO Tm=1,N_MolType
         WRITE(6,'(1x,A24,19x,f9.5,11x,f9.5,15x,f9.5,11x,f9.5)') C_MolType(Tm),
     &     (AvR_Nmptpb(Ib,Tm)/AvR_N_MolInBox(Ib), Ib=1,N_Box),
     &     (AvB_Nmptpb(Ib,Tm)/AvB_N_MolInBox(Ib), Ib=1,N_Box)
      END DO

      WRITE(6,*)
      WRITE(6,'(A25,13x,2e20.10e3,3x,2e20.10e3)') "Volume                   ",
     &  (AvR_Volume(Ib)/AvR, Ib=1,N_Box),
     &  (AvB_Volume(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,*)

CCC   Write properties obtained from the CFC method
      IF(L_NVPT) THEN
         CALL WRITE_HEADER("PROPERTIES FROM CFCMC")
         WRITE(6,'(A)') TRIM(Caverages(N_Box))
         IF(N_Box.EQ.2) WRITE(6,'(A)') TRIM(Cbox(2))

         WRITE(6,'(A)') "Chemical Potentials"
         WRITE(6,'(A)') TRIM(doublebars(N_Box))

         DO Ifrac=1,N_Frac

            IF(Type_Frac(Ifrac).NE.1) CYCLE

            WRITE(6,*)

            IF(N_MolInFrac(Ifrac).EQ.1) THEN

               Tm = I_MolTypeInFrac(Ifrac,1)
               Rs = 1

               Ib = Box_Frac(Ifrac)

               IF(N_Box.EQ.1) THEN
                     FMT(1)="(A6,A24,8x,2e20.10e3)"
                     FMT(2)="(A16,22x,2e20.10e3)"
                     FMT(3)="(A16,22x,2e20.10e3)"
               ELSE
                  IF(Ib.EQ.1) THEN
                     FMT(1)="(A6,A24,8x,e20.10e3,23x,e20.10e3)"
                     FMT(2)="(A16,22x,e20.10e3,23x,e20.10e3)"
                     FMT(3)="(A16,22x,e20.10e3,23x,e20.10e3)"
                  ELSE
                     FMT(1)="(A6,A24,28x,e20.10e3,23x,e20.10e3)"
                     FMT(2)="(A16,42x,e20.10e3,23x,e20.10e3)"
                     FMT(3)="(A16,42x,e20.10e3,23x,e20.10e3)"
                  END IF
               END IF


               WRITE(6,FMT(1)) "Total ", Name_Frac(Ifrac),
     &           -dlog(AvR/AvR_Densityptpb(Ib,Tm))/beta
     &           -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &                /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta,
     &           -dlog(AvB/AvB_Densityptpb(Ib,Tm))/beta
     &           -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &               /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta

               WRITE(6,FMT(2))   " Ideal gas      ",
     &           -dlog(AvR/AvR_Densityptpb(Ib,Tm))/beta,
     &           -dlog(AvB/AvB_Densityptpb(Ib,Tm))/beta

               WRITE(6,FMT(3))   " Excess         ",
     &           -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &                /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta,
     &           -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &                /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta

            END IF

            WRITE(6,*)
            WRITE(6,'(A)') TRIM(bars(N_Box))

         END DO

         IF(L_ImposedPressure) THEN
            WRITE(6,'(A)')
            WRITE(6,'(A)') "Fugacity coefficients"
            WRITE(6,'(A)') TRIM(doublebars(N_Box))
            WRITE(6,'(A)')


            DO Ifrac=1,N_Frac

               IF(Type_Frac(Ifrac).NE.1) CYCLE

               Rs = 1

               IF(N_MolInFrac(Ifrac).EQ.1) THEN
                  Tm = I_MolTypeInFrac(Ifrac,1)
                  Ib = Box_Frac(Ifrac)

                  IF(N_Box.EQ.1) THEN
                     FMT(1)="(A24,14x,2e20.10e3)"
                  ELSE
                     IF(Ib.EQ.1) THEN
                        FMT(1)="(A24,14x,e20.10e3,23x,e20.10e3)"
                     ELSE
                        FMT(1)="(A24,34x,e20.10e3,23x,e20.10e3)"
                     END IF
                  END IF

                  WRITE(6,FMT(1)) Name_Frac(Ifrac),
     &              ((AvR_Nmptpb(Ib,Tm)/AvR_Volume(Ib))/(beta*Pressure*AvR_Nmptpb(Ib,Tm)/(AvR_N_MolInBox(Ib))))
     &               *(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac))
     &               /(LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)))),
     &              ((AvB_Nmptpb(Ib,Tm)/AvB_Volume(Ib))/(beta*Pressure*AvB_Nmptpb(Ib,Tm)/(AvB_N_MolInBox(Ib))))
     &               *(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac))
     &               /(LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))))

               END IF

            END DO

            WRITE(6,'(A)')
            WRITE(6,'(A)') "Excess Partial Molar Enthalpy"
            WRITE(6,'(A)') TRIM(doublebars(N_Box))
            WRITE(6,'(A)')

            DO Ifrac=1,N_Frac

               IF(Type_Frac(Ifrac).NE.1) CYCLE

               Ib = Box_Frac(Ifrac)
               Rs = 1

               IF(Ib.EQ.1) THEN
                  FMT(1)="(A24,14x,2e20.10e3)"
               ELSE
                  FMT(1)="(A24,34x,2e20.10e3)"
               END IF

               WRITE(6,FMT(1)) Name_Frac(Ifrac), -1.0d0/beta
     &               + AvR_EnthalpyvsLambda(N_LambdaBin(Ifrac),Ifrac)/LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)
     &               - AvR_HoverVvslambda(1,Ifrac)/AvR_1overVvsLambda(1,Ifrac)

            END DO

            WRITE(6,'(A)')
            WRITE(6,'(A)') "Partial Molar Volume"
            WRITE(6,'(A)') TRIM(doublebars(N_Box))
            WRITE(6,'(A)')

            DO Ifrac=1,N_Frac

               IF(Type_Frac(Ifrac).NE.1) CYCLE

               Ib = Box_Frac(Ifrac)
               Rs = 1

               IF(Ib.EQ.1) THEN
                  FMT(1)="(A24,14x,2e20.10e3)"
               ELSE
                  FMT(1)="(A24,34x,2e20.10e3)"
               END IF

               WRITE(6,FMT(1)) Name_Frac(Ifrac),
     &             AvR_VolumevsLambda(N_LambdaBin(Ifrac),Ifrac)/LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)
     &           - LambdaCounter(1,Ib,Rs,Ifrac)/AvR_1overVvsLambda(1,Ifrac)

            END DO

         END IF

         WRITE(6,*)

      END IF

      IF(L_GE) THEN
         CALL WRITE_HEADER("PROPERTIES FROM THE CFC GIBBS ENSEMBLE")
         WRITE(6,'(A)') TRIM(Caverages(2))
         WRITE(6,'(A)') Cbox(2)
         WRITE(6,'(A)') "Chemical Potentials (as an indication)"
         WRITE(6,'(A)') TRIM(doublebars(2))

         DO Ifrac=1,N_Frac

            IF(Type_Frac(Ifrac).NE.2) CYCLE

            WRITE(6,*)

            Rs = 1
            Tm = TypeMol(I_MolInFrac(Ifrac,1))

            WRITE(6,'(A6,A24,8x,2e20.10e3,3x,2e20.10e3)') "Total ", Name_Frac(Ifrac),
     &        (-dlog(AvR/AvR_Densityptpb(Ib,Tm))/beta
     &         -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &              /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta, Ib=1,N_Box),
     &        (-dlog(AvB/AvB_Densityptpb(Ib,Tm))/beta
     &         -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &              /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta, Ib=1,N_Box)

            WRITE(6,'(A10,28x,2e20.10e3,3x,2e20.10e3)') " Ideal gas",
     &        (-dlog(AvR/AvR_Densityptpb(Ib,Tm))/beta, Ib=1,N_Box),
     &        (-dlog(AvB/AvB_Densityptpb(Ib,Tm))/beta, Ib=1,N_Box)

            WRITE(6,'(A10,28x,2e20.10e3,3x,2e20.10e3)') " Excess   ",
     &        (-dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &              /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta, Ib=1,N_Box),
     &        (-dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &              /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta, Ib=1,N_Box)

            WRITE(6,*)
            WRITE(6,'(A)') TRIM(bars(N_Box))

         END DO

      END IF
      WRITE(6,*)


C     Write total chemical potentials of reactants and products
      IF(L_RXMC) THEN
         CALL WRITE_HEADER("PROPERTIES FROM THE CFC REACTION ENSEMBLE")
         WRITE(6,'(A)') TRIM(Caverages(N_Box))
         IF(N_Box.EQ.2) WRITE(6,'(A)') TRIM(Cbox(2))

         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) CYCLE

            Ireac = Reaction_Frac(Ifrac)

            WRITE(6,'(A,i1,1x,A,A)') "Reaction ", Ireac, ": ", Name_Reaction(Ireac)
            WRITE(6,'(A)') TRIM(bars(N_Box))
            WRITE(6,'(A)') "Chemical Potentials (as an indication)"
            WRITE(6,'(A)') TRIM(doublebars(N_Box))

            DO Rs=1,N_ReactionStep(Ireac)

               WRITE(6,*)

               DO Ib=1,N_Box
                  ProductOfDensityR(Ib) = 1.0d0
                  ProductOfDensityB(Ib) = 1.0d0
               END DO

               DO I=1,N_MolInReactionStep(Ireac,Rs)
                  Tm=I_MolTypeInReactionStep(Ireac,Rs,I)
                  DO Ib=1,N_Box
                     ProductOfDensityR(Ib)=ProductOfDensityR(Ib)*AvR_Densityptpb(Ib,Tm)/AvR
                     ProductOfDensityB(Ib)=ProductOfDensityB(Ib)*AvB_Densityptpb(Ib,Tm)/AvB
                  END DO
               END DO

               Ib = Box_Frac(Ifrac)

               IF(N_Box.EQ.1) THEN
                  FMT(1)="(A,A17,15x,2e20.10e3)"
                  FMT(2)="(A,27x,2e20.10e3)"
                  FMT(3)="(A,27x,2e20.10e3)"
               ELSE
                  IF(Ib.EQ.1) THEN
                     FMT(1)="(A,A17,15x,e20.10e3,23x,e20.10e3)"
                     FMT(2)="(A,27x,e20.10e3,23x,e20.10e3)"
                     FMT(3)="(A,27x,e20.10e3,23x,e20.10e3)"
                  ELSE
                     FMT(1)="(A,A17,35x,e20.10e3,23x,e20.10e3)"
                     FMT(2)="(A,47x,e20.10e3,23x,e20.10e3)"
                     FMT(3)="(A,47x,e20.10e3,23x,e20.10e3)"
                  END IF
               END IF

               WRITE(6,FMT(1)) "Total ", Name_ReactionStep(Ireac,Rs),
     &           -dlog(1.0d0/ProductOfDensityR(Ib))/beta
     &           -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &                /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta,
     &           -dlog(1.0d0/ProductOfDensityB(Ib))/beta
     &           -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &                /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta
               WRITE(6,FMT(2)) " Ideal Gas ",
     &           -dlog(1.0d0/ProductOfDensityR(Ib))/beta,
     &           -dlog(1.0d0/ProductOfDensityB(Ib))/beta
               WRITE(6,FMT(3)) " Excess    ",
     &           -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &                /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta,
     &           -dlog((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)*dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))
     &                /(LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))))/beta

            END DO
            WRITE(6,*)

            IF(L_ImposedPressure) THEN

               WRITE(6,'(A)') "Fugacity Coefficients (as an indication)"
               WRITE(6,'(A)') TRIM(doublebars(N_Box))

               Ib = Box_Frac(Ifrac)

               IF(N_Box.EQ.1) THEN
                  FMT(1)="(A24,14x,2e20.10e3)"
                  FMT(2)="(A10,1x,A24,3x,2e20.10e3)"
               ELSE
                  IF(Ib.EQ.1) THEN
                     FMT(1)="(A24,14x,e20.10e3,23x,e20.10e3)"
                     FMT(2)="(A10,1x,A24,3x,e20.10e3,23x,e20.10e3)"
                  ELSE
                     FMT(1)="(A24,34x,e20.10e3,23x,e20.10e3)"
                     FMT(2)="(A10,1x,A24,23x,e20.10e3,23x,e20.10e3)"
                  END IF
               END IF

                  DO Rs=1,N_ReactionStep(Ireac)
                     IF(N_MolInReactionStep(Ireac,Rs).EQ.1) THEN
                        WRITE(6,FMT(1)) C_MolType(I_MolTypeInReactionStep(Ireac,Rs,1)),
     &                     AvR_N_MolInBox(Ib)/(beta*Pressure*AvR_Volume(Ib))*
     &                     (LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))/
     &                    ((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)
     &                     *dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)))),
     &                     AvB_N_MolInBox(Ib)/(beta*Pressure*AvB_Volume(Ib))*
     &                     (LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))/
     &                    ((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)
     &                     *dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac))))
                     ELSE
                        WRITE(6,FMT(2)) "Product of", Name_ReactionStep(Ireac,Rs),
     &                    (((AvR_N_MolInBox(Ib)/(beta*Pressure*AvR_Volume(Ib)))**N_MolInReactionStep(Ireac,Rs))*
     &                      (LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))/
     &                     ((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)
     &                      *dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)))), Ib=1,N_Box),
     &                    (((AvB_N_MolInBox(Ib)/(beta*Pressure*AvB_Volume(Ib)))**N_MolInReactionStep(Ireac,Rs))*
     &                      (LambdaCounter(1,Ib,Rs,Ifrac)*dexp(-Weight(1,Ib,Rs,Ifrac)))/
     &                     ((LambdaCounter(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)
     &                      *dexp(-Weight(N_LambdaBin(Ifrac),Ib,Rs,Ifrac)))), Ib=1,N_Box)
                     END IF
                  END DO
                  WRITE(6,*)

            END IF

            WRITE(6,'(A)') "Equilibrium Constants"
            WRITE(6,'(A)') TRIM(doublebars(N_Box))

            Ib = Box_Frac(Ifrac)

            IF(N_Box.EQ.1) THEN
               FMT(1)="(A38,2e20.10e3)"
            ELSE
               IF(Ib.EQ.1) THEN
                  FMT(1)="(A38,e20.10e3,23x,e20.10e3)"
               ELSE
                  FMT(1)="(A38,20x,e20.10e3,23x,e20.10e3)"
               END IF
            END IF

            DO I=1,N_ReactionStep(Ireac)-1
               RsReactant = I
               DO J=I+1,N_ReactionStep(Ireac)
                  RsProduct = J
                  EqconstB  = 0.0d0
                  EqconstR  = 0.0d0

                  DO Tm=1,N_MolType
                     IF(N_MolOfMolTypeInReactionStep(Ireac,RsReactant,Tm).NE.0) THEN
                        EqconstR  = EqconstR
     &                            - dble(N_MolOfMolTypeInReactionStep(Ireac,RsReactant,Tm))*dlog(AvR_Densityptpb(Ib,Tm)/AvR)
                        EqconstB  = EqconstB
     &                            - dble(N_MolOfMolTypeInReactionStep(Ireac,RsReactant,Tm))*dlog(AvB_Densityptpb(Ib,Tm)/AvB)
                     END IF
                     IF(N_MolOfMolTypeInReactionStep(Ireac,RsProduct,Tm).NE.0) THEN
                        EqconstR  = EqconstR
     &                            + dble(N_MolOfMolTypeInReactionStep(Ireac,RsProduct,Tm))*dlog(AvR_Densityptpb(Ib,Tm)/AvR)
                        EqconstB  = EqconstB
     &                            + dble(N_MolOfMolTypeInReactionStep(Ireac,RsProduct,Tm))*dlog(AvB_Densityptpb(Ib,Tm)/AvB)
                     END IF
                  END DO
                  Creac_temp=TRIM(Name_ReactionStep(Ireac,RsReactant)) // " <--> " // TRIM(Name_ReactionStep(Ireac,RsProduct))
                  WRITE(6,FMT(1)) Creac_temp, dexp(EqconstR), dexp(EqconstB)
               END DO
            END DO

            WRITE(6,*)
            IF(Ireac.NE.N_Reaction) THEN
               WRITE(6,*)
               WRITE(6,*)
            END IF

         END DO
      END IF

      IF(Linsertions) THEN
         CALL WRITE_HEADER("PARTICLE INSERTION METHODS")
         WRITE(6,*)
         CALL Chemicalpotential_Insertion(2)
         CALL Enthalpy_Insertion(2)
      END IF

      IF(L_ImposedPressure) THEN
         CALL WRITE_HEADER("PROPERTIES FROM UMBRELLA SAMPLING")
         WRITE(6,'(A)')
         WRITE(6,'(A)') "Excess Partial Molar Enthalpy"
         WRITE(6,'(A)') TRIM(doublebars(N_Box))
         WRITE(6,'(A)')

         DO Ifrac=1,N_Frac

            IF(Type_Frac(Ifrac).NE.1) CYCLE

            Ib = Box_Frac(Ifrac)
            Rs = 1

            IF(Ib.EQ.1) THEN
               FMT(1)="(A24,14x,2e20.10e3)"
            ELSE
               FMT(1)="(A24,34x,2e20.10e3)"
            END IF

            CALL Umbrella_Sampling_Get_Molar_Enthalpy(Ifrac,PartialMolarEnthalpy)

            WRITE(6,FMT(1)) Name_Frac(Ifrac), PartialMolarEnthalpy

          END DO

          WRITE(6,'(A)')
          WRITE(6,'(A)') "Partial Molar Volume"
          WRITE(6,'(A)') TRIM(doublebars(N_Box))
          WRITE(6,'(A)')

          DO Ifrac=1,N_Frac

            IF(Type_Frac(Ifrac).NE.1) CYCLE

            Ib = Box_Frac(Ifrac)
            Rs = 1

            IF(Ib.EQ.1) THEN
               FMT(1)="(A24,14x,2e20.10e3)"
            ELSE
               FMT(1)="(A24,34x,2e20.10e3)"
            END IF

            CALL Umbrella_Sampling_Get_Molar_Volume(Ifrac,PartialMolarVolume)

            WRITE(6,FMT(1)) Name_Frac(Ifrac), PartialMolarVolume

          END DO
           WRITE(6,*)

      END IF

CCC   Write average energies to screen
      CALL WRITE_HEADER("AVERAGE ENERGIES")
      WRITE(6,'(A)') TRIM(Caverages(N_Box))
      IF(N_Box.EQ.2) THEN
         WRITE(6,'(A)') Cbox(2)
         WRITE(6,'(A)') TRIM(bars(N_Box))
      END IF
      WRITE(6,'(A21)')                          "Lennard Jones        "
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Intermolecular      ", (AvR_U_LJ_Inter(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_LJ_Inter(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Tailcorrection      ", (AvR_U_LJ_Tail(Ib)/AvR,  Ib=1,N_Box),
     &                                                        (AvB_U_LJ_Tail(Ib)/AvB,  Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') "Total Intermolecular ", ((AvR_U_LJ_Inter(Ib)+AvR_U_LJ_Tail(Ib))/AvR, Ib=1,N_Box),
     &                                                        ((AvB_U_LJ_Inter(Ib)+AvB_U_LJ_Tail(Ib))/AvB, Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A21)')                          "Electrostatic        "
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Real                ", (AvR_U_EL_Real(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_EL_Real(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Exclusion           ", (AvR_U_EL_Excl(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_EL_Excl(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Selfterm            ", (AvR_U_EL_Self(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_EL_Self(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Fourier             ", (AvR_U_EL_Four(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_EL_Four(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') "Total Intermolecular ",
     &  ((AvR_U_EL_Real(Ib) + AvR_U_EL_Excl(Ib) + AvR_U_EL_Self(Ib) + AvR_U_EL_Four(Ib))/AvR , Ib=1,N_Box),
     &  ((AvB_U_EL_Real(Ib) + AvB_U_EL_Excl(Ib) + AvB_U_EL_Self(Ib) + AvB_U_EL_Four(Ib))/AvB , Ib=1,N_Box)
      WRITE(6,*)
      WRITE(6,'(A21)')                          "Intramolecular       "
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Lennard Jones       ", (AvR_U_LJ_Intra(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_LJ_Intra(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Electrostatic       ", (AvR_U_EL_Intra(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_EL_Intra(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Bending             ", (AvR_U_Bending_Total(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_Bending_Total(Ib)/AvB, Ib=1,N_Box)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') " Torsion             ", (AvR_U_Torsion_Total(Ib)/AvR, Ib=1,N_Box),
     &                                                        (AvB_U_Torsion_Total(Ib)/AvB, Ib=1,N_Box)

      WRITE(6,*)
      WRITE(6,'(A21,17x,2e20.10e3,3x,2e20.10e3)') "Total                ", ((AvR_U_LJ_Inter(Ib) + AvR_U_LJ_Tail(Ib)
     &  + AvR_U_EL_Real(Ib) + AvR_U_EL_Excl(Ib) + AvR_U_EL_Self(Ib) + AvR_U_EL_Four(Ib) + AvR_U_LJ_Intra(Ib)
     &  + AvR_U_EL_Intra(Ib) + AvR_U_Bending_Total(Ib) + AvR_U_Torsion_Total(Ib))/AvR, Ib=1,N_Box),
     & ((AvB_U_LJ_Inter(Ib) + AvB_U_LJ_Tail(Ib)
     &  + AvB_U_EL_Real(Ib) + AvB_U_EL_Excl(Ib) + AvB_U_EL_Self(Ib) + AvB_U_EL_Four(Ib) + AvB_U_LJ_Intra(Ib)
     &  + AvB_U_EL_Intra(Ib) + AvB_U_Bending_Total(Ib) + AvB_U_Torsion_Total(Ib))/AvB, Ib=1,N_Box)

      WRITE(6,*)
      WRITE(6,'(A)') hashes(N_Box)
      WRITE(6,*)


CCC   Final checks of lists and calculate total simulation time
      CALL Check_System(Lresult)

      IF(Lresult) THEN
         WRITE(6,'(A,A,A,A)') char(13), "No errors found in bookkeeping."
         WRITE(6,*)
      END IF
      IF(Ldrift) THEN
         WRITE(6,'(A,A,A,A)') char(13), "ENERGY DRIFT! Check the file OUTPUT/energy_drift.info"
         WRITE(6,*)
      END IF

      CALL Timer(0,0,2)

      WRITE(6,*)
      WRITE(6,'(A)') "Simulation finished."
      WRITE(*,*)

      STOP

      END
