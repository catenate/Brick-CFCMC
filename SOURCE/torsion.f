      SUBROUTINE Torsion
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"
      include "torsion.inc"

      integer Imol,I,Tm,Ib,Itors,Tt,Il,Iatom,Iatom1,Iatom2,Select_Random_Integer,It,J,Ifrac
      double precision phi,Ran_Uniform,s,c,Ox,Oy,Oz,Rx,Ry,Rz,R,Ri,Xo,Yo,Zo,dE,XCMold,YCMold,ZCMold,
     &     Xold(MaxAtom),Yold(MaxAtom),Zold(MaxAtom),E_LJ_InterOld,E_LJ_IntraOld,
     &     E_EL_RealOld,E_EL_IntraOld,E_EL_ExclOld,E_TorsionOld,Eold,E_LJ_InterNew,
     &     E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,E_TorsionNew,Enew,
     &     E_BendingNew,E_BendingOld,dE_EL_Four,Myl,Myc,Myi,Rm
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

      Rm = Ran_Uniform()
      IF(Rm.LT.0.95d0) THEN
         Imol = Select_Random_Integer(N_MolTotal)
      ELSE
         IF(N_Frac.EQ.0) RETURN
         Ifrac = Select_Random_Integer(N_Frac)
         I     = Select_Random_Integer(N_MolInFrac(Ifrac))
         Imol  = I_MolInFrac(Ifrac,I)
      END IF

      Tm    = TypeMol(Imol)
      Ib    = Ibox(Imol)

      IF(N_TorsionInMolType(Tm).EQ.0) RETURN

      Itors = Select_Random_Integer(N_TorsionInMolType(Tm))
      Tt    = TypeTorsion(Tm,Itors)
      Il    = Select_Random_Integer(2)      !List or complementary list

      LEwald = .false.
      IF(L_ChargeInMolType(Tm).AND.L_Ewald(Ib)) LEwald = .true.

      IF(LEwald) CALL Ewald_Init

      TrialTorsion(Ib,Tm,Itors) = TrialTorsion(Ib,Tm,Itors) + 1.0d0

      Iatom1=TorsionList(Tm,Itors,2)
      Iatom2=TorsionList(Tm,Itors,3)

      Rm = Ran_Uniform()
      IF(Rm.LT.0.9d0) THEN
         phi = 2.0d0*(Ran_Uniform()-0.5d0)*dphi(Tt)
      ELSE
         phi = 2.0d0*(Ran_Uniform()-0.5d0)*OnePi
      END IF

      s = dsin(0.5d0*phi)
      c = dcos(0.5d0*phi)

      Ox = 0.5d0*(X(Imol,Iatom1)+X(Imol,Iatom2))
      Oy = 0.5d0*(Y(Imol,Iatom1)+Y(Imol,Iatom2))
      Oz = 0.5d0*(Z(Imol,Iatom1)+Z(Imol,Iatom2))

      Rx = X(Imol,Iatom1)-X(Imol,Iatom2)
      Ry = Y(Imol,Iatom1)-Y(Imol,Iatom2)
      Rz = Z(Imol,Iatom1)-Z(Imol,Iatom2)

      R  = dsqrt(Rx*Rx+Ry*Ry+Rz*Rz)
      Ri = 1.0d0/R

      Rx = Rx*Ri
      Ry = Ry*Ri
      Rz = Rz*Ri

      DO I=1,N_AtomInMolType(Tm)
         Xold(I) = X(Imol,I)
         Yold(I) = Y(Imol,I)
         Zold(I) = Z(Imol,I)
      END DO

      XCMold = XCM(Imol)
      YCMold = YCM(Imol)
      ZCMold = ZCM(Imol)

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN
         IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi)
         DO I=1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,I)
            IF(L_Charge(It)) THEN
               NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
               J = NKSPACE(Ib,1)
               XKSPACE(J,Ib,1) = X(Imol,I)
               YKSPACE(J,Ib,1) = Y(Imol,I)
               ZKSPACE(J,Ib,1) = Z(Imol,I)
               IF(L_frac(Imol)) THEN
                  QKSPACE(J,Ib,1) = Myc*Q(It)
               ELSE
                  QKSPACE(J,Ib,1) = Q(It)
               END IF
            END IF
         END DO
      END IF

      CALL Energy_Molecule(Imol,E_LJ_InterOld,E_LJ_IntraOld,E_EL_RealOld,E_EL_IntraOld,
     &                          E_EL_ExclOld,E_BendingOld,E_TorsionOld,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Energy Overlap (Torsion)"
         STOP
      END IF

      Eold = E_LJ_InterOld + E_LJ_IntraOld + E_EL_RealOld + E_EL_IntraOld + E_EL_ExclOld + E_BendingOld + E_TorsionOld

C     ROTATE ATOMS IN THE MOLECULE
      DO I=1,Natomtorslist(Tm,Itors,Il)
         Iatom=Iatomtorslist(Tm,Itors,Il,I)

         Xo=X(Imol,Iatom)-Ox
         Yo=Y(Imol,Iatom)-Oy
         Zo=Z(Imol,Iatom)-Oz

         X(Imol,Iatom) = s*s*(Xo*(Rx*Rx-Ry*Ry-Rz*Rz) + 2.0d0*Rx*(Yo*Ry+Zo*Rz))
     &        + 2.0d0*s*c*(Zo*Ry-Yo*Rz) + c*c*Xo + Ox
         Y(Imol,Iatom) = s*s*(Yo*(Ry*Ry-Rz*Rz-Rx*Rx) + 2.0d0*Ry*(Xo*Rx+Zo*Rz))
     &        + 2.0d0*s*c*(Xo*Rz-Zo*Rx) + c*c*Yo + Oy
         Z(Imol,Iatom) = s*s*(Zo*(Rz*Rz-Rx*Rx-Ry*Ry) + 2.0d0*Rz*(Xo*Rx+Yo*Ry))
     &        + 2.0d0*s*c*(Yo*Rx-Xo*Ry) + c*c*Zo + Oz

      END DO

      XCM(Imol) = 0.0d0
      YCM(Imol) = 0.0d0
      ZCM(Imol) = 0.0d0

      DO Iatom=1,N_AtomInMolType(Tm)
         XCM(Imol) = XCM(Imol) + X(Imol,Iatom)
         YCM(Imol) = YCM(Imol) + Y(Imol,Iatom)
         ZCM(Imol) = ZCM(Imol) + Z(Imol,Iatom)
      END DO

      XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
      YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
      ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

      CALL Place_molecule_back_in_box(Imol)

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN
         IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi)
         DO I=1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,I)
            IF(L_Charge(It)) THEN
               NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
               J = NKSPACE(Ib,2)
               XKSPACE(J,Ib,2) = X(Imol,I)
               YKSPACE(J,Ib,2) = Y(Imol,I)
               ZKSPACE(J,Ib,2) = Z(Imol,I)
               IF(L_frac(Imol)) THEN
                  QKSPACE(J,Ib,2) = Myc*Q(It)
               ELSE
                  QKSPACE(J,Ib,2) = Q(It)
               END IF
            END IF
         END DO
      END IF

      CALL Energy_Molecule(Imol,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,
     &                          E_EL_ExclNew,E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
         Laccept=.false.
         GO TO 1
      END IF

      dE_EL_Four = 0.0d0
      IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_EL_ExclNew + E_BendingNew + E_TorsionNew

      dE = Enew - Eold + dE_EL_Four

      CALL Accept_or_Reject(dexp(-beta*dE),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         AcceptTorsion(Ib,Tm,Itors) = AcceptTorsion(Ib,Tm,Itors) + 1.0d0

         U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
         U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew - E_LJ_IntraOld

         U_EL_Real(Ib)  = U_EL_Real(Ib) + E_EL_RealNew - E_EL_RealOld
         U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew - E_EL_IntraOld
         U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew  - E_EL_ExclOld
         U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

         U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew - E_BendingOld
         U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew - E_TorsionOld

         U_Total(Ib) = U_Total(Ib) + dE

         IF(LEwald) CALL Ewald_Accept(Ib)

      ELSE

         DO I=1,N_AtomInMolType(Tm)
            X(Imol,I) = Xold(I)
            Y(Imol,I) = Yold(I)
            Z(Imol,I) = Zold(I)
         END DO

         XCM(Imol) = XCMold
         YCM(Imol) = YCMold
         ZCM(Imol) = ZCMold

      END IF

      RETURN
      END
