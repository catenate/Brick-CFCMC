      SUBROUTINE Construct_List_for_Torsion
      implicit none

      include "global_variables.inc"
      include "torsion.inc"
      include "output.inc"      

C     Construct torsion-atom-lists which contains the atomlabels that
C     should be rotated for each torsion in each molecule type.
C     The lists are Iatomstorslist(Tm,Itors,1 or 2,I) where Tm is the type
C     of molecule, Itors is the torsion label, 1 or 2 label the list
C     and the complementary list respectively and I is the list-index.

      integer Tm,Itors,Iatom1,Iatom2,temp_list(MaxAtom+1),I,J,K,M,
     &        temp_nrot,Iatom,Jatom
      logical Lbond,Llist

      DO Tm = 1, N_MolType

         DO Itors = 1, N_TorsionInMolType(Tm)
            Iatom1 = TorsionList(Tm,Itors,2)
            Iatom2 = TorsionList(Tm,Itors,3)
            temp_list(1) = Iatom1
            K = 1

 10         CONTINUE
            DO Iatom = 1, N_AtomInMolType(Tm)

               IF(Iatom.EQ.Iatom2) CYCLE
               temp_nrot = K

               DO J = 1, temp_nrot
                  Jatom = temp_list(J)

                  IF(Iatom.EQ.Jatom) CYCLE
                  CALL Check_for_Bond(Iatom,Jatom,Tm,Lbond)

                  IF(Lbond) THEN

                     Llist=.FALSE.
                     DO M=1,temp_nrot
                        IF(Iatom.EQ.temp_list(M)) THEN
                           Llist = .true.
                           EXIT
                        END IF
                     END DO

                     IF(.NOT.Llist) THEN
                        K = K + 1
                        temp_list(K) = Iatom
                        GO TO 10
                     END IF

                  END IF
               END DO

               DO I = 2, temp_nrot
                  Iatomtorslist(Tm,Itors,1,I-1) = temp_list(I)
               END DO
               Natomtorslist(Tm,Itors,1) = temp_nrot - 1

            END DO
         END DO
      END DO

C     Construct complementary list

      DO Tm = 1, N_MolType

         DO Itors = 1, N_TorsionInMolType(Tm)

            K = 0

            DO Iatom = 1, N_AtomInMolType(Tm)

               IF((Iatom.EQ.TorsionList(Tm,Itors,2)).OR.(Iatom.EQ.TorsionList(Tm,Itors,3))) CYCLE

               Llist = .false.

               DO I=1,Natomtorslist(Tm,Itors,1)
                  IF(Iatom.EQ.Iatomtorslist(Tm,Itors,1,I)) THEN
                     Llist = .true.
                     EXIT
                  END IF
               END DO

               IF(.NOT.Llist) THEN
                  K = K + 1
                  Iatomtorslist(Tm,Itors,2,K) = Iatom
               END IF

            END DO

            Natomtorslist(Tm,Itors,2) = K

         END DO

      END DO

C     Check if sum of number of atoms in both lists is equal to N_AtomInMolType-2
      DO Tm = 1, N_MolType

         DO Itors = 1, N_TorsionInMolType(Tm)

            IF(Natomtorslist(Tm,Itors,1)+Natomtorslist(Tm,Itors,2).NE.N_AtomInMolType(Tm)-2) THEN
               WRITE(6,'(A,A)') ERROR, "Something went wrong in constructing torsion-atom-lists for"
               WRITE(6,*) "Molecule type:", Tm
               WRITE(6,*) "Torsion number:", Itors
               STOP
            END IF

         END DO

      END DO

C     Write torsionlists to OUTPUT
      OPEN(87,file="./OUTPUT/torsions.info")

      DO Tm=1,N_MolType

         WRITE(87,'(A,i2)') "Moleculetype" ,Tm

         DO Itors=1,N_TorsionInMolType(Tm)

            WRITE(87,'(A,i3)') "Torsion ", Itors
            WRITE(87,'(100(i3,1x))') (Iatomtorslist(Tm,Itors,1,I), I=1,Natomtorslist(Tm,Itors,1))
            WRITE(87,'(100(i3,1x))') (Iatomtorslist(Tm,Itors,2,I), I=1,Natomtorslist(Tm,Itors,2))
            WRITE(87,*)

         END DO

         WRITE(87,*)

      END DO

      CLOSE(87)

      RETURN
      END
