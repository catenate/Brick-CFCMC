      SUBROUTINE interactionlambda(Imol,Lambda_LJ,Lambda_EL,Lambda_IN)
      implicit none

      include "global_variables.inc"
      include "output.inc"
      include "settings.inc"

      integer  Imol,Ifrac,Jfrac,I,J,N
      double precision Lf,Ls,Lambda_LJ,Lambda_EL,Lambda_IN,InvN

      Jfrac=0
      J=0
      DO Ifrac=1,N_Frac
         DO I=1,N_MolInFrac(Ifrac)
            IF(I_MolInFrac(Ifrac,I).EQ.Imol) THEN
               Jfrac=Ifrac
               J=I
               GO TO 1
            END IF
         END DO
      END DO

  1   CONTINUE

      IF((Jfrac.EQ.0).OR.(J.EQ.0)) THEN
         WRITE(6,'(A,A)') ERROR, "Fractional Molecule not found in Fractional Groups"
         STOP
      ELSEIF(J.GT.N_MolInFrac(Jfrac)) THEN
         WRITE(6,'(A,A)') ERROR, "Fractional Molecule not in Fractional Group"
         STOP
      END IF

      Lf = Lambda_Frac(Jfrac)
      Ls = LambdaSwitch(Jfrac)
      N = N_LambdaBin(Jfrac)

      InvN = 1.0d0/dble(N)

      IF(Lf.LT.InvN) THEN
         Lambda_LJ = 0.0d0
         Lambda_EL = 0.0d0
         Lambda_IN = 0.0d0
      ELSEIF(Lf.LT.Ls) THEN
         Lambda_LJ = (Lf-InvN)/(Ls-InvN)
         Lambda_IN = Lambda_LJ
         Lambda_EL = 0.0d0
      ELSEIF(Lf.LT.dble(N-1)*InvN) THEN
         Lambda_LJ = 1.0d0
         Lambda_IN = 1.0d0
         Lambda_EL = (Lf-Ls)/(InvN*dble(N-1)-Ls)
      ELSE
         Lambda_LJ = 1.0d0
         Lambda_EL = 1.0d0
         Lambda_IN = 1.0d0
      END IF

      RETURN
      END
