      SUBROUTINE Check_for_Bond(Iatom,Jatom,Tm,Lbond)
      implicit none

C     Check if there is a bond between Iatom and Jatom in a molecule of type Tm
      include "global_variables.inc"
      include "output.inc"      

      integer     Iatom,Jatom,Tm,H,I,J
      logical     Lbond

      IF(Iatom.EQ.Jatom) THEN
         WRITE(6,'(A,A)') ERROR, "Check bond, attempt to check for bond between the same atom"
         STOP
      END IF

      Lbond=.FALSE.
      DO H=1,N_BondInMolType(Tm)
         I=Bondlist(Tm,H,1)
         J=Bondlist(Tm,H,2)
         IF(((Iatom.EQ.I).AND.(Jatom.EQ.J)).OR.((Iatom.EQ.J).AND.(Jatom.EQ.I))) THEN
            Lbond=.TRUE.
            EXIT
         END IF
      END DO

      RETURN
      END
