      SUBROUTINE GE_Hybrid(Ifrac)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"
      include "settings.inc"
      include "trial_moves.inc"

      integer Imol(MaxMolInFrac),Jmol(MaxMolInFrac),Ibold,Ibnew,It,Kmol,Rs,
     &     Tm,Ifrac,I,J,Select_Random_Integer,Ibin,Ib,Iatom
      double precision Ran_Uniform,Factorial_Ratio,factor,
     &     Xold(MaxMolInFrac,MaxAtom),Yold(MaxMolInFrac,MaxAtom),
     &     Zold(MaxMolInFrac,MaxAtom),dX,dY,dZ,XCMold(MaxMolInFrac),
     &     YCMold(MaxMolInFrac),ZCMold(MaxMolInFrac),E_LJ_Inter,E_LJ_Intra,E_EL_Excl,
     &     E_EL_Real,E_EL_Intra,E_Torsion,E_LJ_InterOld,E_LJ_IntraOld,E_EL_ExclOld,
     &     E_EL_RealOld,E_EL_IntraOld,E_TorsionOld,E_LJ_InterNew,E_LJ_IntraNew,
     &     E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,E_TorsionNew,
     &     E_LJ_Inter1,E_LJ_Intra1,E_EL_Real1,E_EL_Intra1,E_EL_Excl1,E_Torsion1,
     &     E_LJ_Inter2,E_LJ_Intra2,E_EL_Real2,E_EL_Intra2,E_EL_Excl2,E_Torsion2,
     &     E_LJ_InterOld1,E_LJ_IntraOld1,E_EL_RealOld1,E_EL_IntraOld1,E_EL_ExclOld1,
     &     E_TorsionOld1,E_LJ_InterOld2,E_LJ_IntraOld2,E_EL_RealOld2,E_EL_ExclOld2,
     &     E_EL_IntraOld2,E_TorsionOld2,E_LJ_InterNew1,E_LJ_IntraNew1,
     &     E_EL_RealNew1,E_EL_IntraNew1,E_EL_ExclNew1,E_TorsionNew1,E_LJ_InterNew2,
     &     E_LJ_IntraNew2,E_EL_RealNew2,E_EL_IntraNew2,E_EL_ExclNew2,E_TorsionNew2,
     &     E_EL_SelfOld(2),E_LJ_TailOld(2),E_EL_SelfNew(2),E_LJ_TailNew(2),Eold,Enew,
     &     E_Bending,E_Bending1,E_Bending2,E_BendingOld,E_BendingOld1,E_BendingOld2,
     &     E_BendingNew,E_BendingNew1,E_BendingNew2,dE_EL_FourNew,dE_EL_FourOld,
     &     dE,dW,Volold,Volnew,Myl,Myc,Myi,Lf
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald(2)

      IF(N_Frac.EQ.0) RETURN

 100  CONTINUE
      Ifrac = Select_Random_Integer(N_Frac)
      IF(Type_Frac(Ifrac).NE.2) GO TO 100

      Lf    = Lambda_Frac(Ifrac)
      Ibold = Box_Frac(Ifrac)
      Ibnew = 3 - Ibold
      Ibin  = 1 + int(dble(N_LambdaBin(Ifrac))*Lf)
      Rs = 1

      LEwald(1) = .false.
      LEwald(2) = .false.
      DO I=1,N_MolInFrac(Ifrac)
         Imol(I) = I_MolInFrac(Ifrac,I)
         IF(L_ChargeInMolType(TypeMol(Imol(I)))) THEN
            IF(L_Ewald(Ibold)) LEwald(Ibold) = .true.
            IF(L_Ewald(Ibnew)) LEwald(Ibnew) = .true.
         END IF
      END DO

      IF(LEwald(Ibold).OR.LEwald(Ibnew)) CALL Ewald_Init

C     Check if molecules forming the fractional are in the same box and have the same lambda
      DO I=1,N_MolInFrac(Ifrac)
         IF(Ibox(Imol(I)).NE.Ibold) THEN
            WRITE(6,'(A,A)') ERROR, "Fractional parts are in different boxes"
            WRITE(6,'(A,i3,1x,i2,1x,i5,1x,i1,1x,i1)')
     &         "Ifrac, I, Imol(I), Box_Frac(Ifrac), Ibox(Imol(I))",
     &          Ifrac, I, Imol(I), Box_Frac(Ifrac), Ibox(Imol(I))
            STOP
         END IF
      END DO

      IF(Lf.LT.GEHybridSwapSwitch) THEN
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC               MOVE FRACTIONAL TO OTHER BOX (SWAP MOVE)                  CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

         TrialGEswap(Ibold,Ifrac) = TrialGEswap(Ibold,Ifrac) + 1.0d0
         TrialGEswapvslambda(Ibin,Ibold,Ifrac) = TrialGEswapvslambda(Ibin,Ibold,Ifrac) + 1.0d0

C     Store old coordinates
         DO I=1,N_MolInFrac(Ifrac)

            DO Iatom=1,N_AtomInMolType(TypeMol(Imol(I)))
               Xold(I,Iatom) = X(Imol(I),Iatom)
               Yold(I,Iatom) = Y(Imol(I),Iatom)
               Zold(I,Iatom) = Z(Imol(I),Iatom)
            END DO

            XCMold(I) = XCM(Imol(I))
            YCMold(I) = YCM(Imol(I))
            ZCMold(I) = ZCM(Imol(I))

         END DO

C     Store positions and charges for Ewald summation
         IF(LEwald(Ibold)) THEN
            DO I=1,N_MolInFrac(Ifrac)
               Kmol = Imol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               CALL interactionlambda(Kmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ibold,1) = NKSPACE(Ibold,1) + 1
                     J = NKSPACE(Ibold,1)
                     XKSPACE(J,Ibold,1) = X(Kmol,Iatom)
                     YKSPACE(J,Ibold,1) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ibold,1) = Z(Kmol,Iatom)
                     QKSPACE(J,Ibold,1) = Myc*Q(It)
                  END IF
               END DO
            END DO
         END IF

CCC   ENERGY OF OLD CONFIGURATION
         DO Ib=1,N_Box
            E_LJ_TailOld(Ib) = U_LJ_Tail(Ib)
            E_EL_SelfOld(Ib) = U_EL_Self(Ib)
         END DO

         E_LJ_InterOld = 0.0d0
         E_LJ_IntraOld = 0.0d0
         E_EL_RealOld  = 0.0d0
         E_EL_IntraOld = 0.0d0
         E_EL_ExclOld  = 0.0d0
         E_BendingOld  = 0.0d0
         E_TorsionOld  = 0.0d0

         DO I=1,N_MolInFrac(Ifrac)
            CALL Energy_Molecule(Imol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (GE Hybrid)"
               STOP
            END IF
            E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
            E_LJ_IntraOld = E_LJ_IntraOld + E_LJ_Intra
            E_EL_RealOld  = E_EL_RealOld  + E_EL_Real
            E_EL_IntraOld = E_EL_IntraOld + E_EL_Intra
            E_EL_ExclOld  = E_EL_ExclOld  + E_EL_Excl
            E_BendingOld  = E_BendingOld  + E_Bending
            E_TorsionOld  = E_TorsionOld  + E_Torsion
         END DO

C     Correct for counting LJ and Electrostatic energy double
         DO I=1,N_MolInFrac(Ifrac)-1
            DO J=I+1,N_MolInFrac(Ifrac)
               CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (GE Hybrid)"
                  STOP
               END IF
               E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
               E_EL_RealOld  = E_EL_RealOld  - E_EL_Real
            END DO
         END DO

         Eold = E_LJ_InterOld + E_LJ_IntraOld + E_EL_RealOld + E_EL_IntraOld + E_BendingOld
     &        + E_EL_ExclOld + E_TorsionOld + E_LJ_TailOld(Ibold) + E_LJ_TailOld(Ibnew)
     &        + E_EL_SelfOld(Ibold) + E_EL_SelfOld(Ibnew)

CCC   ENERGY OF NEW CONFIGURATION
         DO I=1,N_MolInFrac(Ifrac)
            Ibox(Imol(I)) = Ibnew

            Tm = TypeMol(Imol(I))

C     Place molecules randomly in the new box

            CALL Random_Orientation(Imol(I))

            XCM(Imol(I)) = 0.0d0
            YCM(Imol(I)) = 0.0d0
            ZCM(Imol(I)) = 0.0d0

            dX = Ran_Uniform()*BoxSize(Ibnew)
            dY = Ran_Uniform()*BoxSize(Ibnew)
            dZ = Ran_Uniform()*BoxSize(Ibnew)

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Imol(I),Iatom) = Xold(I,Iatom) + dX
               Y(Imol(I),Iatom) = Yold(I,Iatom) + dY
               Z(Imol(I),Iatom) = Zold(I,Iatom) + dZ

               XCM(Imol(I)) = XCM(Imol(I)) + X(Imol(I),Iatom)
               YCM(Imol(I)) = YCM(Imol(I)) + Y(Imol(I),Iatom)
               ZCM(Imol(I)) = ZCM(Imol(I)) + Z(Imol(I),Iatom)
            END DO

            XCM(Imol(I)) = XCM(Imol(I))/dble(N_AtomInMolType(Tm))
            YCM(Imol(I)) = YCM(Imol(I))/dble(N_AtomInMolType(Tm))
            ZCM(Imol(I)) = ZCM(Imol(I))/dble(N_AtomInMolType(Tm))

            CALL Place_molecule_back_in_box(Imol(I))

C     Update lists for swapping molecule Imol from box Ibold to Ibnew
C     Imol is added to the list of molecules in box Ibnew
            N_MolInBox(Ibnew) = N_MolInBox(Ibnew) + 1
            I_MolInBox(Ibnew,N_MolInBox(Ibnew)) = Imol(I)

            CALL Find_molecule_in_I_MolInBox(Imol(I),Ibold,J)

            I_MolInBox(Ibold,J) = I_MolInBox(Ibold,N_MolInBox(Ibold))
            N_MolInBox(Ibold)   = N_MolInBox(Ibold) - 1

         END DO

         Box_Frac(Ifrac) = Ibnew

C     Store positions and charges for Ewald summation
         IF(LEwald(Ibnew)) THEN
            DO I=1,N_MolInFrac(Ifrac)
               Kmol = Imol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               CALL interactionlambda(Kmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ibnew,2) = NKSPACE(Ibnew,2) + 1
                     J = NKSPACE(Ibnew,2)
                     XKSPACE(J,Ibnew,2) = X(Kmol,Iatom)
                     YKSPACE(J,Ibnew,2) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ibnew,2) = Z(Kmol,Iatom)
                     QKSPACE(J,Ibnew,2) = Myc*Q(It)
                  END IF
               END DO
            END DO
         END IF

C     Calculate energy of new configuration
         E_LJ_InterNew = 0.0d0
         E_LJ_IntraNew = 0.0d0
         E_EL_RealNew  = 0.0d0
         E_EL_IntraNew = 0.0d0
         E_EL_ExclNew  = 0.0d0
         E_BendingNew  = 0.0d0
         E_TorsionNew  = 0.0d0

         DO I=1,N_MolInFrac(Ifrac)
            CALL Energy_Molecule(Imol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (GE Hybrid)"
               STOP
            END IF
            E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
            E_LJ_IntraNew = E_LJ_IntraNew + E_LJ_Intra
            E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
            E_EL_IntraNew = E_EL_IntraNew + E_EL_Intra
            E_EL_ExclNew  = E_EL_ExclNew  + E_EL_Excl
            E_BendingNew  = E_BendingNew  + E_Bending
            E_TorsionNew  = E_TorsionNew  + E_Torsion
         END DO


C     Correct for counting LJ and Electrostatic energy double
         DO I=1,N_MolInFrac(Ifrac)-1
            DO J=I+1,N_MolInFrac(Ifrac)

               CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (GE Hybrid)"
                  STOP
               END IF

               E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
               E_EL_RealNew  = E_EL_RealNew  - E_EL_Real

            END DO
         END DO

         dE_EL_FourNew = 0.0d0
         dE_EL_FourOld = 0.0d0
         IF(LEwald(Ibold)) CALL Ewald_Move(Ibold,dE_EL_FourOld)
         IF(LEwald(Ibnew)) CALL Ewald_Move(Ibnew,dE_EL_FourNew)

C     Update energy corrections
         CALL Energy_Correction(Ibold)
         CALL Energy_Correction(Ibnew)

         DO Ib=1,N_Box
            E_LJ_TailNew(Ib) = U_LJ_Tail(Ib)
            E_EL_SelfNew(Ib) = U_EL_Self(Ib)
         END DO

         Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
     &        + E_EL_ExclNew + E_TorsionNew + E_LJ_TailNew(Ibold) + E_LJ_TailNew(Ibnew)
     &        + E_EL_SelfNew(Ibold) + E_EL_SelfNew(Ibnew)

C     ACCEPT OR REJECT
         Volold = BoxSize(Ibold)*BoxSize(Ibold)*BoxSize(Ibold)
         Volnew = BoxSize(Ibnew)*BoxSize(Ibnew)*BoxSize(Ibnew)

         dE = Enew - Eold + dE_EL_FourNew + dE_EL_FourOld
         dW = Weight(Ibin,Ibnew,Rs,Ifrac) - Weight(Ibin,Ibold,Rs,Ifrac)

         CALL Accept_or_Reject(dexp(-beta*dE+dW+N_MolInFrac(Ifrac)*dlog(Volnew/Volold)),Laccept)

         IF(Laccept) THEN
            AcceptGEswap(Ibold,Ifrac) = AcceptGEswap(Ibold,Ifrac) + 1.0d0
            AcceptGEswapvslambda(Ibin,Ibold,Ifrac) = AcceptGEswapvslambda(Ibin,Ibold,Ifrac) + 1.0d0

            U_LJ_Inter(Ibnew) = U_LJ_Inter(Ibnew) + E_LJ_InterNew
            U_LJ_Inter(Ibold) = U_LJ_Inter(Ibold) - E_LJ_InterOld

            U_LJ_Intra(Ibnew) = U_LJ_Intra(Ibnew) + E_LJ_IntraNew
            U_LJ_Intra(Ibold) = U_LJ_Intra(Ibold) - E_LJ_IntraOld

            U_EL_Real(Ibnew) = U_EL_Real(Ibnew) + E_EL_RealNew
            U_EL_Real(Ibold) = U_EL_Real(Ibold) - E_EL_RealOld

            U_EL_Intra(Ibnew) = U_EL_Intra(Ibnew) + E_EL_IntraNew
            U_EL_Intra(Ibold) = U_EL_Intra(Ibold) - E_EL_IntraOld

            U_EL_Excl(Ibnew) = U_EL_Excl(Ibnew) + E_EL_ExclNew
            U_EL_Excl(Ibold) = U_EL_Excl(Ibold) - E_EL_ExclOld

            U_EL_Four(Ibnew) = U_EL_Four(Ibnew) + dE_EL_FourNew
            U_EL_Four(Ibold) = U_EL_Four(Ibold) + dE_EL_FourOld

            U_Bending_Total(Ibnew) = U_Bending_Total(Ibnew) + E_BendingNew
            U_Bending_Total(Ibold) = U_Bending_Total(Ibold) - E_BendingOld

            U_Torsion_Total(Ibnew) = U_Torsion_Total(Ibnew) + E_TorsionNew
            U_Torsion_Total(Ibold) = U_Torsion_Total(Ibold) - E_TorsionOld

            U_Total(Ibnew) =  U_LJ_Inter(Ibnew) + U_LJ_Intra(Ibnew) + U_EL_Real(Ibnew) +
     &                        U_EL_Intra(Ibnew) + U_EL_Excl(Ibnew) + U_EL_Four(Ibnew) +
     &                        U_Bending_Total(Ibnew) + U_Torsion_Total(Ibnew) + U_LJ_tail(Ibnew) + U_EL_Self(Ibnew)

            U_Total(Ibold) =  U_LJ_Inter(Ibold) + U_LJ_Intra(Ibold) + U_EL_Real(Ibold) +
     &                        U_EL_Intra(Ibold) + U_EL_Excl(Ibold) + U_EL_Four(Ibold) +
     &                        U_Bending_Total(Ibold) + U_Torsion_Total(Ibold) + U_LJ_tail(Ibold) + U_EL_Self(Ibold)

            IF(LEwald(Ibold)) CALL Ewald_Accept(Ibold)
            IF(LEwald(Ibnew)) CALL Ewald_Accept(Ibnew)

         ELSE

            DO I=1,N_MolInFrac(Ifrac)
               Ibox(Imol(I))=Ibold

               Tm = TypeMol(Imol(I))

               DO Iatom=1,N_AtomInMolType(Tm)
                  X(Imol(I),Iatom) = Xold(I,Iatom)
                  Y(Imol(I),Iatom) = Yold(I,Iatom)
                  Z(Imol(I),Iatom) = Zold(I,Iatom)
               END DO

               XCM(Imol(I)) = XCMold(I)
               YCM(Imol(I)) = YCMold(I)
               ZCM(Imol(I)) = ZCMold(I)

C     Undo the changes made in the lists earlier
               N_MolInBox(Ibold) = N_MolInBox(Ibold) + 1
               I_MolInBox(Ibold,N_MolInBox(Ibold)) = Imol(I)

               CALL Find_molecule_in_I_MolInBox(Imol(I),Ibnew,J)

               I_MolInBox(Ibnew,J) = I_MolInBox(Ibnew,N_MolInBox(Ibnew))
               N_MolInBox(Ibnew) = N_MolInBox(Ibnew) - 1

            END DO

            DO Ib=1,N_Box
               U_LJ_Tail(Ib) = E_LJ_TailOld(Ib)
               U_EL_Self(Ib) = E_EL_SelfOld(Ib)
            END DO

            Box_Frac(Ifrac) = Ibold

         END IF

      ELSEIF(Lf.GT.GEHybridChangeSwitch) THEN
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC                     IDENTITY CHANGE (CHANGE MOVE)                       CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

         DO Tm=1,N_MolType
            IF(Nmptpb(Ibnew,Tm).LT.N_MolOfMolTypeInFrac(Tm,Ifrac)) THEN
               RETURN              !Not enough whole molecules to change with
            END IF
         END DO

         TrialGEchange(Ibold,Ifrac) = TrialGEchange(Ibold,Ifrac) + 1.0d0
         TrialGEchangevslambda(Ibin,Ibold,Ifrac) = TrialGEchangevslambda(Ibin,Ibold,Ifrac) + 1.0d0

CC    Choose whole molecules (labeled Jmol) in the new box
         DO I=1,N_MolInFrac(Ifrac)
 33         CONTINUE
            Tm = TypeMol(Imol(I))
            Jmol(I)  =  Imptpb(Ibnew,Tm,Select_Random_Integer(Nmptpb(Ibnew,Tm)))

C     If a fractional type contains two or more more molecules of the same molecule type
C     it is possible that those two fractional molecules are assigned to the same whole
C     molecule in the new box, this should not happen: we check for that here.
            DO J=1,I-1
               IF(Jmol(I).EQ.Jmol(J)) GO TO 33
            END DO
         END DO

C     Store positions and charges for Ewald summation
         IF(LEwald(Ibold)) THEN
            DO I=1,N_MolInFrac(Ifrac)
               Kmol = Imol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               CALL interactionlambda(Kmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ibold,1) = NKSPACE(Ibold,1) + 1
                     J = NKSPACE(Ibold,1)
                     XKSPACE(J,Ibold,1) = X(Kmol,Iatom)
                     YKSPACE(J,Ibold,1) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ibold,1) = Z(Kmol,Iatom)
                     QKSPACE(J,Ibold,1) = Myc*Q(It)
                  END IF
               END DO
            END DO
         END IF

         IF(LEwald(Ibnew)) THEN
            DO I=1,N_MolInFrac(Ifrac)
               Kmol = Jmol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ibnew,1) = NKSPACE(Ibnew,1) + 1
                     J = NKSPACE(Ibnew,1)
                     XKSPACE(J,Ibnew,1) = X(Kmol,Iatom)
                     YKSPACE(J,Ibnew,1) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ibnew,1) = Z(Kmol,Iatom)
                     QKSPACE(J,Ibnew,1) = Q(It)
                  END IF
               END DO
            END DO
         END IF

CCC   ENERGY OF OLD CONFIGURATION
         DO Ib=1,N_Box
            E_LJ_TailOld(Ib) = U_LJ_Tail(Ib)
            E_EL_SelfOld(Ib) = U_EL_Self(Ib)
         END DO

         E_LJ_InterOld1 = 0.0d0
         E_LJ_IntraOld1 = 0.0d0
         E_EL_RealOld1  = 0.0d0
         E_EL_IntraOld1 = 0.0d0
         E_EL_ExclOld1  = 0.0d0
         E_BendingOld1  = 0.0d0
         E_TorsionOld1  = 0.0d0
         E_LJ_InterOld2 = 0.0d0
         E_LJ_IntraOld2 = 0.0d0
         E_EL_RealOld2  = 0.0d0
         E_EL_IntraOld2 = 0.0d0
         E_EL_ExclOld2  = 0.0d0
         E_BendingOld2  = 0.0d0
         E_TorsionOld2  = 0.0d0

         DO I=1,N_MolInFrac(Ifrac)
            CALL Energy_Molecule(Imol(I),E_LJ_Inter1,E_LJ_Intra1,E_EL_Real1,E_EL_Intra1,E_EL_Excl1,
     &                                   E_Bending1,E_Torsion1,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (GE Hybrid)"
               STOP
            END IF
            CALL Energy_Molecule(Jmol(I),E_LJ_Inter2,E_LJ_Intra2,E_EL_Real2,E_EL_Intra2,E_EL_Excl2,
     &                                   E_Bending2,E_Torsion2,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (GE Hybrid)"
               STOP
            END IF
            E_LJ_InterOld1 = E_LJ_InterOld1 + E_LJ_Inter1
            E_LJ_IntraOld1 = E_LJ_IntraOld1 + E_LJ_Intra1
            E_EL_RealOld1  = E_EL_RealOld1  + E_EL_Real1
            E_EL_IntraOld1 = E_EL_IntraOld1 + E_EL_Intra1
            E_EL_ExclOld1  = E_EL_ExclOld1  + E_EL_Excl1
            E_BendingOld1  = E_BendingOld1  + E_Bending1
            E_TorsionOld1  = E_TorsionOld1  + E_Torsion1
            E_LJ_InterOld2 = E_LJ_InterOld2 + E_LJ_Inter2
            E_LJ_IntraOld2 = E_LJ_IntraOld2 + E_LJ_Intra2
            E_EL_RealOld2  = E_EL_RealOld2  + E_EL_Real2
            E_EL_IntraOld2 = E_EL_IntraOld2 + E_EL_Intra2
            E_EL_ExclOld2  = E_EL_ExclOld2  + E_EL_Excl2
            E_BendingOld2  = E_BendingOld2  + E_Bending2
            E_TorsionOld2  = E_TorsionOld2  + E_Torsion2
         END DO

C     Correct for counting LJ and Electrostatic energy double
         DO I=1,N_MolInFrac(Ifrac)-1
            DO J=I+1,N_MolInFrac(Ifrac)

               CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter1,E_EL_Real1,L_Overlap_Inter)
               IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (GE Hybrid)"
                  STOP
               END IF
               CALL Energy_Intermolecular(Jmol(I),Jmol(J),E_LJ_Inter2,E_EL_Real2,L_Overlap_Inter)
               IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (GE Hybrid)"
                  STOP
               END IF

               E_LJ_InterOld1 = E_LJ_InterOld1 - E_LJ_Inter1
               E_LJ_InterOld2 = E_LJ_InterOld2 - E_LJ_Inter2
               E_EL_RealOld1  = E_EL_RealOld1  - E_EL_Real1
               E_EL_RealOld2  = E_EL_RealOld2  - E_EL_Real2
            END DO
         END DO

         Eold = E_LJ_InterOld1 + E_LJ_IntraOld1 + E_EL_RealOld1 + E_EL_IntraOld1 + E_EL_ExclOld1
     &        + E_TorsionOld1 + E_LJ_InterOld2 + E_LJ_IntraOld2 + E_EL_RealOld2 + E_EL_IntraOld2
     &        + E_EL_ExclOld2 + E_TorsionOld2 + E_LJ_TailOld(Ibold) + E_LJ_TailOld(Ibnew)
     &        + E_EL_SelfOld(Ibold) + E_EL_SelfOld(Ibnew) + E_BendingOld1 + E_BendingOld2

CCC   Update lists and labels
         DO I=1,N_MolInFrac(Ifrac)
            L_Frac(Imol(I)) = .false.
            L_Frac(Jmol(I)) = .true.

C     Imol(I) is now no longer a fractional in stead Jmol is
            I_MolInFrac(Ifrac,I) = Jmol(I)

            Tm = TypeMol(Imol(I))

C     Update lists for changing Imol(I) to a whole and Jmol(I) to a fractional
C     Imol(I) is added to the list of whole molecules in box Ibold
            Nmptpb(Ibold,Tm) = Nmptpb(Ibold,Tm) + 1
            Imptpb(Ibold,Tm,Nmptpb(Ibold,Tm)) = Imol(I)

            CALL Find_molecule_in_Imptpb(Jmol(I),Ibnew,Tm,J)

C     Jmol(I) is removed from the list with whole molecules in box Ibnew by replacing
C     it with the last label in the list.
            Imptpb(Ibnew,Tm,J) = Imptpb(Ibnew,Tm,Nmptpb(Ibnew,Tm))
            Nmptpb(Ibnew,Tm)   = Nmptpb(Ibnew,Tm) - 1

         END DO

         Box_Frac(Ifrac) = Ibnew

C     Store positions and charges for Ewald summation
         IF(LEwald(Ibold)) THEN
            DO I=1,N_MolInFrac(Ifrac)
               Kmol = Imol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ibold,2) = NKSPACE(Ibold,2) + 1
                     J = NKSPACE(Ibold,2)
                     XKSPACE(J,Ibold,2) = X(Kmol,Iatom)
                     YKSPACE(J,Ibold,2) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ibold,2) = Z(Kmol,Iatom)
                     QKSPACE(J,Ibold,2) = Q(It)
                  END IF
               END DO
            END DO
         END IF

         IF(LEwald(Ibnew)) THEN
            DO I=1,N_MolInFrac(Ifrac)
               Kmol = Jmol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               CALL interactionlambda(Kmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ibnew,2) = NKSPACE(Ibnew,2) + 1
                     J = NKSPACE(Ibnew,2)
                     XKSPACE(J,Ibnew,2) = X(Kmol,Iatom)
                     YKSPACE(J,Ibnew,2) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ibnew,2) = Z(Kmol,Iatom)
                     QKSPACE(J,Ibnew,2) = Myc*Q(It)
                  END IF
               END DO
            END DO
         END IF

CCC   ENERGY OF NEW CONFIGURATION
         E_LJ_InterNew1 = 0.0d0
         E_LJ_IntraNew1 = 0.0d0
         E_EL_RealNew1  = 0.0d0
         E_EL_IntraNew1 = 0.0d0
         E_EL_ExclNew1  = 0.0d0
         E_BendingNew1  = 0.0d0
         E_TorsionNew1  = 0.0d0
         E_LJ_InterNew2 = 0.0d0
         E_LJ_IntraNew2 = 0.0d0
         E_EL_RealNew2  = 0.0d0
         E_EL_IntraNew2 = 0.0d0
         E_EL_ExclNew2  = 0.0d0
         E_BendingNew2  = 0.0d0
         E_TorsionNew2  = 0.0d0

         DO I=1,N_MolInFrac(Ifrac)
            CALL Energy_Molecule(Imol(I),E_LJ_Inter1,E_LJ_Intra1,E_EL_Real1,E_EL_Intra1,E_EL_Excl1,
     &                                   E_Bending1,E_Torsion1,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (GE Hybrid)"
               STOP
            ELSEIF(L_Overlap_Inter) THEN
               Laccept=.false.
               GO TO 2
            END IF
            CALL Energy_Molecule(Jmol(I),E_LJ_Inter2,E_LJ_Intra2,E_EL_Real2,E_EL_Intra2,E_EL_Excl2,
     &                                   E_Bending2,E_Torsion2,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (GE Hybrid)"
               STOP
            ELSEIF(L_Overlap_Inter) THEN
               Laccept=.false.
               GO TO 2
            END IF
            E_LJ_InterNew1 = E_LJ_InterNew1 + E_LJ_Inter1
            E_LJ_IntraNew1 = E_LJ_IntraNew1 + E_LJ_Intra1
            E_EL_RealNew1  = E_EL_RealNew1  + E_EL_Real1
            E_EL_IntraNew1 = E_EL_IntraNew1 + E_EL_Intra1
            E_EL_ExclNew1  = E_EL_ExclNew1  + E_EL_Excl1
            E_BendingNew1  = E_BendingNew1  + E_Bending1
            E_TorsionNew1  = E_TorsionNew1  + E_Torsion1
            E_LJ_InterNew2 = E_LJ_InterNew2 + E_LJ_Inter2
            E_LJ_IntraNew2 = E_LJ_IntraNew2 + E_LJ_Intra2
            E_EL_RealNew2  = E_EL_RealNew2  + E_EL_Real2
            E_EL_IntraNew2 = E_EL_IntraNew2 + E_EL_Intra2
            E_EL_ExclNew2  = E_EL_ExclNew2  + E_EL_Excl2
            E_BendingNew2  = E_BendingNew2  + E_Bending2
            E_TorsionNew2  = E_TorsionNew2  + E_Torsion2
         END DO

C     Correct for counting LJ and Electrostatic energy double
         DO I=1,N_MolInFrac(Ifrac)-1
            DO J=I+1,N_MolInFrac(Ifrac)

               CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter1,E_EL_Real1,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (GE Hybrid)"
                  STOP
               END IF
               CALL Energy_Intermolecular(Jmol(I),Jmol(J),E_LJ_Inter2,E_EL_Real2,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (GE Hybrid)"
                  STOP
               END IF

               E_LJ_InterNew1 = E_LJ_InterNew1 - E_LJ_Inter1
               E_LJ_InterNew2 = E_LJ_InterNew2 - E_LJ_Inter2
               E_EL_RealNew1  = E_EL_RealNew1  - E_EL_Real1
               E_EL_RealNew2  = E_EL_RealNew2  - E_EL_Real2
            END DO
         END DO

         dE_EL_FourNew = 0.0d0
         dE_EL_FourOld = 0.0d0
         IF(LEwald(Ibold)) CALL Ewald_Move(Ibold,dE_EL_FourOld)
         IF(LEwald(Ibnew)) CALL Ewald_Move(Ibnew,dE_EL_FourNew)

         CALL Energy_Correction(Ibnew)
         CALL Energy_Correction(Ibold)

         DO Ib=1,N_Box
            E_LJ_TailNew(Ib) = U_LJ_Tail(Ib)
            E_EL_SelfNew(Ib) = U_EL_Self(Ib)
         END DO

         Enew = E_LJ_InterNew1 + E_LJ_IntraNew1 + E_EL_RealNew1 + E_EL_IntraNew1 + E_EL_ExclNew1
     &        + E_TorsionNew1 + E_LJ_InterNew2 + E_LJ_IntraNew2 + E_EL_RealNew2 + E_EL_IntraNew2
     &        + E_EL_ExclNew2 + E_TorsionNew2 + E_LJ_TailNew(Ibold) + E_LJ_TailNew(Ibnew)
     &        + E_EL_SelfNew(Ibold) + E_EL_SelfNew(Ibnew) + E_BendingNew1 + E_BendingNew2

C     Calculate the energy difference
         dE = Enew - Eold + dE_EL_FourNew + dE_EL_FourOld
         dW = Weight(Ibin,Ibnew,Rs,Ifrac) - Weight(Ibin,Ibold,Rs,Ifrac)

         factor = Factorial_Ratio(Nmptpb(Ibold,Tm)-N_MolInFrac(Ifrac),Nmptpb(Ibold,Tm))*
     &            Factorial_Ratio(Nmptpb(Ibnew,Tm)+N_MolInFrac(Ifrac),Nmptpb(Ibnew,Tm))

         CALL Accept_or_Reject(factor*dexp(-beta*dE+dW),Laccept)

   2     CONTINUE

         IF(Laccept) THEN
            AcceptGEchange(Ibold,Ifrac) = AcceptGEchange(Ibold,Ifrac) + 1.0d0
            AcceptGEchangevslambda(Ibin,Ibold,Ifrac) = AcceptGEchangevslambda(Ibin,Ibold,Ifrac) + 1.0d0

            U_LJ_Inter(Ibnew) = U_LJ_Inter(Ibnew) + E_LJ_InterNew2 - E_LJ_InterOld2
            U_LJ_Inter(Ibold) = U_LJ_Inter(Ibold) + E_LJ_InterNew1 - E_LJ_InterOld1

            U_LJ_Intra(Ibnew) = U_LJ_Intra(Ibnew) + E_LJ_IntraNew2 - E_LJ_IntraOld2
            U_LJ_Intra(Ibold) = U_LJ_Intra(Ibold) + E_LJ_IntraNew1 - E_LJ_IntraOld1

            U_EL_Real(Ibnew) = U_EL_Real(Ibnew) + E_EL_RealNew2 - E_EL_RealOld2
            U_EL_Real(Ibold) = U_EL_Real(Ibold) + E_EL_RealNew1 - E_EL_RealOld1

            U_EL_Intra(Ibnew) = U_EL_Intra(Ibnew) + E_EL_IntraNew2 - E_EL_IntraOld2
            U_EL_Intra(Ibold) = U_EL_Intra(Ibold) + E_EL_IntraNew1 - E_EL_IntraOld1

            U_EL_Excl(Ibnew) = U_EL_Excl(Ibnew) + E_EL_ExclNew2 - E_EL_ExclOld2
            U_EL_Excl(Ibold) = U_EL_Excl(Ibold) + E_EL_ExclNew1 - E_EL_ExclOld1

            U_EL_Four(Ibnew) = U_EL_Four(Ibnew) + dE_EL_FourNew
            U_EL_Four(Ibold) = U_EL_Four(Ibold) + dE_EL_FourOld

            U_Bending_Total(Ibnew) = U_Bending_Total(Ibnew) + E_BendingNew2 - E_BendingOld2
            U_Bending_Total(Ibold) = U_Bending_Total(Ibold) + E_BendingNew1 - E_BendingOld1

            U_Torsion_Total(Ibnew) = U_Torsion_Total(Ibnew) + E_TorsionNew2 - E_TorsionOld2
            U_Torsion_Total(Ibold) = U_Torsion_Total(Ibold) + E_TorsionNew1 - E_TorsionOld1

            U_Total(Ibnew) =  U_LJ_Inter(Ibnew) + U_LJ_Intra(Ibnew) + U_EL_Real(Ibnew) +
     &                        U_EL_Intra(Ibnew) + U_EL_Excl(Ibnew) + U_EL_Four(Ibnew) +
     &                        U_Bending_Total(Ibnew) + U_Torsion_Total(Ibnew) + U_LJ_tail(Ibnew) + U_EL_Self(Ibnew)

            U_Total(Ibold) =  U_LJ_Inter(Ibold) + U_LJ_Intra(Ibold) + U_EL_Real(Ibold) +
     &                        U_EL_Intra(Ibold) + U_EL_Excl(Ibold) + U_EL_Four(Ibold) +
     &                        U_Bending_Total(Ibold) + U_Torsion_Total(Ibold) + U_LJ_tail(Ibold) + U_EL_Self(Ibold)

            IF(LEwald(Ibold)) CALL Ewald_Accept(Ibold)
            IF(LEwald(Ibnew)) CALL Ewald_Accept(Ibnew)

         ELSE

            DO I=1,N_MolInFrac(Ifrac)

               Tm = TypeMol(Imol(I))

               L_Frac(Imol(I)) = .true.
               L_Frac(Jmol(I)) = .false.

               I_MolInFrac(Ifrac,I) = Imol(I)

C     Undo the changes in the lists made earlier
               Nmptpb(Ibnew,Tm) = Nmptpb(Ibnew,Tm) + 1
               Imptpb(Ibnew,Tm,Nmptpb(Ibnew,Tm)) = Jmol(I)

               CALL Find_molecule_in_Imptpb(Imol(I),Ibold,Tm,J)

               Imptpb(Ibold,Tm,J) = Imptpb(Ibold,Tm,Nmptpb(Ibold,Tm))
               Nmptpb(Ibold,Tm)   = Nmptpb(Ibold,Tm) - 1

            END DO

            DO Ib=1,N_Box
               U_LJ_Tail(Ib) = E_LJ_TailOld(Ib)
               U_EL_Self(Ib) = E_EL_SelfOld(Ib)
            END DO

            Box_Frac(Ifrac) = Ibold

         END IF

      END IF

      RETURN
      END
