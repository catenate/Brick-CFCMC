      SUBROUTINE Write_Configuration(Ichoice)
      implicit none

      include "global_variables.inc"

      integer  Ichoice,Nparttot,Ib,Tm,Ifrac,I,Imol,unit(MaxMolType),Nmoloftype(MaxMolType),
     &         N_AtomInMolTypePrint(MaxMolType),Iff
      double precision boxshift(2)
      logical  Lclose
      character*100 conffile(MaxMolType)

      save  unit,Iff,N_AtomInMolTypePrint
      data  Iff /0/

      Lclose=.false.

      IF(Iff.EQ.0) THEN
         CALL system('mkdir OUTPUT/CONFIGURATIONS')
         DO Tm = 1,N_MolType
            unit(Tm)=100+Tm
            N_AtomInMolTypePrint(Tm) = 0
            DO I = 1,N_AtomInMolType(Tm)
               IF(L_PrintAtom(TypeAtom(Tm,I))) N_AtomInMolTypePrint(Tm) = N_AtomInMolTypePrint(Tm) + 1
            END DO
         END DO
         Iff=1
      END IF

      IF(Ichoice.EQ.0) THEN
         OPEN(100,file="./OUTPUT/CONFIGURATIONS/ALL_initial.xyz")
         DO Tm=1,N_MolType
            WRITE(conffile(Tm),'(A,A,A)') "./OUTPUT/CONFIGURATIONS/", TRIM(C_MolType(Tm)), "_initial.xyz"
            OPEN(unit=unit(Tm),file=conffile(Tm))
         END DO
         Lclose=.true.
      ELSEIF(Ichoice.EQ.1) THEN
         OPEN(100,file="./OUTPUT/CONFIGURATIONS/ALL.xyz")
         DO Tm=1,N_MolType
            WRITE(conffile(Tm),'(A,A,A)') "./OUTPUT/CONFIGURATIONS/", TRIM(C_MolType(Tm)), ".xyz"
            OPEN(unit=unit(Tm),file=conffile(Tm))
         END DO
      ELSEIF(Ichoice.EQ.2) THEN
         OPEN(100,file="./OUTPUT/CONFIGURATIONS/ALL_final.xyz")
         DO Tm=1,N_MolType
            WRITE(conffile(Tm),'(A,A,A)') "./OUTPUT/CONFIGURATIONS/", TRIM(C_MolType(Tm)), "_final.xyz"
            OPEN(unit=unit(Tm),file=conffile(Tm))
         END DO
         Lclose=.true.
      ELSEIF(Ichoice.EQ.3) THEN
         OPEN(100,file="./OUTPUT/CONFIGURATIONS/ALL_debug.xyz")
         DO Tm=1,N_MolType
            WRITE(conffile(Tm),'(A,A,A)') "./OUTPUT/CONFIGURATIONS/", TRIM(C_MolType(Tm)), "_debug.xyz"
            OPEN(unit=unit(Tm),file=conffile(Tm))
         END DO
      ELSEIF(Ichoice.EQ.4) THEN
         Lclose = .true.
         GO TO 123
      END IF

      Nparttot = 0
      DO Ib=1,N_Box
         DO Tm=1,N_MolType
            Nparttot = Nparttot + Nmptpb(Ib,Tm)*N_AtomInMolTypePrint(Tm)
         END DO
      END DO

      DO Ifrac=1,N_Frac
         DO I=1,N_MolInFrac(Ifrac)
            Nparttot = Nparttot + N_AtomInMolTypePrint(TypeMol(I_MolInFrac(Ifrac,I)))
         END DO
      END DO

      WRITE(100,'(i10)') Nparttot
      WRITE(100,'(2f20.5)') (BoxSize(Ib), Ib=1,N_Box)
      Boxshift(1) = 0.0d0
      Boxshift(2) = 1.5d0*BoxSize(1)
      DO Imol=1,N_MolTotal
         Tm = TypeMol(Imol)
         Ib = Ibox(Imol)
         DO I = 1,N_AtomInMolType(Tm)
            IF(L_PrintAtom(TypeAtom(Tm,I))) THEN
               IF(L_Frac(Imol)) THEN
                  WRITE(100,'(a10,3(2x,f20.5),A)')
     &              C_AtomPrint(TypeAtom(Tm,I)), X(Imol,I)+Boxshift(Ib), Y(Imol,I), Z(Imol,I), " FRACTIONAL"
               ELSE
                  WRITE(100,'(a10,3(2x,f20.5))')
     &              C_AtomPrint(TypeAtom(Tm,I)), X(Imol,I)+Boxshift(Ib), Y(Imol,I), Z(Imol,I)
               END IF
            END IF
         END DO
      END DO

      DO Tm=1,N_MolType
         Nmoloftype(Tm) = 0
         DO Ib=1,N_Box
            Nmoloftype(Tm)= Nmoloftype(Tm) + Nmptpb(Ib,Tm)*N_AtomInMolType(Tm)
         END DO
      END DO

      DO Ifrac=1,N_Frac
         DO I=1,N_MolInFrac(Ifrac)
            Tm = TypeMol(I_MolInFrac(Ifrac,I))
            Nmoloftype(Tm) = Nmoloftype(Tm) + N_AtomInMolType(Tm)
         END DO
      END DO

      DO Tm=1,N_MolType
         WRITE(unit(Tm),'(i10)') Nmoloftype(Tm)
         WRITE(unit(Tm),'(2f20.5)') (BoxSize(Ib), Ib=1,N_Box)
      END DO

      Boxshift(1) = 0.0d0
      Boxshift(2) = 1.5d0*BoxSize(1)

      DO Imol=1,N_MolTotal
         Tm = TypeMol(Imol)
         Ib = Ibox(Imol)
         DO I = 1,N_AtomInMolType(Tm)
            IF(L_PrintAtom(TypeAtom(Tm,I))) THEN
               IF(L_Frac(Imol)) THEN
                  WRITE(unit(Tm),'(a10,3(2x,f20.5),A)')
     &              C_AtomPrint(TypeAtom(Tm,I)), X(Imol,I)+Boxshift(Ib), Y(Imol,I), Z(Imol,I), " FRACTIONAL"
               ELSE
                  WRITE(unit(Tm),'(a10,3(2x,f20.5))')
     &              C_AtomPrint(TypeAtom(Tm,I)), X(Imol,I)+Boxshift(Ib), Y(Imol,I), Z(Imol,I)
               END IF
            END IF
         END DO
      END DO

 123  CONTINUE

      IF(Lclose) THEN
         CLOSE(100)
         DO Tm=1,N_MolType
            CLOSE(unit(Tm))
         END DO
      END IF

      RETURN
      END
