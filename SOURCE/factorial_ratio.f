      FUNCTION Factorial_Ratio(I,J)
      implicit none

C     Calculate the ratio of I! and J!

      include "global_variables.inc"
      include "output.inc"      

      Integer  I,J,K
      Double Precision Factorial_Ratio      

      Factorial_Ratio = 1.0d0

      IF(I.LT.0.OR.J.LT.0) THEN
         WRITE(6,'(A,A,i4,i4)') ERROR, "Something went wrong calculating I!/J! of", I, J
         STOP
      END IF

      IF(I.GT.J) THEN
         DO K=(J+1),I
            Factorial_Ratio = Factorial_Ratio*dble(K)
         END DO
      ELSEIF(J.GT.I) THEN
         DO K=(I+1),J
            Factorial_Ratio = Factorial_Ratio/dble(K)
         END DO
      END IF

      RETURN
      END
