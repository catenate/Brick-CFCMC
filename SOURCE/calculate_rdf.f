      SUBROUTINE Calculate_RDF(Ichoice)
      implicit none

      include "global_variables.inc"
      include "settings.inc"
      include "output.inc"

C     Samples The Radial Distribution Function

      Integer Ichoice,Ib,Tmi,Tmj,I,J,K,Imol,Jmol,Maxbin,It,Jt,Iatom,Jatom,Tm

      Double Precision Ggt,RDF_molecule(2,MaxMolType,MaxMolType,MaxRDFbins),Delta,Idelta,
     &   R(MaxRDFbins),R2,dX,dY,dZ,Maxdist(2),pi4,RDF_atom(2,MaxAtomType,MaxAtomType,MaxRDFbins),
     &   Pairs(2,max(MaxMolType,MaxAtomType),max(MaxMolType,MaxAtomType),MaxRDFbins),
     &   AvNapb(2,MaxAtomType),AvVolume_RDF(2),AvNmptpb_RDF(2,MaxMolType)
      logical L_RDFAtomType(MaxAtomType)
      character*100 rdffile

      Save Ggt,RDF_molecule,RDF_atom,Maxdist,Delta,Idelta,Maxbin,AvVolume_RDF,AvNmptpb_RDF,L_RDFAtomType

C     MaxRDFbins total number of bins
      IF(Ichoice.EQ.1) THEN
         Ggt    = 0.0d0
         Delta  = 0.01d0
         Idelta = 1.0d0/Delta

         Maxdist(1) = 0.5d0*BoxSize(1)
         Maxbin     = int(Maxdist(1)*Idelta)

         IF(N_Box.EQ.2) THEN
            Maxdist(2) = 0.5d0*BoxSize(2)
            Maxbin     = int(Max(Maxdist(1),Maxdist(2))*Idelta)
         END IF

         IF(Maxbin.GT.MaxRDFbins) THEN
            WRITE(6,'(A,A)') ERROR,
     &  "Increase the Maximum number of bins for calculating the RDF in in maximum_dimensions.inc and recompile"
            STOP
         END IF

         DO Ib=1,N_Box
            DO Tmi=1,N_MolType
               DO Tmj=1,N_MolType
                  DO I=1,Maxbin
                     RDF_molecule(Ib,Tmi,Tmj,I) = 0.0d0
                  END DO
               END DO
            AvNmptpb_RDF(Ib,Tmi) = 0.0d0
            END DO
            AvVolume_RDF(Ib) = 0.0d0
            DO It=1,N_AtomType
               DO Jt=1,N_AtomType
                  DO I=1,Maxbin
                     RDF_atom(Ib,It,Jt,I) = 0.0d0
                  END DO
               END DO
               AvNapb(Ib,It) = 0.0d0
            END DO
         END DO

         DO It=1,N_AtomType
            L_RDFAtomType(It) = .false.
         END DO

         DO Tm=1,N_MolType
            DO Iatom=1,N_AtomInMolType(Tm)
               It = TypeAtom(Tm,Iatom)
               L_RDFAtomType(It) = .true.
            END DO
         END DO

         CALL system('mkdir OUTPUT/RDF')

      ELSEIF(Ichoice.EQ.2) THEN

         Ggt = Ggt + 1.0d0

         DO Ib=1,N_Box
            DO Tm=1,N_MolType
               AvNmptpb_RDF(Ib,Tm) = AvNmptpb_RDF(Ib,Tm) + Nmptpb(Ib,Tm)
            END DO
            AvVolume_RDF(Ib) = AvVolume_RDF(Ib) + Volume(Ib)
         END DO

C     Loop Over All Pairs
         DO Ib=1,N_Box

            DO I=1,N_MolInBox(Ib)-1

               Imol = I_MolInBox(Ib,I)
               IF(L_Frac(Imol)) CYCLE

               DO J=I+1,N_MolInBox(Ib)

                  Jmol = I_MolInBox(Ib,J)
                  IF(L_Frac(Jmol)) CYCLE

                  Tmi = TypeMol(Imol)
                  Tmj = TypeMol(Jmol)

                  dX = XCM(Imol) - XCM(Jmol)
                  dY = YCM(Imol) - YCM(Jmol)
                  dZ = ZCM(Imol) - ZCM(Jmol)

                  dX = dX - BoxSize(Ib)*dnint(dX*InvBoxSize(Ib))
                  dY = dY - BoxSize(Ib)*dnint(dY*InvBoxSize(Ib))
                  dZ = dZ - BoxSize(Ib)*dnint(dZ*InvBoxSize(Ib))

                  R2 = dsqrt(dX*dX + dY*dY + dZ*dZ)

                  IF(R2.LT.Maxdist(Ib)) THEN
                     K = 1 + int(R2*Idelta)
                     RDF_molecule(Ib,Tmi,Tmj,K) = RDF_molecule(Ib,Tmi,Tmj,K) + 1.0d0
                  END IF

                  IF(L_RDFAtom) THEN
                     DO Iatom=1,N_AtomInMolType(Tmi)
                        DO Jatom=1,N_AtomInMolType(Tmj)

                           dX = X(Imol,Iatom) - X(Jmol,Jatom)
                           dY = Y(Imol,Iatom) - Y(Jmol,Jatom)
                           dZ = Z(Imol,Iatom) - Z(Jmol,Jatom)

                           dX = dX - BoxSize(Ib)*dnint(dX*InvBoxSize(Ib))
                           dY = dY - BoxSize(Ib)*dnint(dY*InvBoxSize(Ib))
                           dZ = dZ - BoxSize(Ib)*dnint(dZ*InvBoxSize(Ib))

                           R2 = dsqrt(dX*dX + dY*dY + dZ*dZ)

                           IF(R2.LT.Maxdist(Ib)) THEN
                              It = TypeAtom(Tmi,Iatom)
                              Jt = TypeAtom(Tmj,Jatom)
                              K  = 1 + int(R2*Idelta)
                              RDF_atom(Ib,It,Jt,K) = RDF_atom(Ib,It,Jt,K) + 1.0d0
                           END IF

                        END DO
                     END DO
                  END IF

               END DO
            END DO
         END DO

C     Write RDF to file
      ELSEIF(Ichoice.EQ.3) THEN

         DO Ib=1,N_Box
            DO Tm=1,N_MolType
               AvNmptpb_RDF(Ib,Tm) = AvNmptpb_RDF(Ib,Tm)/Ggt
            END DO
            AvVolume_RDF(Ib) = AvVolume_RDF(Ib)/Ggt
         END DO

         pi4 = 2.0d0*TwoPi/3.0d0

CCC   RDF for molecules
         DO I=1,Maxbin-1
            R(I) = (dble(I)-0.5d0)*Delta
            DO Ib=1,N_Box
               DO Tmi=1,N_MolType
                  DO Tmj=1,N_MolType
                     Pairs(Ib,Tmi,Tmj,I) = Ggt*pi4*Delta*Delta*Delta*
     &                   (dble(I)*dble(I)*dble(I) - dble(I-1)*dble(I-1)*dble(I-1))*
     &                   AvNmptpb_RDF(Ib,Tmi)*AvNmptpb_RDF(Ib,Tmj)/AvVolume_RDF(Ib)
                     IF(Tmi.EQ.Tmj) Pairs(Ib,Tmi,Tmj,I) = 0.5d0*Pairs(Ib,Tmi,Tmj,I)
                  END DO
               END DO
            END DO
         END DO


         DO Tmi=1,N_MolType
            DO Tmj=Tmi,N_MolType
               WRITE(rdffile,'(A31,A,A1,A,A4)')
     &  "./OUTPUT/RDF/molecule-molecule_", TRIM(C_MolType(Tmi)), "-", TRIM(C_MolType(Tmj)), ".dat"

               OPEN(30,file=rdffile)
               DO I=1,Maxbin-1
                  WRITE(30,'(3(e20.10e3,1x))') R(I), (RDF_molecule(Ib,Tmi,Tmj,I)/pairs(Ib,Tmi,Tmj,I),Ib=1,N_Box)
               END DO
               CLOSE(30)
            END DO
         END DO

CCC   RDF for atoms
         IF(L_RDFAtom) THEN
            DO Tm=1,N_MolType
               DO I=1,N_AtomInMolType(Tm)
                  It = TypeAtom(Tm,I)
                  DO Ib=1,N_Box
                     AvNapb(Ib,It) = AvNapb(Ib,It) + AvNmptpb_RDF(Ib,Tm)
                  END DO
               END DO
            END DO

            DO I=1,Maxbin-1
               R(I) = (dble(I)-0.5d0)*Delta
               DO Ib=1,N_Box
                  DO It=1,N_AtomType
                     DO Jt=1,N_AtomType
                        Pairs(Ib,It,Jt,I) = Ggt*pi4*Delta*Delta*Delta*
     &                      (dble(I)*dble(I)*dble(I) - dble(I-1)*dble(I-1)*dble(I-1))*
     &                       AvNapb(Ib,It)*AvNapb(Ib,Jt)/AvVolume_RDF(Ib)
                        IF(It.EQ.Jt) Pairs(Ib,It,Jt,I) = 0.5d0*Pairs(Ib,It,Jt,I)
                     END DO
                  END DO
               END DO
            END DO

            DO It=1,N_AtomType
               IF(L_RDFAtomType(It)) THEN
                  DO Jt=It,N_AtomType
                     IF(L_RDFAtomType(Jt)) THEN
                        WRITE(rdffile,'(A23,A,A1,A,A4)')
     &     "./OUTPUT/RDF/atom-atom_", TRIM(C_AtomType(It)), "-", TRIM(C_AtomType(Jt)), ".dat"

                        OPEN(30,file=rdffile)
                        DO I=1,Maxbin-1
                           WRITE(30,'(3(e20.10e3,1x))') R(I), (RDF_atom(Ib,It,Jt,I)/Pairs(Ib,It,Jt,I),Ib=1,N_Box)
                        END DO
                        CLOSE(30)
                     END IF
                  END DO
               END IF
            END DO
         END IF

      END IF

      Return
      END
