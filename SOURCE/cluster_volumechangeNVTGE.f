      SUBROUTINE Cluster_VolumeChangeNVTGE
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "output.inc"

      integer Imol,Ib,I,Iatom,Tm,Icluster,Nclusterold,Nclusternew,Npart(2),
     &   Nmolecincluster(MaxCluster),Imolecincluster(MaxCluster,MaxMolInCluster)
      double precision Boxold(2),Volold(2),Volnew(2),Df(2),Ran_Uniform,
     &   E_LJ_InterNew(2),E_LJ_IntraNew(2),E_EL_RealNew(2),E_EL_IntraNew(2),
     &   E_TorsionNew(2),Eold(2),Enew(2),dX,dY,dZ,dE(2),XCMold(MaxMol),
     &   YCMold(MaxMol),ZCMold(MaxMol),Xold(MaxMol,MaxAtom),E_EL_FourNew(2),
     &   Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),E_LJ_TailOld(2),
     &   E_EL_SelfOld(2),E_EL_ExclNew(2),E_LJ_TailNew(2),E_EL_SelfNew(2),
     &   zetax,zetay,zetaz,cmxold,cmyold,cmzold,cmxnew,cmynew,cmznew,
     &   xix,xiy,xiz,E_BendingNew(2)
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept

C     Volume change in the Gibbs NVT ensemble with clusters
      DO Ib=1,N_Box
         Boxold(Ib) = BoxSize(Ib)
         Volold(Ib) = BoxSize(Ib)*BoxSize(Ib)*BoxSize(Ib)
      END DO

      IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)      

      Volnew(1) = Volold(1) + (2.0d0*Ran_Uniform()-1.0d0)*Delta_ClusterVolume(1)
      IF(Volnew(1).LE.0.0d0) RETURN

      Volnew(2) = Volold(1) + Volold(2) - Volnew(1)
      IF(Volnew(2).LE.0.0d0) RETURN

      DO Ib=1,N_Box
         E_LJ_TailOld(Ib) = U_LJ_Tail(Ib)
         E_EL_SelfOld(Ib) = U_EL_Self(Ib)

         Eold(Ib) = U_Total(Ib)

         Df(Ib)         = (Volnew(Ib)/Volold(Ib))**(1.0d0/3.0d0)
         BoxSize(Ib)    = Boxold(Ib)*Df(Ib)
         InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)
      END DO

      DO Ib=1,N_Box
         IF(.NOT.L_IdealGas(Ib)) THEN
            IF(dsqrt(R_Cut_Max_2(Ib)).GT.0.5d0*BoxSize(Ib)) THEN
               WRITE(6,'(A,A,i1)') ERROR, "Volume; Rcut is too large in box ", Ib
               STOP
            END IF
         END IF
      END DO

C     Save old configuration
      DO Imol=1,N_MolTotal
         Tm = TypeMol(Imol)
         Ib = Ibox(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Imol,Iatom) = X(Imol,Iatom)
            Yold(Imol,Iatom) = Y(Imol,Iatom)
            Zold(Imol,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(Imol) = XCM(Imol)
         YCMold(Imol) = YCM(Imol)
         ZCMold(Imol) = ZCM(Imol)

      END DO

      DO Ib=1,N_Box

         IF(Ib.EQ.Ibvapor) THEN
C     Calculate the center of mass of the cluster and scale coordinates accordingly
            CALL Find_Clusters(Nclusterold,Nmolecincluster,Imolecincluster)

            DO Icluster=1,Nclusterold
               xix = 0.0d0
               xiy = 0.0d0
               xiz = 0.0d0

               zetax = 0.0d0
               zetay = 0.0d0
               zetaz = 0.0d0

               DO I=1,Nmolecincluster(Icluster)
                  Imol = Imolecincluster(Icluster,I)
                  Tm   = TypeMol(Imol)

                  xix = xix + dcos(XCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
                  xiy = xiy + dcos(YCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
                  xiz = xiz + dcos(ZCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)

                  zetax = zetax + dsin(XCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
                  zetay = zetay + dsin(YCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
                  zetaz = zetaz + dsin(ZCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
               END DO

               cmxold = (datan2(-zetax,-xix) + OnePi)*BoxSize(Ib)/TwoPi
               cmyold = (datan2(-zetay,-xiy) + OnePi)*BoxSize(Ib)/TwoPi
               cmzold = (datan2(-zetaz,-xiz) + OnePi)*BoxSize(Ib)/TwoPi

               cmxnew = cmxold*Df(Ib)
               cmynew = cmyold*Df(Ib)
               cmznew = cmzold*Df(Ib)

               dX = cmxnew - cmxold
               dY = cmynew - cmyold
               dZ = cmznew - cmzold

               DO I=1,Nmolecincluster(Icluster)
                  Imol = Imolecincluster(Icluster,I)
                  Tm = TypeMol(Imol)

                  DO Iatom=1,N_AtomInMolType(Tm)
                     X(Imol,Iatom) = X(Imol,Iatom) + dX
                     Y(Imol,Iatom) = Y(Imol,Iatom) + dY
                     Z(Imol,Iatom) = Z(Imol,Iatom) + dZ
                  END DO

                  XCM(Imol) = XCM(Imol) + dX
                  YCM(Imol) = YCM(Imol) + dY
                  ZCM(Imol) = ZCM(Imol) + dZ
               END DO

            END DO

C     Check if clusters changed
            CALL Find_Clusters(Nclusternew,Nmolecincluster,Imolecincluster)

C     If clusters changed than reject the move (because this would violate detailed balance)
            IF(Nclusternew.NE.Nclusterold) THEN
               Laccept=.false.
               GO TO 1
            END IF

            Npart(Ib) = Nclusterold

         ELSE
C     Conventional scaling of all particles in the box
            DO I=1,N_MolInBox(Ib)
               Imol = I_MolInBox(Ib,I)
               Tm   = TypeMol(Imol)

               XCM(Imol) = XCM(Imol)*Df(Ib)
               YCM(Imol) = YCM(Imol)*Df(Ib)
               ZCM(Imol) = ZCM(Imol)*Df(Ib)

               dX = XCM(Imol) - XCMold(Imol)
               dY = YCM(Imol) - YCMold(Imol)
               dZ = ZCM(Imol) - ZCMold(Imol)

               DO Iatom=1,N_AtomInMolType(Tm)
                  X(Imol,Iatom) = XCM(Imol) + dX
                  Y(Imol,Iatom) = YCM(Imol) + dY
                  Z(Imol,Iatom) = ZCM(Imol) + dZ
               END DO

            END DO

            Npart(Ib) = N_MolInBox(Ib)

         END IF

      END DO

      DO Ib=1,N_Box
         CALL Energy_Total(Ib,E_LJ_InterNew(Ib),E_LJ_IntraNew(Ib),E_EL_RealNew(Ib),E_EL_IntraNew(Ib),
     &               E_EL_ExclNew(Ib),E_BendingNew(Ib),E_TorsionNew(Ib),L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Cluster Volume Change NVTGE)"
            STOP
         ELSEIF(L_Overlap_Inter) THEN
            Laccept=.false.
            GO TO 1
         END IF

         E_EL_FourNew(Ib) = 0.0d0
         IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew(Ib))

         CALL Energy_Correction(Ib)

         E_LJ_TailNew(Ib) = U_LJ_Tail(Ib)
         E_EL_SelfNew(Ib) = U_EL_Self(Ib)

         Enew(Ib) = E_LJ_InterNew(Ib) + E_LJ_IntraNew(Ib) + E_EL_RealNew(Ib) + E_EL_IntraNew(Ib)
     &            + E_BendingNew(Ib) + E_EL_ExclNew(Ib) + E_TorsionNew(Ib) + E_LJ_TailNew(Ib) + E_EL_SelfNew(Ib) + E_EL_FourNew(Ib)

         dE(Ib) = Enew(Ib) - Eold(Ib)
      END DO

      CALL Accept_or_Reject(dexp(-beta*(dE(1) + dE(2)) +
     &     dble(Npart(1))*dlog(Volnew(1)/Volold(1)) +
     &     dble(Npart(2))*dlog(Volnew(2)/Volold(2))),Laccept)

   1  CONTINUE

      DO Ib=1,N_Box
         TrialClusterVolume(Ib) = TrialClusterVolume(Ib) + 1.0d0
      END DO

      IF(Laccept) THEN

         DO Ib=1,N_Box
            AcceptClusterVolume(Ib) = AcceptClusterVolume(Ib) + 1.0d0

            U_LJ_Inter(Ib) = E_LJ_InterNew(Ib)
            U_LJ_Intra(Ib) = E_LJ_IntraNew(Ib)

            U_EL_Real(Ib)  = E_EL_RealNew(Ib)
            U_EL_Intra(Ib) = E_EL_IntraNew(Ib)
            U_EL_Excl(Ib)  = E_EL_ExclNew(Ib)
            U_EL_Four(Ib)  = E_EL_FourNew(Ib)

            U_Bending_Total(Ib) = E_BendingNew(Ib)
            U_Torsion_Total(Ib) = E_TorsionNew(Ib)

            U_Total(Ib) = U_Total(Ib) + dE(Ib)

            Volume(Ib) = Volnew(Ib)
         END DO

      ELSE

         DO Ib=1,N_Box
            BoxSize(Ib)       = Boxold(Ib)
            InvBoxSize(Ib)    = 1.0d0/BoxSize(Ib)
            U_LJ_Tail(Ib)     = E_LJ_TailOld(Ib)
            U_EL_Self(Ib) = E_EL_SelfOld(Ib)
         END DO

         DO Imol=1,N_MolTotal
            DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
               X(Imol,Iatom) = Xold(Imol,Iatom)
               Y(Imol,Iatom) = Yold(Imol,Iatom)
               Z(Imol,Iatom) = Zold(Imol,Iatom)
            END DO

            XCM(Imol) = XCMold(Imol)
            YCM(Imol) = YCMold(Imol)
            ZCM(Imol) = ZCMold(Imol)
         END DO

         DO Ib=1,N_Box
            IF(L_Ewald(Ib)) THEN
               CALL Ewald_Store(Ib,2)
            END IF
         END DO

      END IF

      RETURN
      END
