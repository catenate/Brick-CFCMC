      SUBROUTINE RXMC_Hybrid(Ifrac)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"
      include "settings.inc"
      include "trial_moves.inc"

      integer Imol(MaxMolInFrac),Jmol(MaxMolInFrac),Ifrac,Jfrac,Ib,Tm,It,Rsold,Rsnew,
     &     I,J,K,Select_Random_Integer,Ibin,Ireac,Iatom,Kmol,Lmol
      double precision Factorial_Ratio,Eold,Enew,dE,dW,Lf,factor,E_LJ_Inter,E_LJ_Intra,
     &     E_EL_Real,E_EL_Intra,E_EL_Excl,E_Torsion,E_LJ_InterOld,E_LJ_IntraOld,
     &     E_EL_RealOld,E_EL_IntraOld,E_EL_ExclOld,E_TorsionOld,E_LJ_InterNew,
     &     E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,E_TorsionNew,
     &     E_EL_SelfOld,E_LJ_TailOld,E_EL_SelfNew,E_LJ_TailNew,XCMold(MaxMolInFrac),
     &     YCMold(MaxMolInFrac),ZCMold(MaxMolInFrac),Xold(MaxMolInFrac,MaxAtom),
     &     Yold(MaxMolInFrac,MaxAtom),Zold(MaxMolInFrac,MaxAtom),sum,
     &     E_BendingOld,E_BendingNew,E_Bending,Myl,Myc,Myi,dE_EL_Four
      logical L_Overlap_Inter,L_Overlap_Intra,Lsamefrac,Laccept,LEwald

      IF(N_Frac.EQ.0) THEN
         WRITE(6,'(A,A)') ERROR, "No fractional(s)"
         STOP
      END IF

 100  CONTINUE
      Ifrac = Select_Random_Integer(N_Frac)

      IF(Type_Frac(Ifrac).NE.3) GO TO 100

C     Select a random fractional
      Ireac = Reaction_Frac(Ifrac)
      Lf    = Lambda_Frac(Ifrac)
      Ib    = Box_Frac(Ifrac)
      Rsold = ReactionStep_Frac(Ifrac)

      IF(L_Ewald(Ib)) CALL Ewald_Init

      IF(Lf.LT.RXMCHybridSwapSwitch) THEN
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC                   REACTION OF FRACTIONAL (SWAP MOVE)                    CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C     Select the new reactionstep at random, there are N-1 possiblities (Rsold is not allowed)
         Rsnew = Select_Random_Integer(N_ReactionStep(Ireac)-1)
         IF(Rsnew.EQ.Rsold) Rsnew = N_ReactionStep(Ireac)

         LEwald = .false.
C     Get labels and lambda of all molecules in the fractional
         DO I=1,N_MolInReactionStep(Ireac,Rsold)
            Imol(I) = I_MolInFrac(Ifrac,I)
            IF(L_ChargeInMolType(TypeMol(Imol(I))).AND.L_Ewald(Ib)) LEwald = .true.
         END DO

         Ibin = 1 + int(dble(N_LambdaBin(Ifrac))*Lf)

         IF(L_Ewald(Ib)) CALL Ewald_Init

         TrialRXMCswap(Ib,Rsold,Ifrac) = TrialRXMCswap(Ib,Rsold,Ifrac) + 1.0d0
         TrialRXMCswapvslambda(Ibin,Ib,Rsold,Ifrac) = TrialRXMCswapvslambda(Ibin,Ib,Rsold,Ifrac) + 1.0d0

C     Store old configuration
         DO I=1,N_MolInReactionStep(Ireac,Rsold)

            DO Iatom=1,N_AtomInMolType(TypeMol(Imol(I)))
               Xold(I,Iatom) = X(Imol(I),Iatom)
               Yold(I,Iatom) = Y(Imol(I),Iatom)
               Zold(I,Iatom) = Z(Imol(I),Iatom)
            END DO

            XCMold(I) = XCM(Imol(I))
            YCMold(I) = YCM(Imol(I))
            ZCMold(I) = ZCM(Imol(I))

         END DO

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            DO I=1,N_MolInReactionStep(Ireac,Rsold)
               Kmol = Imol(I)
               Tm   = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               CALL interactionlambda(Kmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                     J = NKSPACE(Ib,1)
                     XKSPACE(J,Ib,1) = X(Kmol,Iatom)
                     YKSPACE(J,Ib,1) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ib,1) = Z(Kmol,Iatom)
                     QKSPACE(J,Ib,1) = Myc*Q(It)
                  END IF
               END DO
            END DO
         END IF

CCC   ENERGY OF OLD CONFIGURATION
         E_LJ_TailOld  = U_LJ_Tail(Ib)
         E_EL_SelfOld  = U_EL_Self(Ib)

         E_LJ_InterOld = 0.0d0
         E_LJ_IntraOld = 0.0d0
         E_EL_RealOld  = 0.0d0
         E_EL_IntraOld = 0.0d0
         E_EL_ExclOld  = 0.0d0
         E_BendingOld  = 0.0d0
         E_TorsionOld  = 0.0d0

         DO I=1,N_MolInReactionStep(Ireac,Rsold)
            CALL Energy_Molecule(Imol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (RXMC Hybrid)"
               STOP
            END IF
            E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
            E_LJ_IntraOld = E_LJ_IntraOld + E_LJ_Intra
            E_EL_RealOld  = E_EL_RealOld  + E_EL_Real
            E_EL_IntraOld = E_EL_IntraOld + E_EL_Intra
            E_EL_ExclOld  = E_EL_ExclOld  + E_EL_Excl
            E_BendingOld  = E_BendingOld  + E_Bending
            E_TorsionOld  = E_TorsionOld  + E_Torsion
         END DO

C     Correct for counting LJ and Electrostatic energy double
         DO I=1,N_MolInReactionStep(Ireac,Rsold)-1
            DO J=I+1,N_MolInReactionStep(Ireac,Rsold)
               CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (RXMC Hybrid)"
                  STOP
               END IF
               E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
               E_EL_RealOld  = E_EL_RealOld  - E_EL_Real
            END DO
         END DO

         Eold = E_LJ_InterOld + E_EL_RealOld + E_EL_ExclOld + E_LJ_TailOld + E_EL_SelfOld

C     Remove old fractional, 'recycle' the label so that there are
C     no holes in the list I_MolInBox
         DO I=1,N_MolInReactionStep(Ireac,Rsold)

            Kmol = Imol(I)

            Ibox(Kmol)       = Ibox(N_MolTotal)
            TypeMol(Kmol)    = TypeMol(N_MolTotal)
            L_Frac(Kmol)     = L_Frac(N_MolTotal)
            XCM(Kmol)        = XCM(N_MolTotal)
            YCM(Kmol)        = YCM(N_MolTotal)
            ZCM(Kmol)        = ZCM(N_MolTotal)

            DO Iatom=1,N_AtomInMolType(TypeMol(N_MolTotal))
               X(Kmol,Iatom) = X(N_MolTotal,Iatom)
               Y(Kmol,Iatom) = Y(N_MolTotal,Iatom)
               Z(Kmol,Iatom) = Z(N_MolTotal,Iatom)
            END DO

            IF(Ibox(N_MolTotal).EQ.Ib) THEN
               Lmol = I_MolInBox(Ib,N_MolInBox(Ib))
               CALL Find_molecule_in_I_MolInBox(N_MolTotal,Ib,J)
               I_MolInBox(Ib,J) = Lmol
            ELSE
               CALL Find_molecule_in_I_MolInBox(N_MolTotal,Ibox(N_MolTotal),J)
               I_MolInBox(Ibox(N_MolTotal),J) = Kmol
               CALL Find_molecule_in_I_MolInBox(Kmol,Ib,J)
               Lmol = I_MolInBox(Ib,N_MolInBox(Ib))
               I_MolInBox(Ib,J) = Lmol
            END IF

C      If the last molecule in the list I_MolInBox (Lmol) is a fractional (which can be of a
C      different type than we consider at the moment) its corresponding I_MolInFrac should be
C      updated such that it refers to the new label (Kmol).
            IF(.NOT.L_Frac(N_MolTotal)) THEN
               CALL Find_molecule_in_Imptpb(N_MolTotal,Ibox(N_MolTotal),TypeMol(N_MolTotal),J)
               Imptpb(Ibox(N_MolTotal),TypeMol(N_MolTotal),J) = Kmol
            ELSE
               Lsamefrac=.false.
               DO J=I+1,N_MolInReactionStep(Ireac,Rsold)
                  IF(Imol(J).EQ.N_MolTotal) THEN
                     Imol(J) = Kmol
                     Lsamefrac=.true.
                  END IF
               END DO
               IF(.NOT.Lsamefrac) THEN
                  DO Jfrac=1,N_Frac
                     IF(Jfrac.EQ.Ifrac) CYCLE
                     DO K=1,N_MolInFrac(Jfrac)
                        IF(I_MolInFrac(Jfrac,K).EQ.N_MolTotal) THEN
                           I_MolInFrac(Jfrac,K) = Kmol
                           EXIT
                        END IF
                     END DO
                  END DO
               END IF
            END IF

            N_MolTotal = N_MolTotal - 1
            N_MolInBox(Ib) = N_MolInBox(Ib) - 1

         END DO

C     Insert new fractional
         DO I=1,N_MolInReactionStep(Ireac,Rsnew)

            N_MolTotal = N_MolTotal + 1

C     Assign a label to the new molecule
            N_MolInBox(Ib) = N_MolInBox(Ib) + 1
            Jmol(I)        = N_MolTotal
            Kmol           = Jmol(I)

C     Assign information to the new molecule
            Ibox(Kmol)                    = Ib
            I_MolInBox(Ib,N_MolInBox(Ib)) = Kmol
            L_Frac(Kmol)                  = .true.
            TypeMol(Kmol)                 = I_MolTypeInReactionStep(Ireac,Rsnew,I)

            I_MolInFrac(Ifrac,I) = Kmol
            IF(L_ChargeInMolType(TypeMol(Kmol)).AND.L_Ewald(Ib)) LEwald = .true.

C     Insert new molecule randomly
            CALL Generate_Conformation(Kmol)

         END DO

         ReactionStep_Frac(Ifrac) = Rsnew
         N_MolInFrac(Ifrac) = N_MolInReactionStep(Ireac,Rsnew)

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            DO I=1,N_MolInReactionStep(Ireac,Rsnew)
               Kmol = Jmol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               CALL interactionlambda(Kmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                     J = NKSPACE(Ib,2)
                     XKSPACE(J,Ib,2) = X(Kmol,Iatom)
                     YKSPACE(J,Ib,2) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ib,2) = Z(Kmol,Iatom)
                     QKSPACE(J,Ib,2) = Myc*Q(It)
                  END IF
               END DO
            END DO
         END IF

CCC   ENERGY OF NEW CONFIGURATION
         E_LJ_InterNew = 0.0d0
         E_LJ_IntraNew = 0.0d0
         E_EL_RealNew  = 0.0d0
         E_EL_IntraNew = 0.0d0
         E_EL_ExclNew  = 0.0d0
         E_BendingNew  = 0.0d0
         E_TorsionNew  = 0.0d0

         DO I=1,N_MolInReactionStep(Ireac,Rsnew)
            CALL Energy_Molecule(Jmol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (RXMC Hybrid)"
               STOP
            END IF
            E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
            E_LJ_IntraNew = E_LJ_IntraNew + E_LJ_Intra
            E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
            E_EL_IntraNew = E_EL_IntraNew + E_EL_Intra
            E_EL_ExclNew  = E_EL_ExclNew  + E_EL_Excl
            E_BendingNew  = E_BendingNew  + E_Bending
            E_TorsionNew  = E_TorsionNew  + E_Torsion
         END DO

C     Correct for counting LJ and Electrostatic energy double
         DO I=1,N_MolInReactionStep(Ireac,Rsnew)-1
            DO J=I+1,N_MolInReactionStep(Ireac,Rsnew)
               CALL Energy_Intermolecular(Jmol(I),Jmol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (RXMC Hybrid)"
                  STOP
               END IF
               E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
               E_EL_RealNew  = E_EL_RealNew  - E_EL_Real
            END DO
         END DO

         dE_EL_Four = 0.0d0
         IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

C     Update energy corrections
         CALL Energy_Correction(Ib)

         E_LJ_TailNew = U_LJ_Tail(Ib)
         E_EL_SelfNew = U_EL_Self(Ib)

         Enew = E_LJ_InterNew + E_EL_RealNew + E_EL_ExclNew + E_LJ_TailNew + E_EL_SelfNew

         dE = Enew - Eold + dE_EL_Four

         dW = Weight(Ibin,Ib,Rsnew,Ifrac) - Weight(Ibin,Ib,Rsold,Ifrac)

         sum=-beta*dE+dW
         DO Tm=1,N_MolType
            IF(N_MolOfMolTypeInReactionStep(Ireac,Rsold,Tm).NE.0) THEN
               sum=sum-N_MolOfMolTypeInReactionStep(Ireac,Rsold,Tm)*(dlog(Volume(Ib))+Qpartition(Tm))
            END IF
            IF(N_MolOfMolTypeInReactionStep(Ireac,Rsnew,Tm).NE.0) THEN
               sum=sum+N_MolOfMolTypeInReactionStep(Ireac,Rsnew,Tm)*(dlog(Volume(Ib))+Qpartition(Tm))
            END IF
         END DO

         CALL Accept_or_Reject(dexp(sum),Laccept)

         IF(Laccept) THEN

         AcceptRXMCswap(Ib,Rsold,Ifrac) = AcceptRXMCswap(Ib,Rsold,Ifrac) + 1.0d0
         AcceptRXMCswapvslambda(Ibin,Ib,Rsold,Ifrac) = AcceptRXMCswapvslambda(Ibin,Ib,Rsold,Ifrac) + 1.0d0

            U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew  - E_LJ_InterOld
            U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew  - E_LJ_IntraOld

            U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
            U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew - E_EL_IntraOld
            U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew  - E_EL_ExclOld
            U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

            U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew - E_BendingOld
            U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew - E_TorsionOld

            U_Total(Ib) = U_Total(Ib) + dE
     &                  + E_LJ_IntraNew - E_LJ_IntraOld
     &                  + E_EL_IntraNew - E_EL_IntraOld
     &                  + E_BendingNew  - E_BendingOld
     &                  + E_TorsionNew  - E_TorsionOld

            IF(LEwald) CALL Ewald_Accept(Ib)

         ELSE

            N_MolTotal     = N_MolTotal     - N_MolInReactionStep(Ireac,Rsnew)
            N_MolInBox(Ib) = N_MolInBox(Ib) - N_MolInReactionStep(Ireac,Rsnew)

            ReactionStep_Frac(Ifrac) = Rsold
            N_MolInFrac(Ifrac) = N_MolInReactionStep(Ireac,Rsold)

            DO I=1,N_MolInReactionStep(Ireac,Rsold)

               N_MolTotal     = N_MolTotal + 1
               N_MolInBox(Ib) = N_MolInBox(Ib) + 1
               Kmol           = N_MolTotal

               Ibox(Kmol)                    = Ib
               I_MolInBox(Ib,N_MolInBox(Ib)) = Kmol
               L_Frac(Kmol)                  = .true.
               TypeMol(Kmol)                 = I_MolTypeInReactionStep(Ireac,Rsold,I)
               I_MolInFrac(Ifrac,I)          = Kmol

               DO Iatom=1,N_AtomInMolType(TypeMol(Kmol))
                  X(Kmol,Iatom) = Xold(I,Iatom)
                  Y(Kmol,Iatom) = Yold(I,Iatom)
                  Z(Kmol,Iatom) = Zold(I,Iatom)
               END DO

               XCM(Kmol) = XCMold(I)
               YCM(Kmol) = YCMold(I)
               ZCM(Kmol) = ZCMold(I)

            END DO

            U_LJ_Tail(Ib) = E_LJ_TailOld
            U_EL_Self(Ib) = E_EL_SelfOld

         END IF

      ELSEIF(Lf.GT.RXMCHybridChangeSwitch) THEN
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC                      IDENTITY CHANGE (CHANGE MOVE)                      CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C     Select the new reactionstep at random, there are N-1 possiblities (Rsold is not allowed)
         Rsnew = Select_Random_Integer(N_ReactionStep(Ireac)-1)
         IF(Rsnew.EQ.Rsold) Rsnew = N_ReactionStep(Ireac)

         LEwald = .false.
C     Get labels and lambda of all molecules in the fractional
         DO I=1,N_MolInReactionStep(Ireac,Rsold)
            Imol(I) = I_MolInFrac(Ifrac,I)
            IF(L_ChargeInMolType(TypeMol(Imol(I))).AND.L_Ewald(Ib)) LEwald = .true.
         END DO

         Ibin = 1 + int(dble(N_LambdaBin(Ifrac))*Lf)

C     Check if there are enough whole molecules to change into fractional
         DO Tm=1,N_MolType
            IF(Nmptpb(Ib,Tm).LT.N_MolOfMolTypeInReactionStep(Ireac,Rsnew,Tm)) RETURN
         END DO

         TrialRXMCchange(Ib,Rsold,Ifrac) = TrialRXMCchange(Ib,Rsold,Ifrac) + 1.0d0
         TrialRXMCchangevslambda(Ibin,Ib,Rsold,Ifrac) = TrialRXMCchangevslambda(Ibin,Ib,Rsold,Ifrac) + 1.0d0

CC    Choose whole molecules (labeled Jmol) which will become fractional
         DO I=1,N_MolInReactionStep(Ireac,Rsnew)
 33         CONTINUE
            Tm = I_MolTypeInReactionStep(Ireac,Rsnew,I)
            Jmol(I) = Imptpb(Ib,Tm,Select_Random_Integer(Nmptpb(Ib,Tm)))

C     If a fractional type contains two or more more molecules of the same molecule type
C     it is possible that those two fractional molecules are assigned to the same whole
C     molecule in the new box, this should not happen: we check for that here.
            DO J=1,I-1
               IF(Jmol(I).EQ.Jmol(J)) GO TO 33
            END DO
            IF(L_ChargeInMolType(TypeMol(Jmol(I))).AND.L_Ewald(Ib)) LEwald = .true.
         END DO

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            DO I=1,N_MolInReactionStep(Ireac,Rsold)
               Kmol = Imol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               CALL interactionlambda(Kmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                     J = NKSPACE(Ib,1)
                     XKSPACE(J,Ib,1) = X(Kmol,Iatom)
                     YKSPACE(J,Ib,1) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ib,1) = Z(Kmol,Iatom)
                     QKSPACE(J,Ib,1) = Myc*Q(It)
                  END IF
               END DO
            END DO

            DO I=1,N_MolInReactionStep(Ireac,Rsnew)
               Kmol = Jmol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                     J = NKSPACE(Ib,1)
                     XKSPACE(J,Ib,1) = X(Kmol,Iatom)
                     YKSPACE(J,Ib,1) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ib,1) = Z(Kmol,Iatom)
                     QKSPACE(J,Ib,1) = Q(It)
                  END IF
               END DO
            END DO
         END IF

CCC   ENERGY OF OLD CONFIGURATION
         E_LJ_TailOld = U_LJ_Tail(Ib)
         E_EL_SelfOld = U_EL_Self(Ib)

         E_LJ_InterOld = 0.0d0
         E_LJ_IntraOld = 0.0d0
         E_EL_RealOld  = 0.0d0
         E_EL_IntraOld = 0.0d0
         E_EL_ExclOld  = 0.0d0
         E_BendingOld  = 0.0d0
         E_TorsionOld  = 0.0d0

         DO I=1,N_MolInReactionStep(Ireac,Rsold)
            CALL Energy_Molecule(Imol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (RXMC Hybrid)"
               STOP
            END IF
            E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
            E_LJ_IntraOld = E_LJ_IntraOld + E_LJ_Intra
            E_EL_RealOld  = E_EL_RealOld  + E_EL_Real
            E_EL_IntraOld = E_EL_IntraOld + E_EL_Intra
            E_EL_ExclOld  = E_EL_ExclOld  + E_EL_Excl
            E_BendingOld  = E_BendingOld  + E_Bending
            E_TorsionOld  = E_TorsionOld  + E_Torsion
         END DO

         DO I=1,N_MolInReactionStep(Ireac,Rsnew)
            CALL Energy_Molecule(Jmol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (RXMC Hybrid)"
               STOP
            END IF
            E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
            E_LJ_IntraOld = E_LJ_IntraOld + E_LJ_Intra
            E_EL_RealOld  = E_EL_RealOld  + E_EL_Real
            E_EL_IntraOld = E_EL_IntraOld + E_EL_Intra
            E_EL_ExclOld  = E_EL_ExclOld  + E_EL_Excl
            E_BendingOld  = E_BendingOld  + E_Bending
            E_TorsionOld  = E_TorsionOld  + E_Torsion
         END DO

C     Correct for counting LJ and Electrostatic energy double
         DO I=1,N_MolInReactionStep(Ireac,Rsold)-1
            DO J=I+1,N_MolInReactionStep(Ireac,Rsold)
               CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (RXMC Hybrid)"
                  STOP
               END IF
               E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
               E_EL_RealOld  = E_EL_RealOld  - E_EL_Real
            END DO
         END DO

         DO I=1,N_MolInReactionStep(Ireac,Rsnew)-1
            DO J=I+1,N_MolInReactionStep(Ireac,Rsnew)
               CALL Energy_Intermolecular(Jmol(I),Jmol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (RXMC Hybrid)"
                  STOP
               END IF
               E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
               E_EL_RealOld  = E_EL_RealOld  - E_EL_Real
            END DO
         END DO

         DO I=1,N_MolInReactionStep(Ireac,Rsold)
            DO J=1,N_MolInReactionStep(Ireac,Rsnew)
               CALL Energy_Intermolecular(Imol(I),Jmol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy overlap (RXMC_hybrid.f)"
                  STOP
               END IF
               E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
               E_EL_RealOld  = E_EL_RealOld  - E_EL_Real
            END DO
         END DO

         Eold = E_LJ_InterOld + E_EL_RealOld + E_EL_ExclOld + E_LJ_TailOld + E_EL_SelfOld

C     Transform fractionals into wholes
         DO I=1,N_MolInReactionStep(Ireac,Rsold)
            I_MolInFrac(Ifrac,I) = 0

            Tm = TypeMol(Imol(I))

            Nmptpb(Ib,Tm) = Nmptpb(Ib,Tm) + 1
            Imptpb(Ib,Tm,Nmptpb(Ib,Tm)) = Imol(I)

            L_Frac(Imol(I)) = .false.
         END DO

C     Transform wholes into fractionals
         DO I=1,N_MolInReactionStep(Ireac,Rsnew)
            I_MolInFrac(Ifrac,I) = Jmol(I)

            Tm = TypeMol(Jmol(I))

            CALL Find_molecule_in_Imptpb(Jmol(I),Ib,Tm,J)

            Imptpb(Ib,Tm,J) = Imptpb(Ib,Tm,Nmptpb(Ib,Tm))
            Nmptpb(Ib,Tm)   = Nmptpb(Ib,Tm) - 1

            L_Frac(Jmol(I)) = .true.
         END DO

         ReactionStep_Frac(Ifrac) = Rsnew
         N_MolInFrac(Ifrac) = N_MolInReactionStep(Ireac,Rsnew)

C     Store positions and charges for Ewald summation
         IF(LEwald) THEN
            DO I=1,N_MolInReactionStep(Ireac,Rsold)
               Kmol = Imol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                     J = NKSPACE(Ib,2)
                     XKSPACE(J,Ib,2) = X(Kmol,Iatom)
                     YKSPACE(J,Ib,2) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ib,2) = Z(Kmol,Iatom)
                     QKSPACE(J,Ib,2) = Q(It)
                  END IF
               END DO
            END DO

            DO I=1,N_MolInReactionStep(Ireac,Rsnew)
               Kmol = Jmol(I)
               Tm = TypeMol(Kmol)
               IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
               CALL interactionlambda(Kmol,Myl,Myc,Myi)
               DO Iatom=1,N_AtomInMolType(Tm)
                  It=TypeAtom(Tm,Iatom)
                  IF(L_Charge(It)) THEN
                     NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                     J = NKSPACE(Ib,2)
                     XKSPACE(J,Ib,2) = X(Kmol,Iatom)
                     YKSPACE(J,Ib,2) = Y(Kmol,Iatom)
                     ZKSPACE(J,Ib,2) = Z(Kmol,Iatom)
                     QKSPACE(J,Ib,2) = Myc*Q(It)
                  END IF
               END DO
            END DO
         END IF

CCC   ENERGY OF NEW CONFIGURATION
         E_LJ_InterNew = 0.0d0
         E_LJ_IntraNew = 0.0d0
         E_EL_RealNew  = 0.0d0
         E_EL_IntraNew = 0.0d0
         E_EL_ExclNew  = 0.0d0
         E_BendingNew  = 0.0d0
         E_TorsionNew  = 0.0d0

         DO I=1,N_MolInReactionStep(Ireac,Rsold)
            CALL Energy_Molecule(Imol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               Laccept=.false.
               GO TO 2
            END IF
            E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
            E_LJ_IntraNew = E_LJ_IntraNew + E_LJ_Intra
            E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
            E_EL_IntraNew = E_EL_IntraNew + E_EL_Intra
            E_EL_ExclNew  = E_EL_ExclNew  + E_EL_Excl
            E_BendingNew  = E_BendingNew  + E_Bending
            E_TorsionNew  = E_TorsionNew  + E_Torsion
         END DO

         DO I=1,N_MolInReactionStep(Ireac,Rsnew)
            CALL Energy_Molecule(Jmol(I),E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (RXMC Hybrid)"
               STOP
            END IF
            E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
            E_LJ_IntraNew = E_LJ_IntraNew + E_LJ_Intra
            E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
            E_EL_IntraNew = E_EL_IntraNew + E_EL_Intra
            E_EL_ExclNew  = E_EL_ExclNew  + E_EL_Excl
            E_BendingNew  = E_BendingNew  + E_Bending
            E_TorsionNew  = E_TorsionNew  + E_Torsion
         END DO

C     Correct for counting LJ and Electrostatic energy double
         DO I=1,N_MolInReactionStep(Ireac,Rsold)-1
            DO J=I+1,N_MolInReactionStep(Ireac,Rsold)
               CALL Energy_Intermolecular(Imol(I),Imol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  Laccept=.false.
                  GO TO 2
               END IF
               E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
               E_EL_RealNew  = E_EL_RealNew  - E_EL_Real
            END DO
         END DO

         DO I=1,N_MolInReactionStep(Ireac,Rsnew)-1
            DO J=I+1,N_MolInReactionStep(Ireac,Rsnew)
               CALL Energy_Intermolecular(Jmol(I),Jmol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (RXMC Hybrid)"
                  STOP
               END IF
               E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
               E_EL_RealNew  = E_EL_RealNew  - E_EL_Real
            END DO
         END DO

         DO I=1,N_MolInReactionStep(Ireac,Rsold)
            DO J=1,N_MolInReactionStep(Ireac,Rsnew)
               CALL Energy_Intermolecular(Imol(I),Jmol(J),E_LJ_Inter,E_EL_Real,L_Overlap_Inter)
               IF(L_Overlap_Inter) THEN
                  WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (RXMC Hybrid)"
                  STOP
               END IF
               E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
               E_EL_RealNew  = E_EL_RealNew  - E_EL_Real
            END DO
         END DO

         dE_EL_Four = 0.0d0
         IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four)

C     Update energy corrections
         CALL Energy_Correction(Ib)

         E_LJ_TailNew = U_LJ_Tail(Ib)
         E_EL_SelfNew = U_EL_Self(Ib)

         Enew = E_LJ_InterNew + E_EL_RealNew + E_EL_ExclNew + E_LJ_TailNew + E_EL_SelfNew

         dE = Enew - Eold + dE_EL_Four
         dW = Weight(Ibin,Ib,Rsnew,Ifrac) - Weight(Ibin,Ib,Rsold,Ifrac)

         factor = dexp(-beta*dE+dW)

         DO Tm=1,N_MolType
            IF(N_MolOfMolTypeInReactionStep(Ireac,Rsold,Tm).NE.0) THEN
               factor = factor*Factorial_Ratio(Nmptpb(Ib,Tm)-N_MolOfMolTypeInReactionStep(Ireac,Rsold,Tm),Nmptpb(Ib,Tm))
            END IF
            IF(N_MolOfMolTypeInReactionStep(Ireac,Rsnew,Tm).NE.0) THEN
               factor = factor*Factorial_Ratio(Nmptpb(Ib,Tm)+N_MolOfMolTypeInReactionStep(Ireac,Rsnew,Tm),Nmptpb(Ib,Tm))
            END IF
         END DO

         CALL Accept_or_Reject(factor,Laccept)

   2     CONTINUE

         IF(Laccept) THEN

            AcceptRXMCchange(Ib,Rsold,Ifrac) = AcceptRXMCchange(Ib,Rsold,Ifrac) + 1.0d0
            AcceptRXMCchangevslambda(Ibin,Ib,Rsold,Ifrac) = AcceptRXMCchangevslambda(Ibin,Ib,Rsold,Ifrac) + 1.0d0

            U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
            U_LJ_Intra(Ib) = U_LJ_Intra(Ib) + E_LJ_IntraNew - E_LJ_IntraOld

            U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
            U_EL_Intra(Ib) = U_EL_Intra(Ib) + E_EL_IntraNew - E_EL_IntraOld
            U_EL_Excl(Ib)  = U_EL_Excl(Ib)  + E_EL_ExclNew  - E_EL_ExclOld
            U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

            U_Bending_Total(Ib) = U_Bending_Total(Ib) + E_BendingNew - E_BendingOld
            U_Torsion_Total(Ib) = U_Torsion_Total(Ib) + E_TorsionNew - E_TorsionOld

            U_Total(Ib) = U_Total(Ib) + dE
     &                  + E_LJ_IntraNew - E_LJ_IntraOld
     &                  + E_EL_IntraNew - E_EL_IntraOld
     &                  + E_BendingNew  - E_BendingOld
     &                  + E_TorsionNew  - E_TorsionOld

            IF(LEwald) CALL Ewald_Accept(Ib)

         ELSE

C     Undo changes in lists
            DO I=1,N_MolInReactionStep(Ireac,Rsnew)
               I_MolInFrac(Ifrac,I) = 0

               Tm = TypeMol(Jmol(I))

               Nmptpb(Ib,Tm) = Nmptpb(Ib,Tm) + 1
               Imptpb(Ib,Tm,Nmptpb(Ib,Tm)) = Jmol(I)

               L_Frac(Jmol(I)) = .false.
            END DO

            DO I=1,N_MolInReactionStep(Ireac,Rsold)
               I_MolInFrac(Ifrac,I) = Imol(I)

               Tm = TypeMol(Imol(I))

               CALL Find_molecule_in_Imptpb(Imol(I),Ib,Tm,J)

               Imptpb(Ib,Tm,J) = Imptpb(Ib,Tm,Nmptpb(Ib,Tm))
               Nmptpb(Ib,Tm) = Nmptpb(Ib,Tm) - 1

               L_Frac(Imol(I)) = .true.
            END DO

            U_LJ_Tail(Ib) = E_LJ_TailOld
            U_EL_Self(Ib) = E_EL_SelfOld

            ReactionStep_Frac(Ifrac) = Rsold
            N_MolInFrac(Ifrac) = N_MolInReactionStep(Ireac,Rsold)

         END IF

      END IF

      RETURN
      END
