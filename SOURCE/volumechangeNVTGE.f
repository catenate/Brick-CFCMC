      SUBROUTINE VolumeChangeNVTGE
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "output.inc"

      integer Imol,Ib,Iatom,Tm
      double precision Boxold(2),Volold(2),Volnew(2),Df(2),Ran_Uniform,
     &     E_LJ_InterNew(2),E_LJ_IntraNew(2),E_EL_RealNew(2),E_EL_IntraNew(2),
     &     E_TorsionNew(2),Eold(2),Enew(2),dX,dY,dZ,dE(2),XCMold(MaxMol),
     &     YCMold(MaxMol),ZCMold(MaxMol),Xold(MaxMol,MaxAtom),E_BendingNew(2),
     &     Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),E_LJ_TailOld(2),E_EL_FourNew(2),
     &     E_EL_SelfOld(2),E_EL_ExclNew(2),E_LJ_TailNew(2),E_EL_SelfNew(2)
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept

C     Volume change in the Gibbs NVT ensemble
      DO Ib=1,N_Box
         Boxold(Ib)       = BoxSize(Ib)
         Volold(Ib)       = BoxSize(Ib)*BoxSize(Ib)*BoxSize(Ib)

         E_LJ_TailOld(Ib) = U_LJ_Tail(Ib)
         E_EL_SelfOld(Ib) = U_EL_Self(Ib)

         Eold(Ib) = U_Total(Ib)

         IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)
      END DO

      Volnew(1) = Volold(1) + (2.0d0*Ran_Uniform()-1.0d0)*Delta_Volume(1)

      IF(Volnew(1).LE.0.0d0) RETURN

      Volnew(2) = Volold(1) + Volold(2) - Volnew(1)

      IF(Volnew(2).LE.0.0d0) RETURN

      DO Ib=1,N_Box
         Df(Ib)         = (Volnew(Ib)/Volold(Ib))**(1.0d0/3.0d0)
         BoxSize(Ib)    = Boxold(Ib)*Df(Ib)
         InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)
      END DO

      DO Ib=1,N_Box
         IF(.NOT.L_IdealGas(Ib)) THEN
            IF(dsqrt(R_Cut_Max_2(Ib)).GT.0.5d0*BoxSize(Ib)) THEN
               WRITE(6,'(A,A,i1)') ERROR, "Volume; Rcut is too large in Box ", Ib
               STOP
            END IF
         END IF
      END DO

      DO Imol=1,N_MolTotal

         Tm = TypeMol(Imol)
         Ib = Ibox(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Imol,Iatom) = X(Imol,Iatom)
            Yold(Imol,Iatom) = Y(Imol,Iatom)
            Zold(Imol,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(Imol) = XCM(Imol)
         YCMold(Imol) = YCM(Imol)
         ZCMold(Imol) = ZCM(Imol)

         XCM(Imol) = XCM(Imol)*Df(Ib)
         YCM(Imol) = YCM(Imol)*Df(Ib)
         ZCM(Imol) = ZCM(Imol)*Df(Ib)

         dX = XCM(Imol) - XCMold(Imol)
         dY = YCM(Imol) - YCMold(Imol)
         dZ = ZCM(Imol) - ZCMold(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            X(Imol,Iatom) = X(Imol,Iatom) + dX
            Y(Imol,Iatom) = Y(Imol,Iatom) + dY
            Z(Imol,Iatom) = Z(Imol,Iatom) + dZ
         END DO

      END DO

      DO Ib=1,N_Box
         CALL Energy_Total(Ib,E_LJ_InterNew(Ib),E_LJ_IntraNew(Ib),E_EL_RealNew(Ib),E_EL_IntraNew(Ib),
     &                        E_EL_ExclNew(Ib),E_BendingNew(Ib),E_TorsionNew(Ib),L_Overlap_Inter,L_Overlap_Intra)
         IF(L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Volume Change NVTGE)"
            STOP
         ELSEIF(L_Overlap_Inter) THEN
            Laccept=.false.
            GO TO 1
         END IF

         E_EL_FourNew(Ib) = 0.0d0
         IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew(Ib))
    
         CALL Energy_Correction(Ib)

         E_LJ_TailNew(Ib) = U_LJ_Tail(Ib)
         E_EL_SelfNew(Ib) = U_EL_Self(Ib)

         Enew(Ib) = E_LJ_InterNew(Ib) + E_LJ_IntraNew(Ib) + E_EL_RealNew(Ib) + E_EL_IntraNew(Ib) + E_EL_ExclNew(Ib)
     &            + E_BendingNew(Ib)  + E_TorsionNew(Ib)  + E_LJ_TailNew(Ib) + E_EL_SelfNew(Ib)  + E_EL_FourNew(Ib)

         dE(Ib) = Enew(Ib) - Eold(Ib)
      END DO

      CALL Accept_or_Reject(dexp(-beta*(dE(1)+dE(2)) +
     &     dble(N_MolInBox(1))*dlog(Volnew(1)/Volold(1)) +
     &     dble(N_MolInBox(2))*dlog(Volnew(2)/Volold(2))),Laccept)

   1  CONTINUE

      DO Ib=1,N_Box
         TrialVolume(Ib) = TrialVolume(Ib) + 1.0d0
      END DO

      IF(Laccept) THEN

         DO Ib=1,N_Box

            AcceptVolume(Ib) = AcceptVolume(Ib) + 1.0d0

            U_LJ_Inter(Ib) = E_LJ_InterNew(Ib)
            U_LJ_Intra(Ib) = E_LJ_IntraNew(Ib)

            U_EL_Real(Ib)  = E_EL_RealNew(Ib)
            U_EL_Intra(Ib) = E_EL_IntraNew(Ib)
            U_EL_Excl(Ib)  = E_EL_ExclNew(Ib)
            U_EL_Four(Ib)  = E_EL_FourNew(Ib)

            U_Bending_Total(Ib) = E_BendingNew(Ib)
            U_Torsion_Total(Ib) = E_TorsionNew(Ib)

            U_Total(Ib) = U_Total(Ib) + dE(Ib)

            Volume(Ib) = Volnew(Ib)

         END DO

      ELSE

         DO Ib=1,N_Box
            BoxSize(Ib)    = Boxold(Ib)
            InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)
            U_LJ_Tail(Ib)  = E_LJ_TailOld(Ib)
            U_EL_Self(Ib)  = E_EL_SelfOld(Ib)
         END DO

         DO Imol=1,N_MolTotal
            DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
               X(Imol,Iatom) = Xold(Imol,Iatom)
               Y(Imol,Iatom) = Yold(Imol,Iatom)
               Z(Imol,Iatom) = Zold(Imol,Iatom)
            END DO

            XCM(Imol) = XCMold(Imol)
            YCM(Imol) = YCMold(Imol)
            ZCM(Imol) = ZCMold(Imol)

         END DO

         DO Ib=1,N_Box
            IF(L_Ewald(Ib)) THEN
               CALL Ewald_Store(Ib,2)
            END IF
         END DO

      END IF

      RETURN
      END
