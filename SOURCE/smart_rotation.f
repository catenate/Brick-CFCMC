      SUBROUTINE Smart_Rotation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"

C     Give random molecule a random displacement

      integer Imol,Ib,Tm,I,Select_Random_Integer,Iatom
      double precision dE,XCMold(MaxMol),YCMold(MaxMol),ZCMold(MaxMol),Xold(MaxMol,MaxAtom),
     &      Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),M_x,M_y,M_z,E_LJ_InterNew,E_LJ_IntraNew,
     &      E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,E_BendingNew,E_TorsionNew,E_EL_FourNew,Eold,
     &      Enew,dX(MaxMol),dY(MaxMol),dZ(MaxMol),Ri,s_old,s_new,ds,Ran_Gauss,E_EL_SelfOld,Avd,
     &      E_EL_SelfNew,dgamma(MaxMol),Rrx,Rry,Rrz,Rxxnew,Ryynew,Rzznew,sing,cosg,dXu(Maxmol),dYu(Maxmol),
     &      dZu(Maxmol),dR(Maxmol),E_LJ_TailOld,E_LJ_TailNew,q0,qx,qy,qz,a3,b3,c3,d3,dummy
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept

      Ib = Select_Random_Integer(N_Box)

      TrialSmartRotation(Ib) = TrialSmartRotation(Ib) + 1.0d0

C     Save old configuration
      E_LJ_TailOld = U_LJ_Tail(Ib)
      E_EL_SelfOld = U_EL_Self(Ib)

      Eold  = U_Total(Ib)

      IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)

C     Calculate 's' of old configuration for acceptance rule
      s_old = 0.0d0

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Imol,Iatom) = X(Imol,Iatom)
            Yold(Imol,Iatom) = Y(Imol,Iatom)
            Zold(Imol,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(Imol) = XCM(Imol)
         YCMold(Imol) = YCM(Imol)
         ZCMold(Imol) = ZCM(Imol)

         CALL Torque_Molecule(Imol,M_x,M_y,M_z,L_Overlap_Inter,L_Overlap_Intra)

         dX(I) = beta*B_Rotation(Ib)*M_x + Ran_Gauss()*Gamma_Rotation(Ib)
         dY(I) = beta*B_Rotation(Ib)*M_y + Ran_Gauss()*Gamma_Rotation(Ib)
         dZ(I) = beta*B_Rotation(Ib)*M_z + Ran_Gauss()*Gamma_Rotation(Ib)

         dR(I) = dX(I)*dX(I) + dY(I)*dY(I) + dZ(I)*dZ(I)

         dgamma(I) = -dsqrt(dR(I))

         IF(dR(I).GT.zero) THEN
            Ri = -1.0d0/dgamma(I)

            dXu(I) = dX(I)*Ri
            dYu(I) = dY(I)*Ri
            dZu(I) = dZ(I)*Ri
         ELSE
            dXu(I) = 0.0d0
            dYu(I) = 0.0d0
            dZu(I) = 0.0d0
         END IF

         s_old = s_old - (beta*B_Rotation(Ib)*M_x - dX(I))*(beta*B_Rotation(Ib)*M_x - dX(I))
     &                 - (beta*B_Rotation(Ib)*M_y - dY(I))*(beta*B_Rotation(Ib)*M_y - dY(I))
     &                 - (beta*B_Rotation(Ib)*M_z - dZ(I))*(beta*B_Rotation(Ib)*M_z - dZ(I))

      END DO

C     Generate new configuration
      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         cosg   = dcos(0.5d0*dgamma(I))
         sing   = dsin(0.5d0*dgamma(I))

         DO Iatom=1,N_AtomInMolType(Tm)

            Rrx = X(Imol,Iatom) - XCM(Imol)
            Rry = Y(Imol,Iatom) - YCM(Imol)
            Rrz = Z(Imol,Iatom) - ZCM(Imol)

            q0 = cosg
            qx = sing*dXu(I)
            qy = sing*dYu(I)
            qz = sing*dZu(I)

            CALL Quaternion_Multiply(q0,-qx,-qy,-qz, 0.0d0,Rrx,Rry,Rrz, a3,b3,c3,d3)
            CALL Quaternion_Multiply(a3,b3,c3,d3, q0,qx,qy,qz, dummy,Rxxnew,Ryynew,Rzznew)

            X(Imol,Iatom) = Rxxnew + XCM(Imol)
            Y(Imol,Iatom) = Ryynew + YCM(Imol)
            Z(Imol,Iatom) = Rzznew + ZCM(Imol)

          END DO

          CALL Place_molecule_back_in_box(Imol)

      END DO

C     Calculate 's' of new configuration for acceptance rule
      s_new = 0.0d0

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)

         CALL Torque_Molecule(Imol,M_x,M_y,M_z,L_Overlap_Inter,L_Overlap_Intra)

         s_new = s_new - (beta*B_Rotation(Ib)*M_x + dX(I))*(beta*B_Rotation(Ib)*M_x + dX(I))
     &                 - (beta*B_Rotation(Ib)*M_y + dY(I))*(beta*B_Rotation(Ib)*M_y + dY(I))
     &                 - (beta*B_Rotation(Ib)*M_z + dZ(I))*(beta*B_Rotation(Ib)*M_z + dZ(I))
      END DO

C     Calculate energy of new configuration
      CALL Energy_Total(Ib,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,
     &                     E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra)
      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Smart Translation)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF

      E_EL_FourNew = 0.0d0
      IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew)

      CALL Energy_Correction(Ib)

      E_LJ_TailNew = U_LJ_Tail(Ib)
      E_EL_SelfNew = U_EL_Self(Ib)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
     &     + E_TorsionNew  + E_LJ_TailNew  + E_EL_SelfNew + E_EL_ExclNew  + E_EL_FourNew

      dE = Enew - Eold
      ds = (s_new - s_old)/(4.0d0*B_Rotation(Ib))

      CALL Accept_or_Reject(dexp(-beta*dE + ds),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         AcceptSmartRotation(Ib) = AcceptSmartRotation(Ib) + 1.0d0

         U_LJ_Inter(Ib) = E_LJ_InterNew
         U_LJ_Intra(Ib) = E_LJ_IntraNew

         U_EL_Real(Ib)  = E_EL_RealNew
         U_EL_Intra(Ib) = E_EL_IntraNew
         U_EL_Excl(Ib)  = E_EL_ExclNew
         U_EL_Four(Ib)  = E_EL_FourNew
         U_EL_Self(Ib)  = E_EL_SelfNew

         U_Bending_Total(Ib) = E_BendingNew
         U_Torsion_Total(Ib) = E_TorsionNew

         U_Total(Ib) = U_Total(Ib) + dE

         Avd = 0.0d0
         DO I=1,N_MolInBox(Ib)
            Avd = Avd - dgamma(I) !angles are negative
         END DO
         AvDelta_AcceptSmartRotation(Ib) = AvDelta_AcceptSmartRotation(Ib) + Avd/dble(N_MolInBox(Ib))

      ELSE

         U_LJ_Tail(Ib)  = E_LJ_TailOld
         U_EL_Self(Ib)  = E_EL_SelfOld

         DO I=1,N_MolInBox(Ib)
            Imol=I_MolInBox(Ib,I)

            DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
               X(Imol,Iatom) = Xold(Imol,Iatom)
               Y(Imol,Iatom) = Yold(Imol,Iatom)
               Z(Imol,Iatom) = Zold(Imol,Iatom)
            END DO

            XCM(Imol) = XCMold(Imol)
            YCM(Imol) = YCMold(Imol)
            ZCM(Imol) = ZCMold(Imol)
         END DO

         IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,2)

      END IF

      RETURN
      END
