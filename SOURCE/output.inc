      character*24   OK,ERROR,WARNING
      character*1    Cnum(9)
      character*3    Month(12)
      character*6    Carrow
      character*121  hashes(2),bars(2),doublebars(2),Caverages(2),Cbox(2)

      Parameter (OK      = "OK")
      Parameter (WARNING = "WARNING")
      Parameter (ERROR   = "ERROR")

      parameter (hashes =
     &(/"##############################################################################                                           ",
     &"#########################################################################################################################"/))
      parameter (bars =
     &(/"------------------------------------------------------------------------------                                           ",
     &"-------------------------------------------------------------------------------------------------------------------------"/))
      parameter (doublebars =
     &(/"==============================================================================                                           ",
     &"========================================================================================================================="/))
      parameter (Cbox =
     &(/"                                               Box 1               Box 2                                           ",
     &"                                               Box 1               Box 2                  Box 1               Box 2"/))

      parameter (Caverages =
     &(/"                                             Boltzmann            Biased                                  ",
     & "                                                       Boltzmann                                    Biased"/))
      parameter (Carrow = " <--> ")
      parameter (Cnum = (/"1","2","3","4","5","6","7","8","9"/))
      parameter (Month = (/"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"/))
